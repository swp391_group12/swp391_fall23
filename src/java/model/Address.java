/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import lombok.Builder;

/**
 *
 * @author Admin
 */
@Builder
public class Address {
    private int addressId;
    private int userId;
    private String name;
    private String address;
    private String phone;
    private String commune;
    private String district;
    private String city;
    private boolean defaut;
    private boolean isDisplay;

    public Address() {
    }

    public Address(int addressId, int userId, String name, String address, String phone, String commune, String district, String city, boolean defaut, boolean isDisplay) {
        this.addressId = addressId;
        this.userId = userId;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.commune = commune;
        this.district = district;
        this.city = city;
        this.defaut = defaut;
        this.isDisplay = isDisplay;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public boolean isDefaut() {
        return defaut;
    }

    public void setDefaut(boolean defaut) {
        this.defaut = defaut;
    }

    public boolean isIsDisplay() {
        return isDisplay;
    }

    public void setIsDisplay(boolean isDisplay) {
        this.isDisplay = isDisplay;
    }
    
    

   
    
    

    
    
}
