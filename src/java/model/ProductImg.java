/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Admin
 */
public class ProductImg {
    private int ImgId;
    private String ImgUrl;
    private int productId;

    public ProductImg() {
    }

    public ProductImg(int ImgId, String ImgUrl, int productId) {
        this.ImgId = ImgId;
        this.ImgUrl = ImgUrl;
        this.productId = productId;
    }

    public int getImgId() {
        return ImgId;
    }

    public void setImgId(int ImgId) {
        this.ImgId = ImgId;
    }

    public String getImgUrl() {
        return ImgUrl;
    }

    public void setImgUrl(String ImgUrl) {
        this.ImgUrl = ImgUrl;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }
    
    
}
