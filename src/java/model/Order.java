/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.math.BigDecimal;
import java.sql.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Trung Kien
 */
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Order {

    private int orderId;
    private BigDecimal totalPrice;
    private String note;
    private String name;
    private String phone;
    private String email;
    private Date createDate;
    private int userId;
    private int status;
    private Integer stoId;
    private String orderCode;
    private int addressId;
    private int sellerId;
    private Address address;
    private Status statusModel;
    private User seller;
    private Date deliveryDate;
    private BigDecimal totalPriceIn;
}
