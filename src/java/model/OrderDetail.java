/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Trung Kien
 */
@Builder
@Getter
@Setter
@AllArgsConstructor
public class OrderDetail {
    private int orderDetailId;
    private float quantity;
    private int productSizeId;
    private int productId;
    private int orderId;
    private Product product;

    public OrderDetail() {
    }

    public OrderDetail(int orderDetailId, float quantity, int productSizeId, int productId, int orderId) {
        this.orderDetailId = orderDetailId;
        this.quantity = quantity;
        this.productSizeId = productSizeId;
        this.productId = productId;
        this.orderId = orderId;
    }
    
}
