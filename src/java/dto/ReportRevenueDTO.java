/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dto;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Trung Kien
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ReportRevenueDTO {
    private BigDecimal revenueOut;
    private BigDecimal revenueIn;
    
    public void formatRevenue() {
        if (revenueIn == null) {
            this.revenueIn = BigDecimal.ZERO;
        }
        if (revenueOut == null) {
            this.revenueOut = BigDecimal.ZERO;
        }
    }
}
