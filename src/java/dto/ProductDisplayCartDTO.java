/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import model.Category;
import model.Image;
import model.Product;
import model.ProductSize;
import model.Size;
import model.Status;
import model.Store;

/**
 *
 * @author Trung Kien
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductDisplayCartDTO {

    private int pid;
    private String pName;
    private String pDetail;
    private String pBrand;
    private String pSKU;
    private Category category;
    private Status status;
    private Store store;
    private Float quantity = 1F;
    private double price;
    private double priceIn;
    private double total;
    private String sizeName;
    private int sizeId;
    private String imageUrl;

    public ProductDisplayCartDTO(Product product) {
        this.pid = product.getPid();
        this.pName = product.getpName();
        this.pBrand = product.getpBrand();
        this.pSKU = product.getpSKU();
        this.category = product.getCategory();
        this.status = product.getStatus();
        this.store = product.getStore();
        this.price = product.getPrice();
        this.priceIn = product.getPriceIn();
        this.imageUrl = product.getImage().getiUrl();
    }

    public double getTotal() {
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        return Double.parseDouble(decimalFormat.format(this.getPrice() * this.quantity));
    }
}
