/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import model.Slider;

/**
 *
 * @author Trung Kien
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SliderDTO {

    private int slid;
    private String slUrl;
    private String slLink;

    public static SliderDTO toDto(Slider slider) {
        return SliderDTO.builder()
                .slid(slider.getSliderId())
                .slUrl(slider.getSliderImg())
                .slLink(slider.getSliderLink())
                .build();
    }
}
