/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dal.StatusDAO;
import dto.StatusDTO;
import java.util.ArrayList;
import java.util.List;
import model.Status;
import service.interfaces.IStatusService;

/**
 *
 * @author Trung Kien
 */
public class StatusService implements IStatusService{
    
    private final StatusDAO statusDAO = new StatusDAO();

    @Override
    public List<StatusDTO> findAll() {
        List<Status> statuses = statusDAO.getAll();
        List<StatusDTO> result = new ArrayList<>();
        for (Status status : statuses) {
            result.add(StatusDTO.toDto(status));
        }
        return result;
    }

}
