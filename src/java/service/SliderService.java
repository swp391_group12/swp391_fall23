/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dal.SliderDAO;
import dto.SliderDTO;
import java.util.ArrayList;
import java.util.List;
import model.Slider;
import service.interfaces.ISliderService;

/**
 *
 * @author Trung Kien
 */
public class SliderService implements ISliderService{
    private final SliderDAO sliderDAO = new SliderDAO();

    @Override
    public List<SliderDTO> getAll() {
        List<SliderDTO> data = new ArrayList<>();
        List<Slider> sliders = sliderDAO.findAll();
        for(Slider s : sliders) {
            data.add(SliderDTO.toDto(s));
        }
        return data;
    }
    
}
