/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.ProductSize;
import model.Size;
import utils.Helpers;

/**
 *
 * @author Trung Kien
 */
public class ProductSizeDAO extends DBContext implements GenericDAO<ProductSize, Integer> {

    @Override
    public boolean create(ProductSize data) {
        return false;
    }

    @Override
    public boolean update(ProductSize data) {
        try {
            String sql = "UPDATE [dbo].[Product_Size] SET [quantity] = ? ,[price] = ? WHERE pid = ? and sid = ?";

            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, data.getQuantity());
            ps.setDouble(2, data.getPrice());
            ps.setInt(3, data.getProductId());
            ps.setInt(4, data.getpSizeId());

            return ps.executeUpdate() != 0;

        } catch (SQLException ex) {
            Logger.getLogger(StoreDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean deleteById(Integer id) {
        return false;
    }

    @Override
    public ProductSize getById(Integer id) {
        return new ProductSize();
    }

    @Override
    public List<ProductSize> findAll() {
        return new ArrayList<>();
    }

    public ProductSize getByProductIdAndSizeId(int proId, int sizeId) {
        String sql = "select * from Product_Size ps left join Sizes s on ps.sid = s.sid where pid=? and s.sid=?";
        ProductSize productSize = new ProductSize();
        try {

            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, proId);
            ps.setInt(2, sizeId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                productSize = new ProductSize(
                        rs.getInt("psid"),
                        rs.getInt("pid"),
                        new Size(rs.getInt("sid"), rs.getString("sName")),
                        rs.getInt("quantity"),
                        rs.getDouble("price")
                );
            }
        } catch (SQLException ex) {
            Logger.getLogger(StoreDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return productSize;
    }

}
