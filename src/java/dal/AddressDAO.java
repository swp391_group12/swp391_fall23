/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Address;

/**
 *
 * @author Admin
 */
public class AddressDAO extends DBContext {

    public List<Address> getAddressByUid(int uid) {
        List<Address> a = new ArrayList<>();
        try {
            String sql = "SELECT [aid]\n"
                    + "      ,[uid]\n"
                    + "      ,[name]\n"
                    + "      ,[address]\n"
                    + "      ,[phone]\n"
                    + "      ,[default]\n"
                    + "      ,[city]\n"
                    + "      ,[commune]\n"
                    + "      ,[district]\n"
                    + "      ,[isDisplay]\n"
                    + "  from [dbo].[Address] where [uid] = ? and [isDisplay] = 1 ORDER BY [default] DESC";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, uid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a.add(new Address(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(8), rs.getString(9), rs.getString(7), rs.getBoolean(6), rs.getBoolean(10)));
            }
            return a;
        } catch (Exception e) {

        }
        return null;
    }

    public Address getAddressByid(int id) {
        Address a = null;
        try {
            String sql = "SELECT [aid]\n"
                    + "      ,[uid]\n"
                    + "      ,[name]\n"
                    + "      ,[address]\n"
                    + "      ,[phone]\n"
                    + "      ,[default]\n"
                    + "      ,[city]\n"
                    + "      ,[commune]\n"
                    + "      ,[district]\n"
                    + "      ,[isDisplay]\n"
                    + "  from [dbo].[Address] where [aid] = ? ";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new Address(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(8), rs.getString(9), rs.getString(7), rs.getBoolean(6), rs.getBoolean(10));
            }
        } catch (Exception e) {

        }
        return a;
    }

    public boolean addNewAddress(Address a) {
        try {
            String sql = "INSERT INTO [dbo].[Address] "
                    + "      ([uid]\n"
                    + "      ,[name]\n"
                    + "      ,[address]\n"
                    + "      ,[phone]\n"
                    + "      ,[default]\n"
                    + "      ,[city]\n"
                    + "      ,[commune]\n"
                    + "      ,[district]\n"
                    + "      ,[isDisplay])\n"
                    + " VALUES\n"
                    + "		(?, ?, ?, ?, ? ,? ,? ,? ,?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, a.getUserId());
            st.setString(2, a.getName());
            st.setString(3, a.getAddress());
            st.setString(4, a.getPhone());
            st.setBoolean(5, a.isDefaut());
            st.setString(6, a.getCity());
            st.setString(7, a.getCommune());
            st.setString(8, a.getDistrict());
            st.setBoolean(9, a.isIsDisplay());
            st.executeUpdate();
            return true;

        } catch (Exception e) {

        }
        return false;
    }

    public boolean setDefault(int uid) {
        try {
            String sql = "UPDATE address SET [default] = 0 WHERE uid = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, uid);
            int row = st.executeUpdate();
            if (row > 0) {
                return true;
            }
        } catch (Exception e) {

        }
        return false;
    }

    public int addressCount(int uid) {
        int count = 0;
        try {
            String sql = "SELECT COUNT(*) AS address_count FROM address WHERE uid = ? and isDisplay = 1";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, uid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                count = rs.getInt("address_count");
            }

        } catch (Exception e) {

        }
        return count;
    }

    public Address getByUserIdAndAddress(int uid, String city, String district, String ward, String address) {
        String sql = "select * from address where uid = ? and city = ? and district = ? and commune = ? and address = ?";
        Address a = null;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, uid);
            st.setString(2, city);
            st.setString(3, district);
            st.setString(4, ward);
            st.setString(5, address);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                a = new Address(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(8), rs.getString(9), rs.getString(7), rs.getBoolean(6), rs.getBoolean(10));
            }
            return a;
        } catch (Exception e) {

        }
        return null;
    }

    public boolean DeleteAddress(int aid) {
        try {
            String sql = "UPDATE address SET [isDisplay] = 0 WHERE aid = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, aid);
            int row = st.executeUpdate();
            if (row > 0) {
                return true;
            }
        } catch (Exception e) {

        }
        return false;
    }

    public static void main(String[] args) {
        AddressDAO d = new AddressDAO();
        boolean address = d.setDefault(32);
        System.out.println(address);

    }

}
