/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Size;
import utils.Helpers;

public class SizeDAO extends DBContext {

    private String reason = "SizeDAO -> ";

    public void update(List<Size> s, int pid) {

        try {
            String sql = "UPDATE [dbo].[Product_Size] SET [quantity] = ? ,[price] = ? WHERE pid = ? and sid = ?";

            if (!s.isEmpty()) {
                for (Size item : s) {
                    PreparedStatement ps = connection.prepareStatement(sql);
                    ps.setInt(1, item.getQuantity());
                    ps.setDouble(2, item.getPrice());
                    ps.setInt(3, pid);
                    ps.setInt(4, item.getSid());

                    ps.executeUpdate();
                }
            }

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "update()", e);
        }
    }

    public boolean checkSizeNameExisted(String name) {
        try {
            String sql = "SELECT * FROM Sizes WHERE sname like ?";

            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + name + "%");

            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                return true;
            }

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "addSizeInfomation()", e);
        }
        return false;
    }

    public void addSizeInfomation(Size s) {
        try {
            String sql = "INSERT INTO [dbo].[Sizes] ([sName]) VALUES (?)";

            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, s.getsName());

            st.executeUpdate();

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "addSizeInfomation()", e);
        }
    }

    public void add(Size s, int pid) {

        try {
            String sql = "INSERT INTO [dbo].[Product_Size] ([pid] ,[sid] ,[quantity] ,[price])\n"
                    + "VALUES	(?, ? ,? ,?)";

            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, pid);
            ps.setInt(2, s.getSid());
            ps.setInt(3, s.getQuantity());
            ps.setDouble(4, s.getPrice());

            ps.executeUpdate();

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "add()", e);
        }

    }

    public List<Size> getAll() {
        List<Size> list = new ArrayList<>();

        try {
            String q = "SELECT  s.[sid] ,[sName]"
                    + "  FROM [dbo].[Sizes] s";

            PreparedStatement p = connection.prepareStatement(q);

            ResultSet r = p.executeQuery();

            while (r.next()) {
                Size s = new Size(r.getInt(1), r.getString(2), 0, 0);
                list.add(s);
            }

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "getSize()", e);
        }

        return list;
    }

    public List<Size> getNotExist(List<Size> compare) {
        List<Size> list = new ArrayList<>();

        try {
            String q = "SELECT distinct s.[sid] ,[sName] FROM [dbo].[Sizes] s WHERE 1 = 1 ";

            if (!compare.isEmpty()) {
                q += " AND sid not in (";
                for (int i = 0; i < compare.size(); i++) {
                    if (i == compare.size() - 1) {
                        q += compare.get(i).getSid() + " )";
                    } else {
                        q += compare.get(i).getSid() + ", ";
                    }
                }
            }

            PreparedStatement p = connection.prepareStatement(q);

            ResultSet r = p.executeQuery();

            while (r.next()) {
                Size s = new Size(r.getInt(1), r.getString(2), 0, 0);
                list.add(s);
            }

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "getSize()", e);
        }

        return list;
    }

    public void remove(int sid) {
        try {
            String q = "DELETE FROM [dbo].[Product_Size] WHERE sid = ?\n"
                    + "\n"
                    + "DELETE FROM [dbo].[Sizes] WHERE sid = ?";

            PreparedStatement p = connection.prepareStatement(q);

            p.setInt(1, sid);
            p.setInt(2, sid);
            
            p.executeUpdate();

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "remove()", e);
        }
    }

}
