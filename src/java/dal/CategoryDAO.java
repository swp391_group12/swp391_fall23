/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Category;
import utils.Helpers;

public class CategoryDAO extends DBContext {

    private String reason = "CategoryDAO -> ";

    public List<Category> getAll() {
        List<Category> list = new ArrayList<>();

        try {
            String q = "SELECT * FROM [dbo].[Categories]";

            PreparedStatement p = connection.prepareStatement(q);

            ResultSet r = p.executeQuery();

            while (r.next()) {

                Category c = new Category(r.getInt("cid"), r.getString("cName"), r.getBoolean("cStatus"));

                list.add(c);
            }

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "getAll()", e);
        }

        return list;
    }
}
