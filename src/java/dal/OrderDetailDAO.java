/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Image;
import model.OrderDetail;
import model.OrderDetailDTO;
import model.Product;

/**
 *
 * @author Trung Kien
 */
public class OrderDetailDAO extends DBContext implements GenericDAO<OrderDetail, Integer> {

    @Override
    public boolean create(OrderDetail data) {
        String sql = "insert into OrderDetail(quantity, psid, pid, oid) values(?,?,?,?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setFloat(1, data.getQuantity());
            ps.setInt(2, data.getProductSizeId());
            ps.setInt(3, data.getProductId());
            ps.setInt(4, data.getOrderId());

            return ps.executeUpdate() != 0;
        } catch (SQLException ex) {
            Logger.getLogger(StoreDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean update(OrderDetail data) {
        return false;
    }

    @Override
    public boolean deleteById(Integer id) {
        return false;
    }

    @Override
    public OrderDetail getById(Integer id) {

        return new OrderDetail();
    }
    public OrderDetail getById1(Integer id) {
        try {

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public List<OrderDetailDTO> getDetailById1(String id) {
        try {
            List<OrderDetailDTO> list = new ArrayList<>();
            String sql = "  select * from OrderDetail d join Products p on d.pid = p.pId\n"
                    + " join Product_Image i on i.pid = p.pId\n"
                    + "               where d.oid = " + id;
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                OrderDetailDTO d = new OrderDetailDTO();
                d.setOrderDetailId(rs.getInt(1));
                d.setQuantity(rs.getInt(2));
                Product p = new Product();
                Image i = new Image();
                i.setiUrl(rs.getString("iUrl"));
                p.setPid(rs.getInt("pId"));
                p.setpName(rs.getString("pName"));
                p.setpBrand(rs.getString("pBrand"));
                p.setPrice(rs.getDouble("price"));
                p.setImage(i);
                d.setProduct(p);
                list.add(d);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<OrderDetail> findAll() {
        return new ArrayList<>();
    }
    
    public List<OrderDetail> findAllByOrderCode(int orderId) {
        List<OrderDetail> orderDetails = new ArrayList<>();
        String sql = "select * from OrderDetail od left join Products p on od.pid = p.pId where od.oid = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, orderId);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                Product product = Product.builder()
                        .pid(rs.getInt("pId"))
                        .pName(rs.getString("pName"))
                        .pDetail(rs.getString("pDetail"))
                        .pBrand(rs.getString("pBrand"))
                        .pSKU(rs.getString("pSKU"))
                        .price(rs.getDouble("price"))
                        .build();
                OrderDetail orderDetail = OrderDetail.builder()
                        .orderDetailId(rs.getInt("odid"))
                        .quantity(rs.getInt("quantity"))
                        .product(product)
                        .build();
                orderDetails.add(orderDetail);
            }
        } catch (Exception ex) {
            
        }
        return orderDetails;
    }

}
