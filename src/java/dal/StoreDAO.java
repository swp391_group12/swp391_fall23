package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Status;
import model.Store;
import utils.Helpers;

public class StoreDAO extends DBContext implements GenericDAO<Store, Integer> {

    private String reason = "StoreDAO -> ";

    public List<Store> getAll() {
        List<Store> list = new ArrayList<>();

        try {
            String q = "SELECT [stoId]\n"
                    + "      ,[stoName]\n"
                    + "      ,[stoAddress]\n"
                    + "      ,[stoPhone]\n"
                    + "      ,[stoTimeOpen]\n"
                    + "      ,[stoTimeClosed]\n"
                    + "      ,sto.[stid]\n"
                    + "	     ,[stName]\n"
                    + "  FROM [dbo].[Stores] sto inner join [dbo].[Status] st ON sto.stid = st.stid where sto.deleted = 0";

            PreparedStatement p = connection.prepareStatement(q);

            ResultSet r = p.executeQuery();

            while (r.next()) {

                Status st = new Status(r.getInt("stid"), r.getString("stName"));

                Store sto = new Store(r.getInt("stoId"), r.getString("stoName"), r.getString("stoAddress"), r.getString("stoPhone"),
                        r.getString("stoTimeOpen"), r.getString("stoTimeClosed"), st);
                list.add(sto);
            }

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "getAll()", e);
        }

        return list;
    }
    public Store getStoreById(int id) {

        try {
            String q = "SELECT [stoId]\n"
                    + "      ,[stoName]\n"
                    + "      ,[stoAddress]\n"
                    + "      ,[stoPhone]\n"
                    + "      ,[stoTimeOpen]\n"
                    + "      ,[stoTimeClosed]\n"
                    + "      ,sto.[stid]\n"
                    + "	     ,[stName]\n"
                    + "  FROM [dbo].[Stores] sto inner join [dbo].[Status] st ON sto.stid = st.stid where sto.stoId = ? ";

            PreparedStatement p = connection.prepareStatement(q);
            p.setInt(1, id);
            ResultSet r = p.executeQuery();
            
            while (r.next()) {

                Status st = new Status(r.getInt("stid"), r.getString("stName"));

                Store sto = new Store(r.getInt("stoId"), r.getString("stoName"), r.getString("stoAddress"), r.getString("stoPhone"),
                        r.getString("stoTimeOpen"), r.getString("stoTimeClosed"), st);
                return sto;
            }

        } catch (SQLException e) {
            Helpers.ErrorMessage(reason + "getAll()", e);
        }

        return null;
    }

    @Override
    public boolean create(Store data) {
        String sql = "insert into Stores(stoName,stoAddress,stoPhone,stoTimeOpen,stoTimeClosed,stid,deleted) values(?,?,?,?,?,?,0)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, data.getStoName());
            ps.setString(2, data.getStoAddress());
            ps.setString(3, data.getStoPhone());
            ps.setString(4, data.getStoTimeOpen());
            ps.setString(5, data.getStoTimeClosed());
            ps.setInt(6, data.getStatus().getStid());

            return ps.executeUpdate() != 0;
        } catch (SQLException ex) {
            Logger.getLogger(StoreDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean update(Store data) {
        String sql = "update Stores set stoName=?, stoAddress=?, stoPhone=?, stoTimeOpen=?, stoTimeClosed=?, stid=? where stoId=?";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, data.getStoName());
            ps.setString(2, data.getStoAddress());
            ps.setString(3, data.getStoPhone());
            ps.setString(4, data.getStoTimeOpen());
            ps.setString(5, data.getStoTimeClosed());
            ps.setInt(6, data.getStatus().getStid());
            ps.setInt(7, data.getStoid());

            return ps.executeUpdate() != 0;
        } catch (SQLException ex) {
            Logger.getLogger(StoreDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean deleteById(Integer id) {

        String sql = "update Stores set deleted = 1 where stoId=?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            return ps.executeUpdate() != 0;
        } catch (SQLException ex) {
            Logger.getLogger(StoreDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public List<Store> findAll() {
        String sql = "select * from Stores s left join Status st on s.stid = st.stid where deleted = 0";
        List<Store> list = new ArrayList<>();
        try {

            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Store store = new Store(
                        rs.getInt("stoId"),
                        rs.getString("stoName"),
                        rs.getString("stoAddress"),
                        rs.getString("stoPhone"),
                        rs.getString("stoTimeOpen"),
                        rs.getString("stoTimeClosed"),
                        new Status(rs.getInt("stid"), rs.getString("stName"))
                );
                list.add(store);
            }
        } catch (SQLException ex) {
            Logger.getLogger(StoreDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public Store getById(Integer id) {
        String sql = "select * from Stores s left join Status st on s.stid = st.stid where s.stoId = ? and deleted = 0";
        Store store = new Store();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                store = new Store(
                        rs.getInt("stoId"),
                        rs.getString("stoName"),
                        rs.getString("stoAddress"),
                        rs.getString("stoPhone"),
                        rs.getString("stoTimeOpen"),
                        rs.getString("stoTimeClosed"),
                        new Status(rs.getInt("stid"), rs.getString("stName"))
                );
            }
        } catch (SQLException ex) {
            Logger.getLogger(StoreDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return store;
    }
}
