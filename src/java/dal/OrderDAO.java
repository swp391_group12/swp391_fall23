/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import controller.Home.productDetail;
import dto.OrderDTO;
import dto.ReportRevenueDTO;
import dto.UserDTO;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Address;
import model.Order;
import model.OrderDetail;
import model.Product;
import model.Status;
import model.User;

/**
 *
 * @author Trung Kien
 */
public class OrderDAO extends DBContext implements GenericDAO<Order, Integer> {

    public ArrayList<OrderDTO> getOrderByUid(int uid) {

        ArrayList<OrderDTO> list = new ArrayList<>();
        String sql = "  Select *, a.[address] AS addressDetail from [Orders] o left JOIN [address] a ON a.aid = o.[aid] \n"
                + "   WHERE o.[uid] = " + uid;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                OrderDTO o = new OrderDTO(rs.getInt(1), rs.getDouble(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6), rs.getDate(7), rs.getInt(8), rs.getInt(9));
                Address address = Address.builder()
                        .address(rs.getString("addressDetail"))
                        .name(rs.getString("name"))
                        .addressId(rs.getInt("aid"))
                        .city(rs.getString("city"))
                        .district(rs.getString("district"))
                        .commune(rs.getString("commune"))
                        .phone(rs.getString("phone"))
                        .userId(rs.getInt("uid"))
                        .build();
                o.setAddressModel(address);
                o.setDeliveryDate(rs.getDate("deliveryDate"));
                o.setOrderCode(rs.getString("orderCode"));
                list.add(o);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public boolean create(Order data) {
        StatusDAO statusDAO = new StatusDAO();
        Status status = statusDAO.getByName("Waiting_Confirm");
        String sql = "insert into Orders(orderCode,totalPrice,note,phone,name,aid,createDate,uid,stoId,stid,totalPriceIn) values(?,?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, data.getOrderCode());
            ps.setBigDecimal(2, data.getTotalPrice());
            ps.setString(3, data.getNote() == null ? "" : data.getNote());
            ps.setString(4, data.getPhone());
            ps.setString(5, data.getName());
            ps.setInt(6, data.getAddressId());
            ps.setDate(7, Date.valueOf(LocalDate.now()));
            ps.setInt(8, data.getUserId());
            ps.setInt(9, data.getStoId());
            ps.setInt(10, status != null ? status.getStid() : null);
            ps.setBigDecimal(11, data.getTotalPriceIn());

            return ps.executeUpdate() != 0;
        } catch (SQLException ex) {
            Logger.getLogger(StoreDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean update(Order data) {
        return false;
    }

    public boolean update(String orderCode, int statusId, int sellerId) {
        String sql = "update Orders set stid = ?, sellerId = ? where orderCode = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, statusId);
            ps.setInt(2, sellerId);
            ps.setString(3, orderCode);
            boolean result = ps.executeUpdate() != 0;
            Order order = this.getByOrderCode(orderCode);
            if (result && statusId == 8) {
                handleCancle(order.getOrderId());
            }
            return result;
        } catch (Exception ex) {

            Logger.getLogger(StoreDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean update(String orderCode, int statusId, int sellerId, Date deliveryDate) {
        String sql = "update Orders set stid = ?, sellerId = ?, deliveryDate = ? where orderCode = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, statusId);
            ps.setInt(2, sellerId);
            ps.setDate(3, deliveryDate);
            ps.setString(4, orderCode);

            return ps.executeUpdate() != 0;
        } catch (SQLException ex) {

            Logger.getLogger(StoreDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean cancel(String oid) {
        String sql = "update Orders set stid = 8 where oid = " + oid;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            boolean result = ps.executeUpdate() != 0;
            if (result) {
                handleCancle(Integer.parseInt(oid));
            }
            return result;
        } catch (Exception ex) {
            Logger.getLogger(StoreDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    private void handleCancle(int oid) {
        OrderDetailDAO orderDetailDAO = new OrderDetailDAO();
        ProductDAO productDAO = new ProductDAO();
        List<OrderDetail> orderDetails = orderDetailDAO.findAllByOrderCode(oid);
        for (OrderDetail od : orderDetails) {
            Product product = productDAO.getProductById(od.getProduct().getPid());
            product.setQuantity(product.getQuantity() + od.getQuantity());
            productDAO.update(product);
        }

    }

    @Override
    public boolean deleteById(Integer id) {
        return false;
    }

    @Override
    public Order getById(Integer id) {
        return new Order();
    }

    public Order getByOrderCode(String oderCode) {
        String sql = "select *, a.address as addressDetail from Orders o "
                + "left join address a on o.aid = a.aid "
                + "left join Status s on o.stid = s.stid "
                + "where orderCode=?";
        Order order = new Order();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, oderCode);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Address address = Address.builder()
                        .address(rs.getString("addressDetail"))
                        .city(rs.getString("city"))
                        .commune(rs.getString("commune"))
                        .district(rs.getString("district"))
                        .addressId(rs.getInt("aid"))
                        .build();
                Status status = Status.builder()
                        .stid(rs.getInt("stid"))
                        .stName(rs.getString("stName"))
                        .stType(rs.getString("stType"))
                        .stNameVi(rs.getString("stNameVi"))
                        .build();
                order = Order.builder()
                        .orderId(rs.getInt("oid"))
                        .totalPrice(rs.getBigDecimal("totalPrice"))
                        .note(rs.getString("note"))
                        .phone(rs.getString("phone"))
                        .name(rs.getString("name"))
                        .addressId(rs.getInt("aid"))
                        .userId(rs.getInt("uid"))
                        .status(rs.getInt("stid"))
                        .address(address)
                        .statusModel(status)
                        .orderCode(rs.getString("orderCode"))
                        .createDate(rs.getDate("createDate"))
                        .deliveryDate(rs.getDate("deliveryDate"))
                        .build();
            }
        } catch (SQLException ex) {
            Logger.getLogger(StoreDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return order;
    }

    @Override
    public List<Order> findAll() {
        List<Order> list = new ArrayList<>();
        String sql = " Select * from Orders o left "
                + "join address a on o.aid = a.aid "
                + "left join Status s on o.stid = s.stid "
                + "where sellerId is null";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Address address = Address.builder()
                        .address(rs.getString("address"))
                        .name(rs.getString("name"))
                        .addressId(rs.getInt("aid"))
                        .city(rs.getString("city"))
                        .district(rs.getString("district"))
                        .commune(rs.getString("commune"))
                        .phone(rs.getString("phone"))
                        .userId(rs.getInt("uid"))
                        .build();
                Order order = Order.builder()
                        .orderId(rs.getInt("oid"))
                        .address(address)
                        .addressId(rs.getInt("aid"))
                        .totalPrice(rs.getBigDecimal("totalPrice"))
                        .note(rs.getString("note"))
                        .name(rs.getString("name"))
                        .phone(rs.getString("phone"))
                        .createDate(rs.getDate("createDate"))
                        .orderCode(rs.getString("orderCode"))
                        .status(rs.getInt("stid"))
                        .deliveryDate(rs.getDate("deliveryDate"))
                        .statusModel(new Status(rs.getInt("stid"), rs.getString("stName"), rs.getString("stNameVi"), rs.getString("stType")))
                        .build();
                list.add(order);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Order> findAll(Integer stoId) {
        List<Order> list = new ArrayList<>();
        String sql = " Select *, a.address as addressDetail from Orders o left "
                + "join address a on o.aid = a.aid "
                + "left join Status s on o.stid = s.stid "
                + "where sellerId is null "
                + "and (? = -1 or stoId = ?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, stoId == null ? -1 : stoId);
            ps.setInt(2, stoId == null ? -1 : stoId);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Address address = Address.builder()
                        .address(rs.getString("addressDetail"))
                        .name(rs.getString("name"))
                        .addressId(rs.getInt("aid"))
                        .city(rs.getString("city"))
                        .district(rs.getString("district"))
                        .commune(rs.getString("commune"))
                        .phone(rs.getString("phone"))
                        .userId(rs.getInt("uid"))
                        .build();
                Order order = Order.builder()
                        .orderId(rs.getInt("oid"))
                        .address(address)
                        .addressId(rs.getInt("aid"))
                        .totalPrice(rs.getBigDecimal("totalPrice"))
                        .note(rs.getString("note"))
                        .name(rs.getString("name"))
                        .phone(rs.getString("phone"))
                        .createDate(rs.getDate("createDate"))
                        .orderCode(rs.getString("orderCode"))
                        .status(rs.getInt("stid"))
                        .deliveryDate(rs.getDate("deliveryDate"))
                        .statusModel(new Status(rs.getInt("stid"), rs.getString("stName"), rs.getString("stNameVi"), rs.getString("stType")))
                        .build();
                list.add(order);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Order> findAllForAdmin(String status) {
        List<Order> list = new ArrayList<>();
        String sql = " Select *, a.address as addressDetail from Orders o left "
                + "join address a on o.aid = a.aid "
                + "left join Status s on o.stid = s.stid "
                + "left join Users u on o.sellerId = u.uid "
                + "where 1 = 1 "
                + "and (? is null or s.stName = ?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, status);
            ps.setString(2, status);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Address address = Address.builder()
                        .address(rs.getString("addressDetail"))
                        .name(rs.getString("name"))
                        .addressId(rs.getInt("aid"))
                        .city(rs.getString("city"))
                        .district(rs.getString("district"))
                        .commune(rs.getString("commune"))
                        .phone(rs.getString("phone"))
                        .userId(rs.getInt("uid"))
                        .build();
                User seller = User.builder()
                        .userName((rs.getString("username")))
                        .build();
                Order order = Order.builder()
                        .orderId(rs.getInt("oid"))
                        .address(address)
                        .addressId(rs.getInt("aid"))
                        .totalPrice(rs.getBigDecimal("totalPrice"))
                        .note(rs.getString("note"))
                        .name(rs.getString("name"))
                        .phone(rs.getString("phone"))
                        .createDate(rs.getDate("createDate"))
                        .orderCode(rs.getString("orderCode"))
                        .status(rs.getInt("stid"))
                        .seller(seller)
                        .deliveryDate(rs.getDate("deliveryDate"))
                        .statusModel(new Status(rs.getInt("stid"), rs.getString("stName"), rs.getString("stNameVi"), rs.getString("stType")))
                        .build();
                list.add(order);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Order> findAllBySeller(Integer sellerId, String status) {
        List<Order> list = new ArrayList<>();
        String sql = " Select *, a.address as addressDetail from Orders o left "
                + "join address a on o.aid = a.aid "
                + "left join Status s on o.stid = s.stid "
                + "where sellerId = ? "
                + "and (? is null or s.stName = ?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, sellerId);
            ps.setString(2, status);
            ps.setString(3, status);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Address address = Address.builder()
                        .address(rs.getString("addressDetail"))
                        .name(rs.getString("name"))
                        .addressId(rs.getInt("aid"))
                        .city(rs.getString("city"))
                        .district(rs.getString("district"))
                        .commune(rs.getString("commune"))
                        .phone(rs.getString("phone"))
                        .userId(rs.getInt("uid"))
                        .build();
                Order order = Order.builder()
                        .orderId(rs.getInt("oid"))
                        .address(address)
                        .addressId(rs.getInt("aid"))
                        .totalPrice(rs.getBigDecimal("totalPrice"))
                        .note(rs.getString("note"))
                        .name(rs.getString("name"))
                        .phone(rs.getString("phone"))
                        .createDate(rs.getDate("createDate"))
                        .orderCode(rs.getString("orderCode"))
                        .status(rs.getInt("stid"))
                        .deliveryDate(rs.getDate("deliveryDate"))
                        .statusModel(new Status(rs.getInt("stid"), rs.getString("stName"), rs.getString("stNameVi"), rs.getString("stType")))
                        .build();
                list.add(order);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public int countAll() {
        String sql = "select count(*) as total from Orders";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt("total");
            }
        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;

    }
    
    
   public int countorderStore(int stoId) {
    String sql = "SELECT COUNT(*) AS total FROM Orders WHERE stoId = ?";
    try (PreparedStatement ps = connection.prepareStatement(sql)) {
        ps.setInt(1, stoId);
        try (ResultSet rs = ps.executeQuery()) {
            if (rs.next()) {
                return rs.getInt("total");
            }
        }
    } catch (SQLException ex) {
        Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
    }
    return 0;
}


    public List<Order> getNewOrders(int limit) {
        List<Order> orders = new ArrayList<>();
        String sql = "select * from Orders o left "
                + "join Status s on o.stid = s.stid "
                + "order by o.createDate desc "
                + "OFFSET 0 ROWS FETCH NEXT ? ROWS ONLY;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, limit);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Order order = Order.builder()
                        .orderId(rs.getInt("oid"))
                        .addressId(rs.getInt("aid"))
                        .totalPrice(rs.getBigDecimal("totalPrice"))
                        .note(rs.getString("note"))
                        .name(rs.getString("name"))
                        .phone(rs.getString("phone"))
                        .createDate(rs.getDate("createDate"))
                        .orderCode(rs.getString("orderCode"))
                        .status(rs.getInt("stid"))
                        .deliveryDate(rs.getDate("deliveryDate"))
                        .statusModel(new Status(rs.getInt("stid"), rs.getString("stName"), rs.getString("stNameVi"), rs.getString("stType")))
                        .build();
                orders.add(order);
            }

        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return orders;
    }
    
    
   public List<Order> getNewOrdersStore(int stoId, int limit) {
    List<Order> orders = new ArrayList<>();
    String sql = "SELECT * FROM Orders o " +
                 "LEFT JOIN Status s ON o.stid = s.stid " +
                 "WHERE stoId = ? " +
                 "ORDER BY o.createDate DESC " +
                 "OFFSET 0 ROWS FETCH NEXT ? ROWS ONLY";
    try {
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setInt(1, stoId);
        ps.setInt(2, limit);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Order order = Order.builder()
                    .orderId(rs.getInt("oid"))
                    .addressId(rs.getInt("aid"))
                    .totalPrice(rs.getBigDecimal("totalPrice"))
                    .note(rs.getString("note"))
                    .name(rs.getString("name"))
                    .phone(rs.getString("phone"))
                    .createDate(rs.getDate("createDate"))
                    .orderCode(rs.getString("orderCode"))
                    .status(rs.getInt("stid"))
                    .deliveryDate(rs.getDate("deliveryDate"))
                    .statusModel(new Status(
                            rs.getInt("stid"),
                            rs.getString("stName"),
                            rs.getString("stNameVi"),
                            rs.getString("stType")))
                    .build();
            orders.add(order);
        }
    } catch (SQLException ex) {
        Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
    }
    return orders;
}


    public UserDTO getTotalOrderSeller(int uid, int year, int month) {
        UserDTO u = null;
        String sql = null;
        switch (uid) {
            case 0:
                sql = "SELECT\n"
                        + "    COUNT(*) AS total_orders,\n"
                        + "    SUM(totalPrice) AS total_price\n"
                        + "FROM Orders\n"
                        + "WHERE MONTH(createDate) = ? and YEAR(createDate) = ? and stid =7";
                try {
                    PreparedStatement ps = connection.prepareStatement(sql);
                    ps.setInt(1, month);
                    ps.setInt(2, year);
                    ResultSet rs = ps.executeQuery();
                    if (rs.next()) {
                        u = new UserDTO(uid, rs.getDouble("total_price"), rs.getInt("total_orders"));
                        return u;
                    }
                } catch (Exception e) {
                }
                break;
            default:
                sql = "SELECT\n"
                        + "    COUNT(*) AS total_orders,\n"
                        + "    sellerId,\n"
                        + "    SUM(totalPrice) AS total_price\n"
                        + "FROM Orders\n"
                        + "WHERE MONTH(createDate) = ? and YEAR(createDate) = ? and [sellerId] = ? and stid = 7\n"
                        + "GROUP BY sellerId;";
                try {
                    PreparedStatement ps = connection.prepareStatement(sql);
                    ps.setInt(1, month);
                    ps.setInt(2, year);
                    ps.setInt(3, uid);
                    ResultSet rs = ps.executeQuery();
                    if (rs.next()) {
                        u = new UserDTO(uid, rs.getDouble("total_price"), rs.getInt("total_orders"));
                        return u;
                    }
                } catch (Exception e) {
                }
                break;
        }

        return u;
    }

    public UserDTO getTotalOrderCustomer(int uid, int year, int month) {
        UserDTO u = null;
        try {
            String sql = "SELECT\n"
                    + "    COUNT(*) AS total_orders,\n"
                    + "    uid,\n"
                    + "    SUM(totalPrice) AS total_price\n"
                    + "FROM Orders\n"
                    + "WHERE MONTH(createDate) = ? and YEAR(createDate) = ? and [uid] = ? and stid = 7\n"
                    + "GROUP BY uid;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, month);
            ps.setInt(2, year);
            ps.setInt(3, uid);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                u = new UserDTO(uid, rs.getDouble("total_price"), rs.getInt("total_orders"), 0);
            }
            try {
                String sql1 = "SELECT COUNT(*) AS total_orders_cancel FROM Orders WHERE MONTH(createDate) = ? and YEAR(createDate) = ? and [uid] = ? and stid = 8 GROUP BY uid;";
                PreparedStatement ps1 = connection.prepareStatement(sql1);
                ps1.setInt(1, month);
                ps1.setInt(2, year);
                ps1.setInt(3, uid);
                ResultSet rs1 = ps1.executeQuery();
                if (rs1.next()) {
                    u.setTotalOrderCancel(rs1.getInt("total_orders_cancel"));
                    return u;
                }
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }
        return u;
    }

    public List<Order> getOderById(int uid) {
        List<Order> d = new ArrayList<>();
        StoreDAO st = new StoreDAO();
        try {
            String sql = " select o.oid, o.orderCode, o.totalPrice, o.createDate,o.stid,o.stoId,o.deliveryDate,o.totalPriceIn, s.[stid]\n"
                    + "      ,s.[stName]\n"
                    + "      ,s.[stType]\n"
                    + "      ,s.[stNameVi]\n"
                    + "   from [Orders] o join [Status] s on o.stid = s.stid where o.uid = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, uid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Status status = new Status(rs.getInt("stid"), rs.getString("stName"), rs.getString("stNameVi"), rs.getString("stType"));
                d.add(new Order(
                        rs.getInt("oid"), rs.getBigDecimal("totalPrice"),
                        null, null, null, null, rs.getDate("createDate"),
                        uid, rs.getInt("stid"), rs.getInt("stoId"),
                        rs.getString("orderCode"), 0, 0, null, status,
                        null, rs.getDate("deliveryDate"), rs.getBigDecimal("totalPriceIn")
                ));
            }
            return d;
        } catch (Exception e) {

        }
        return null;
    }

    public ReportRevenueDTO getReportRevenue() {
        String sql = "select sum(o.totalPrice) as totalPrice, sum(o.totalPriceIn) as totalPriceIn "
                + "from Orders o left join Status s on o.stid = s.stid "
                + "where s.stName != 'Order_Cancel'";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return ReportRevenueDTO.builder()
                        .revenueOut(rs.getBigDecimal("totalPrice"))
                        .revenueIn(rs.getBigDecimal("totalPriceIn"))
                        .build();
            }
        } catch (SQLException ex) {

        }
        return null;
    }

    public static void main(String[] args) {
        OrderDAO d = new OrderDAO();
        List<Order> ld = d.getOderById(29);
        for (Order order : ld) {
            System.out.println(order);
        }
    }

}
