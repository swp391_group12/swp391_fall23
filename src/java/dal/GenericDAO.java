/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;


import java.util.List;

/**
 *
 * @author Trung Kien
 */
public interface GenericDAO<T,ID> {
    boolean create(T data);
    boolean update(T data);
    boolean deleteById(ID id);
    T getById(ID id);
    List<T> findAll();
}
