/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import model.Role;
import model.User;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;
import model.Status;
import model.Store;
import utils.Helpers;

/**
 *
 * @author Admin
 */
public class UserDAO extends DBContext {

    public boolean addUser(User a) {
        try {

            String sql = "INSERT INTO [dbo].[Users] ([userName] , [email] , [password] , [phone] ,[rid], [stoId], [status])\n"
                    + "VALUES\n"
                    + "		(?, ?, ?, ?, ?, ?, ?)";

            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, a.getUserName());
            st.setString(2, a.getEmail());
            st.setString(3, convertPassToMD5(a.getPass()));
            st.setString(4, a.getPhone());
            st.setInt(5, a.getRole().getRoleId());
            if (a.getStore() == null) {
                st.setNull(6, java.sql.Types.INTEGER);
            } else {
                st.setInt(6, a.getStore().getStoid());
            }
            st.setBoolean(7, true);
            st.executeUpdate();
            return true;

        } catch (SQLException e) {
            System.out.println();
        }
        return false;
    }

    public List<User> findAllUser(int rid) {
        List<User> u = new ArrayList<>();
        int xid = 0;
        try {
            String sql = "SELECT DISTINCT u.[uid]\n"
                    + "      ,u.[userName]\n"
                    + "      ,u.[email]\n"
                    + "      ,u.[password]\n"
                    + "      ,u.[phone]\n"
                    + "      ,u.[rid]\n"
                    + "      ,u.[stoId]\n"
                    + "      ,u.[status]\n"
                    + "       ,r.[rName]\n"
                    + "  FROM [Users] u join Roles r on u.rid = r.rid where r.rid = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, rid);
            ResultSet rs = st.executeQuery();
            StoreDAO d = new StoreDAO();
            while (rs.next() && rs.getInt(1) != xid) {
                xid = rs.getInt(1);
                Role role = new Role(rs.getInt("rid"), rs.getString("rName"));
                if (rs.getInt("stoId") != 0) {
                    Store store = d.getStoreById(rs.getInt("stoId"));
                    u.add(new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(5), rs.getString(4), role, store, rs.getBoolean("status")));
                } else {
                    u.add(new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(5), rs.getString(4), role, null, rs.getBoolean("status")));
                }
            }
            return u;
        } catch (Exception e) {
        }
        return null;
    }

    public List<User> findAllUserByShopId(int stoId) {
        List<User> users = new ArrayList<>();
        String sql = "select * from Users u where u.stoId = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, stoId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                User user = User.builder()
                        .userId(rs.getInt("uid"))
                        .userName(rs.getString("userName"))
                        .email(rs.getString("email"))
                        .phone(rs.getString("phone"))
                        .status(rs.getBoolean("status"))
                        .build();
                users.add(user);
            }
        } catch (SQLException e) {
            Logger.getLogger(StoreDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return users;
    }

    public List<Role> findAllRole() {
        List<Role> u = new ArrayList<>();
        try {
            String sql = "SELECT [rid]\n"
                    + "      ,[rName]\n"
                    + "  FROM [dbo].[Roles]";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Role role = new Role(rs.getInt("rid"), rs.getString("rName"));
                u.add(role);
            }
            return u;
        } catch (Exception e) {
        }
        return null;

    }

    public boolean isEmailExists(String email) {
        boolean emailExists = false;
        try {
            String sql = "SELECT COUNT(*) FROM [Users] WHERE [email] = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                int count = rs.getInt(1);
                emailExists = (count > 0);
            }
        } catch (Exception e) {
        }
        return emailExists;
    }

    public User getUserById(int id) {
        User u = null;
        try {
            String sql = "SELECT DISTINCT u.[uid]\n"
                    + "      ,u.[userName]\n"
                    + "      ,u.[email]\n"
                    + "      ,u.[password]\n"
                    + "      ,u.[phone]\n"
                    + "      ,u.[rid]\n"
                    + "      ,u.[stoId]\n"
                    + "      ,u.[status]\n"
                    + "       ,r.[rName]\n"
                    + "  FROM [Users] u join Roles r on u.rid = r.rid where u.uid = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            Store store = new Store();
            StoreDAO d = new StoreDAO();
            if (rs.next()) {
                Role role = new Role(rs.getInt("rid"), rs.getString("rName"));
                if (rs.getInt("stoId") != 0) {
                    store = d.getStoreById(rs.getInt("stoId"));
                    u = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(5), rs.getString(4), role, store, rs.getBoolean("status"));
                } else {
                    u = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(5), rs.getString(4), role, null, rs.getBoolean("status"));
                }
            }
            return u;
        } catch (Exception e) {

        }
        return null;
    }

    public User getUserByEmail(String email) {
        User u = null;
        try {
            String sql = "SELECT DISTINCT u.[uid]\n"
                    + "      ,u.[userName]\n"
                    + "      ,u.[email]\n"
                    + "      ,u.[password]\n"
                    + "      ,u.[phone]\n"
                    + "      ,u.[rid]\n"
                    + "      ,u.[stoId]\n"
                    + "      ,u.[status]\n"
                    + "       ,r.[rName]\n"
                    + "  FROM [Users] u join Roles r on u.rid = r.rid where u.email = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            Store store = new Store();
            StoreDAO d = new StoreDAO();
            if (rs.next()) {
                Role role = new Role(rs.getInt("rid"), rs.getString("rName"));
                if (rs.getInt("stoId") != 0) {
                    store = d.getStoreById(rs.getInt("stoId"));
                    u = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(5), rs.getString(4), role, store, rs.getBoolean("status"));
                } else {
                    u = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(5), rs.getString(4), role, null, rs.getBoolean("status"));
                }
            }
            return u;
        } catch (Exception e) {

        }
        return null;
    }

    public User login(String email, String pass) {
        User u = null;
        try {
            String sql = "SELECT DISTINCT \n"
                    + "    u.[uid],\n"
                    + "    u.[userName],\n"
                    + "    u.[email],\n"
                    + "    u.[password],\n"
                    + "    u.[phone],\n"
                    + "    u.[rid],\n"
                    + "    u.[stoId],\n"
                    + "    u.[status],\n"
                    + "    r.[rName]\n"
                    + "FROM \n"
                    + "    [Users] u \n"
                    + "    JOIN Roles r ON u.rid = r.rid \n"
                    + "WHERE \n"
                    + "    u.email = ? AND u.password = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            st.setString(2, convertPassToMD5(pass));
            ResultSet rs = st.executeQuery();
            Store store = new Store();
            StoreDAO d = new StoreDAO();
            if (rs.next()) {
                Role role = new Role(rs.getInt("rid"), rs.getString("rName"));
                if (rs.getInt("stoId") != 0) {
                    store = d.getStoreById(rs.getInt("stoId"));
                    u = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(5), rs.getString(4), role, store, rs.getBoolean("status"));
                } else {
                    u = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(5), rs.getString(4), role, null, rs.getBoolean("status"));
                }
            }
            return u;
        } catch (Exception e) {

        }
        return null;
    }

    public boolean UpdateUser(User u, int num) {
        try {

            // Cập nhật thông tin người dùng
            String sql1 = "UPDATE Users "
                    + "SET userName = ?, "
                    + "email = ?, "
                    + "password = ?, "
                    + "phone = ?, "
                    + "rid = ?,  "
                    + "stoId = ?,  "
                    + "status = ?  "
                    + "WHERE uid = ?";
            PreparedStatement st = connection.prepareStatement(sql1);
            st.setString(1, u.getUserName());
            st.setString(2, u.getEmail());
            switch (num) {
                case 1:
                    st.setString(3, convertPassToMD5(u.getPass()));
                    break;
                case 2:
                    st.setString(3, u.getPass());
                    break;
            }
            st.setString(4, u.getPhone());
            st.setInt(5, u.getRole().getRoleId());
            if (u.getStore() != null) {
                st.setInt(6, u.getStore().getStoid());
            } else {
                st.setNull(6, java.sql.Types.INTEGER);
            }
            st.setBoolean(7, u.isStatus());
            st.setInt(8, u.getUserId());
            int updated1 = st.executeUpdate();
            if (updated1 > 0) {
                return true;
            }
            // Lấy ID của địa chỉ

        } catch (SQLException e) {
            e.printStackTrace();

        }
        return false;
    }

    public Role getRoleById(int id) {
        Role r = null;
        try {
            String sql = "Select * from roles where rid = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            rs.next();
            r = new Role(rs.getInt(1), rs.getString(2));
            return r;
        } catch (Exception e) {

        }
        return null;
    }

    public String getEmailById(int id) {
        String email = "";
        try {
            String sql = "select email from users where uid = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            rs.next();
            email = rs.getString(1);
            return email;
        } catch (Exception e) {

        }
        return null;
    }

    public boolean deleteUser(int uid) {
        try {
            connection.setAutoCommit(false);
            String sql = "Delete from Address where uid = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, uid);
            int rowsDeleted = st.executeUpdate();

            String sql2 = "Delete from Users where uid = ?";
            PreparedStatement st2 = connection.prepareStatement(sql2);
            st2.setInt(1, uid);
            int rowsDeleted2 = st2.executeUpdate();
            if (rowsDeleted2 > 0) {
                connection.commit();
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public Status getStatus(int stid) {
        try {

            String q = "SELECT * FROM [dbo].[Status] WHERE stid = ?";

            PreparedStatement p = connection.prepareStatement(q);
            p.setInt(1, stid);

            ResultSet r = p.executeQuery();

            if (r.next()) {
                return new Status(r.getInt(1), r.getString(2));
            }

        } catch (SQLException e) {

        }

        return null;
    }

    public static void main(String[] args) {
        UserDAO u = new UserDAO();
        List<User> l = u.findAllUser(2);
        for (User user : l) {
            System.out.println(user);

        }
    }

    public static String convertPassToMD5(String password) {
        MessageDigest convert = null;

        try {

            convert = MessageDigest.getInstance("MD5");

        } catch (NoSuchAlgorithmException e) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, e);
        }

        convert.update(password.getBytes());
        byte[] passwordByte = convert.digest();
        return DatatypeConverter.printHexBinary(passwordByte);
    }

    public int countAllUser() {
        String sql = "select count(*) as total from Users s where s.rid = 1";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt("total");
            }
        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public List<User> findNewUsers(int limit) {
        List<User> users = new ArrayList<>();
        String sql = "select * from Users u where u.rid = 1 order by u.uid desc OFFSET 0 ROWS FETCH NEXT ? ROWS ONLY;";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, limit);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                User user = User.builder()
                        .userId(rs.getInt("uid"))
                        .userName(rs.getString("username"))
                        .email(rs.getString("email"))
                        .phone(rs.getString("phone"))
                        .build();
                users.add(user);
            }
        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return users;
    }

}
