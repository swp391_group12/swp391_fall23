/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller.Shopping;

import dal.ProductDAO;
import dal.ProductSizeDAO;
import dto.ProductDisplayCartDTO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;
import model.ProductSize;
import model.User;
import utils.JsonService;
import utils.ListUtils;
import utils.MessageEnum;
import utils.MessageUtils;
import utils.StringUtils;

/**
 *
 * @author Trung Kien
 */
@WebServlet(
        name = "AddorCheckRedirectController",
        urlPatterns = {
            "/AddorCheckRedirectController", "/AddorCheckRedirectController/increase",
            "/AddorCheckRedirectController/descrease", "/AddorCheckRedirectController/removeProduct"
        }
)
public class AddorCheckRedirectController extends HttpServlet {

    private final ProductDAO productDao = new ProductDAO();
    private final ProductSizeDAO productSizeDAO = new ProductSizeDAO();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");
        //them parameter action
        String action = req.getParameter("action");
        // chuyen edit va remove qua post
        switch (action) {
            case "editQuantity":
//                editQuantityOfProduct(req, resp);
                break;
            case "remove":
                removeProduct(req, resp);
                break;
            default:
                processRequest(req, resp, "", 1);
                break;
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");
        String path = req.getServletPath();
        try {
            switch (path) {
                case "/AddorCheckRedirectController/increase":
                    updateQuantityCard(req, resp, false);
                    break;
                case "/AddorCheckRedirectController/descrease":
                    updateQuantityCard(req, resp, true);
                    break;
                default:
                    processRequest(req, resp, "", 1);
                    break;
            }
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response, String quantityInCart, int flag)
            throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");
        String productId = request.getParameter("productId");
        String action = request.getParameter("action");

        if (StringUtils.isBlank(productId)) {
            MessageUtils.showMessage(request, MessageEnum.PRODUCT_NOT_FOUND);
            redirectCurrentPage(request, response);
            return;
        }
        String quantity = "";

        if ("".equals(quantityInCart)) {
            quantity = request.getParameter("quantity");
        } else {
            quantity = quantityInCart;
        }
        float quantityNum = "".equals(quantity) ? 1 : Float.parseFloat(quantity);
        Product product = productDao.getProductById(Integer.parseInt(productId));
        if (product.getQuantity() < quantityNum) {
            MessageUtils.showMessage(request, MessageEnum.NOT_ENOUGH_QUANTITY);
            redirectCurrentPage(request, response);
            return;
        }

        Cookie cookieProduct = null;
        Cookie cookieNumOfProduct = null;
        Cookie cookieTotalPrice = null;

        double totalPrice = 0;
        List<ProductDisplayCartDTO> productsCart = new ArrayList<>();
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("account");
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("products")) {
                try {
                    String json = JsonService.base64ToString(cookie.getValue());
                    productsCart.addAll(JsonService.parseJsonToList(json, ProductDisplayCartDTO.class));
                    cookieProduct = cookie;
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(AddorCheckRedirectController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (cookie.getName().equals("totalPrice")) {
                totalPrice = Double.parseDouble(cookie.getValue());
                cookieTotalPrice = cookie;
            }
        }
        if (!checkTheSameCityProduct(request, productsCart)) {
            MessageUtils.showMessage(request, MessageEnum.PRODUCT_MUST_THE_SAME_STORE);
            redirectCurrentPage(request, response);
            return;
        }
        if (productsCart.isEmpty() || !ListUtils.isExistedProductCart(productsCart, Integer.parseInt(productId))) {
            ProductDisplayCartDTO pdcdto = new ProductDisplayCartDTO(product);
            pdcdto.setQuantity(quantityNum);
            pdcdto.setTotal(product.getPrice() * quantityNum);
            productsCart.add(pdcdto);

            totalPrice += product.getPrice() * quantityNum;
        } else {
            totalPrice = 0;
            for (ProductDisplayCartDTO data : productsCart) {
                if (data.getPid() == Integer.parseInt(productId)) {
                    if ("replace".equals(action)) {
                        data.setQuantity(quantityNum);
                        quantityNum -= data.getQuantity();
                    } else {
                        data.setQuantity(data.getQuantity() + quantityNum);
                    }
                }
                totalPrice += data.getPrice() * data.getQuantity();
            }
        }
        String data = JsonService.toString(productsCart);
        String context = JsonService.stringToBase64(data);
        cookieProduct = cookieProduct != null ? cookieProduct : new Cookie("products", context);
        cookieProduct.setHttpOnly(true);
        cookieProduct.setPath("/");
        cookieProduct.setMaxAge(60 * 60 * 24);
        cookieProduct.setValue(context);
        response.addCookie(cookieProduct);

        cookieNumOfProduct = cookieNumOfProduct != null ? cookieNumOfProduct : new Cookie("numOfProducts", String.valueOf(productsCart.size()));
        cookieNumOfProduct.setHttpOnly(true);
        cookieNumOfProduct.setMaxAge(60 * 60 * 24);
        cookieNumOfProduct.setPath("/");
        cookieNumOfProduct.setValue(String.valueOf(productsCart.size()));
        response.addCookie(cookieNumOfProduct);

        totalPrice = formatDouble(totalPrice);
        cookieTotalPrice = cookieTotalPrice != null ? cookieTotalPrice : new Cookie("totalPrice", String.valueOf(totalPrice));
        cookieTotalPrice.setHttpOnly(true);
        cookieTotalPrice.setMaxAge(60 * 60 * 24);
        cookieTotalPrice.setPath("/");
        cookieTotalPrice.setValue(String.valueOf(totalPrice));
        response.addCookie(cookieTotalPrice);

        if (flag == 1) {
            decreaseQuantityProduct(Integer.valueOf(productId), quantityNum);
        }
        redirectCurrentPage(request, response);
    }

    private void updateQuantityCard(HttpServletRequest request, HttpServletResponse response, boolean isDescrease) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        int productId = -1;
        try {
            productId = Integer.parseInt(request.getParameter("productId"));
        } catch (Exception ex) {

        }

        Product product = productDao.getProductById(productId);
        List<ProductDisplayCartDTO> pdcdtos = new ArrayList<>();

        Cookie cookieProduct = null;
        Cookie cookieTotalPrice = null;
        double totalPrice = 0;
        User user = null;
        if (user == null) {
            Cookie[] cookies = request.getCookies();
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("products")) {
                    try {
                        String json = JsonService.base64ToString(cookie.getValue());
                        pdcdtos.addAll(JsonService.parseJsonToList(json, ProductDisplayCartDTO.class));
                        cookieProduct = cookie;
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(AddorCheckRedirectController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                if (cookie.getName().equals("totalPrice")) {
                    totalPrice = Double.parseDouble(cookie.getValue());
                    cookieTotalPrice = cookie;
                }
            }

            DecimalFormat decimalFormat = new DecimalFormat("#.#");
            for (ProductDisplayCartDTO p : pdcdtos) {
                if (isDescrease) {
                    if (p.getPid() == productId && p.getQuantity() > 0) {
                        p.setQuantity(Float.valueOf(decimalFormat.format(p.getQuantity() - 0.1)));
                        totalPrice -= p.getPrice() * 0.1;
                        increaseQuantityProduct(productId, Float.parseFloat(decimalFormat.format(0.1)));
                    }
                } else {
                    if (p.getPid() == productId) {
                        if (product.getQuantity() > 0) {
                            totalPrice += p.getPrice() * 0.1;
                            p.setQuantity(Float.valueOf(decimalFormat.format(p.getQuantity() + 0.1)));
                            decreaseQuantityProduct(productId, Float.parseFloat(decimalFormat.format(0.1)));
                        } else {
                            MessageUtils.showMessage(request, MessageEnum.NOT_ENOUGH_QUANTITY);
                        }
                    }
                }
            }
            String data = JsonService.toString(pdcdtos);
            String context = JsonService.stringToBase64(data);
            cookieProduct = cookieProduct != null ? cookieProduct : new Cookie("products", context);
            cookieProduct.setValue(context);
            cookieProduct.setHttpOnly(true);
            cookieProduct.setPath("/");
            cookieProduct.setMaxAge(60 * 60 * 24);
            response.addCookie(cookieProduct);

            totalPrice = formatDouble(totalPrice);
            cookieTotalPrice = cookieTotalPrice != null ? cookieTotalPrice : new Cookie("totalPrice", String.valueOf(totalPrice));
            cookieTotalPrice.setHttpOnly(true);
            cookieTotalPrice.setMaxAge(60 * 60 * 24);
            cookieTotalPrice.setPath("/");
            cookieTotalPrice.setValue(String.valueOf(totalPrice));
            response.addCookie(cookieTotalPrice);
            redirectCurrentPage(request, response);
        }
    }

    private void increaseQuantityProduct(int productId, float quantity) {
        Product product = productDao.getProductById(productId);
        product.setQuantity(product.getQuantity() + quantity);
        productDao.update(product);
    }

    private void decreaseQuantityProduct(int productId, float quantity) {
        Product product = productDao.getProductById(productId);
        product.setQuantity(product.getQuantity() - quantity);
        productDao.update(product);
    }

    private void removeProduct(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String productId = request.getParameter("productId");
        String quantity = request.getParameter("quantity");
        String contextPath = request.getContextPath();

        User userAccountDTO = null;
        int num = 0;
        Cookie[] cookies = request.getCookies();
        List<ProductDisplayCartDTO> pdcdtos = new ArrayList<>();
        Cookie cookieProduct = null;
        Cookie cookieNumOfProduct = null;
        Cookie cookieTotalPrice = null;

        double totalPrice = 0;
        increaseQuantityProduct(Integer.parseInt(productId), Float.parseFloat(quantity));
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("products")) {
                try {
                    String json = JsonService.base64ToString(cookie.getValue());
                    pdcdtos.addAll(JsonService.parseJsonToList(json, ProductDisplayCartDTO.class));
                    cookieProduct = cookie;
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(AddorCheckRedirectController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (cookie.getName().equals("numOfProducts")) {
                num = Integer.parseInt(cookie.getValue()) - 1;
                cookieNumOfProduct = cookie;
            }
            if (cookie.getName().equals("totalPrice")) {
                totalPrice = Double.parseDouble(cookie.getValue());
                cookieTotalPrice = cookie;
            }

        }
        if (userAccountDTO == null) {

            cookieNumOfProduct = cookieNumOfProduct != null ? cookieNumOfProduct : new Cookie("numOfProducts", String.valueOf(num));
            cookieNumOfProduct.setHttpOnly(true);
            cookieNumOfProduct.setMaxAge(60 * 60 * 24);
            cookieNumOfProduct.setPath("/");
            cookieNumOfProduct.setValue(String.valueOf(num));
            response.addCookie(cookieNumOfProduct);

            for (ProductDisplayCartDTO data : pdcdtos) {
                if (data.getPid() == Integer.parseInt(productId)) {
                    totalPrice -= formatDouble(data.getPrice() * data.getQuantity());
                }
            }

            pdcdtos = ListUtils.removeByPId(pdcdtos, Integer.parseInt(productId));
            String data = JsonService.toString(pdcdtos);
            String context = JsonService.stringToBase64(data);
            cookieProduct = cookieProduct != null ? cookieProduct : new Cookie("products", context);
            cookieProduct.setHttpOnly(true);
            cookieProduct.setMaxAge(60 * 60 * 24);
            cookieProduct.setPath("/");
            cookieProduct.setValue(context);
            response.addCookie(cookieProduct);
            totalPrice = formatDouble(totalPrice);

            cookieTotalPrice = cookieTotalPrice != null ? cookieTotalPrice : new Cookie("totalPrice", String.valueOf(totalPrice));
            cookieTotalPrice.setHttpOnly(true);
            cookieTotalPrice.setMaxAge(60 * 60 * 24);
            cookieTotalPrice.setPath("/");
            cookieTotalPrice.setValue(String.valueOf(totalPrice));
            response.addCookie(cookieTotalPrice);
            response.sendRedirect(contextPath + "/shopping-cart");
        }
    }

    private void redirectCurrentPage(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String path = request.getParameter("path");
        String context = request.getContextPath();
        String alert = (String) request.getAttribute("alert");
        String message = (String) request.getAttribute("message");

        switch (path.trim()) {
            case "home":
                response.sendRedirect("/home");
                break;
            case "single": {
                String curProductId = request.getParameter("curProductId");
                String temp = "/singleproduct?productId=" + curProductId;
                response.sendRedirect(temp);
                break;
            }
            case "cart":
                if (!StringUtils.isBlank(alert)) {
                    response.sendRedirect(context + "/shopping-cart?alert=" + alert + "&message=" + message);
                } else {
                    response.sendRedirect(context + "/shopping-cart");
                }
                break;
            case "shop":

                if (!StringUtils.isBlank(alert)) {
                    response.sendRedirect(context + "/shop?alert=" + alert + "&message=" + message);
                } else {
                    response.sendRedirect(context + "/shop");
                }
                break;
            case "productDetail": {
                if (!StringUtils.isBlank(alert)) {
                    String productId = request.getParameter("productId");
                    response.sendRedirect(context + "/productDetail?pid=" + productId + "&alert=" + alert + "&message=" + message);
                } else {
                    response.sendRedirect(context + "/shopping-cart");
                }
                break;
            }
            default: {
                response.sendRedirect(context + "/shop");
                break;
            }
        }
    }

    private boolean checkTheSameCityProduct(HttpServletRequest request, List<ProductDisplayCartDTO> pdcdtos) {
        if (pdcdtos.isEmpty()) {
            return true;
        }
        Cookie[] cookies = request.getCookies();
        String selectedCity = null;
        for (Cookie cookie : cookies) {
            if ("selectedCity".equals(cookie.getName())) {
                selectedCity = cookie.getValue();
                break;
            }
        }
        int stiId = Integer.parseInt(selectedCity);
        for (ProductDisplayCartDTO pdcdto : pdcdtos) {
            if (pdcdto.getStore().getStoid() != stiId) {
                return false;
            }
        }
        return true;
    }

    public double formatDouble(double in) {
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        return Double.parseDouble(decimalFormat.format(in));
    }
}
