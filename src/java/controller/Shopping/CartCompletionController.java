/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller.Shopping;

import dal.AddressDAO;
import dal.OrderDAO;
import dal.OrderDetailDAO;
import dal.ProductSizeDAO;
import dto.ProductDisplayCartDTO;
import dto.StoresDTO;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Order;
import model.OrderDetail;
import model.ProductSize;
import model.User;
import service.StoreService;
import jakarta.servlet.http.HttpSession;
import model.Address;
import service.interfaces.IStoreService;
import utils.JsonService;
import utils.MailUtils;
import utils.MessageEnum;
import utils.MessageUtils;
import utils.StringUtils;

/**
 *
 * @author Trung Kien
 */
@WebServlet(name = "CartCompletionController", urlPatterns = {"/cartCompletion"})
public class CartCompletionController extends HttpServlet {

    private final IStoreService iStoreService = new StoreService();
    private final OrderDAO orderDAO = new OrderDAO();
    private final OrderDetailDAO orderDetailDAO = new OrderDetailDAO();
    private final ProductSizeDAO productSizeDAO = new ProductSizeDAO();
    private final AddressDAO addressDAO = new AddressDAO();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");
        int storeId = Integer.parseInt(req.getParameter("storeId"));
        String fullname = req.getParameter("fullname");
        String email = req.getParameter("email");
        String phone = req.getParameter("phone");
        String addressId = req.getParameter("addressId");
        String address = req.getParameter("address");
        String note = req.getParameter("note");
        String productsOrder = req.getParameter("productsOrder");
        String city = req.getParameter("cityText");
        String district = req.getParameter("districtText");
        String ward = req.getParameter("wardText");
        List<StoresDTO> storesDTOss = iStoreService.getListStores();

        req.setAttribute("listStores", storesDTOss);
        List<ProductDisplayCartDTO> pdcdtos = new ArrayList<>();
        try {
            String ordersString = JsonService.base64ToString(productsOrder);
            pdcdtos.addAll(JsonService.parseJsonToList(ordersString, ProductDisplayCartDTO.class));
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CartCompletionController.class.getName()).log(Level.SEVERE, null, ex);
        }

        double sum = 0;
        double sumIn = 0;
        for (ProductDisplayCartDTO productDisplayCartDTO : pdcdtos) {
            double totalOnProduct = productDisplayCartDTO.getPrice() * productDisplayCartDTO.getQuantity();
            double totalInOnProduct = productDisplayCartDTO.getPriceIn() * productDisplayCartDTO.getQuantity();
            productDisplayCartDTO.setTotal(totalOnProduct);
            sum += totalOnProduct;
            sumIn += totalInOnProduct;
        }
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("account");

        if (user == null) {
            resp.sendRedirect(req.getContextPath() + "/login");
            return;
        }
        if (pdcdtos.isEmpty()) {
            List<StoresDTO> storesDTOs = iStoreService.getListStores();
            MessageUtils.showMessage(req, MessageEnum.CART_EMPTY);
            List<Address> addresses = addressDAO.getAddressByUid(user.getUserId());
            Address addressDefault = getAddressDefault(addresses);
            req.setAttribute("addresses", addresses);
            req.setAttribute("addressDefault", addressDefault);
            RequestDispatcher rd = req.getRequestDispatcher("checkoutCart.jsp");
            req.setAttribute("stores", storesDTOs);
            req.setAttribute("productDisplayCartDTOList", pdcdtos);
            req.setAttribute("total", sum);
            rd.forward(req, resp);
            return;
        }
        if (storeId == -1) {
            List<StoresDTO> storesDTOs = iStoreService.getListStores();
            MessageUtils.showMessage(req, MessageEnum.NO_CHOOSE_STORE_DELIVERY);
            List<Address> addresses = addressDAO.getAddressByUid(user.getUserId());
            Address addressDefault = getAddressDefault(addresses);
            req.setAttribute("addresses", addresses);
            req.setAttribute("addressDefault", addressDefault);
            RequestDispatcher rd = req.getRequestDispatcher("checkoutCart.jsp");
            req.setAttribute("stores", storesDTOs);
            req.setAttribute("productDisplayCartDTOList", pdcdtos);
            req.setAttribute("total", sum);
            rd.forward(req, resp);
            return;
        }
        Address addressModel = null;
        if (StringUtils.isBlank(city) && StringUtils.isBlank(addressId)) {
            List<StoresDTO> storesDTOs = iStoreService.getListStores();
            MessageUtils.showMessage(req, MessageEnum.ADDRESS_IS_BLANK);
            List<Address> addresses = addressDAO.getAddressByUid(user.getUserId());
            Address addressDefault = getAddressDefault(addresses);
            req.setAttribute("addresses", addresses);
            req.setAttribute("addressDefault", addressDefault);
            RequestDispatcher rd = req.getRequestDispatcher("checkoutCart.jsp");
            req.setAttribute("stores", storesDTOs);
            req.setAttribute("productDisplayCartDTOList", pdcdtos);
            req.setAttribute("total", sum);
            rd.forward(req, resp);
            return;
        }
        if (!StringUtils.isBlank(city)) {
            addressModel = Address.builder()
                    .userId(user.getUserId())
                    .name(fullname)
                    .address(address)
                    .city(city)
                    .district(district)
                    .phone(phone)
                    .commune(ward)
                    .build();
            addressDAO.addNewAddress(addressModel);
            addressModel = addressDAO.getByUserIdAndAddress(user.getUserId(), city, district, ward, address);
        }

        String orderCode = StringUtils.genOrderCode();
        Order order = Order.builder()
                .orderCode(orderCode)
                .totalPrice(BigDecimal.valueOf(sum))
                .totalPriceIn(BigDecimal.valueOf(sumIn))
                .name(fullname)
                .note(note)
                .phone(phone)
                .stoId(storeId)
                .addressId(addressModel != null ? addressModel.getAddressId() : Integer.parseInt(addressId))
                .userId(user.getUserId())
                .build();
        orderDAO.create(order);
        order = orderDAO.getByOrderCode(orderCode);
        MailUtils mailUtils = new MailUtils(email);
        Address addr = addressDAO.getAddressByid(order.getAddressId());
        StoresDTO store = iStoreService.getById(storeId);
        mailUtils.sendMailBilling(order, pdcdtos, fullname, String.format("%s, %s, %s, %s", addr.getAddress(), addr.getCommune(), addr.getDistrict(), addr.getCity()), store);
        for (ProductDisplayCartDTO pdcdto : pdcdtos) {
            OrderDetail orderDetail = OrderDetail.builder()
                    .quantity(pdcdto.getQuantity())
                    .productId(pdcdto.getPid())
                    .orderId(order.getOrderId())
                    .build();
            orderDetailDAO.create(orderDetail);

            Cookie[] cookies = req.getCookies();
            for (Cookie cookie : cookies) {
                switch (cookie.getName()) {
                    case "products": {
                        List<ProductDisplayCartDTO> pdcdtos1 = new ArrayList<>();
                        String data = JsonService.toString(pdcdtos1);
                        String context = JsonService.stringToBase64(data);
                        cookie.setPath("/");
                        cookie.setValue(context);
                        cookie.setHttpOnly(true);
                        cookie.setMaxAge(0);
                        resp.addCookie(cookie);
                    }
                    case "totalPrice": {
                        cookie.setValue(String.valueOf(0));
                        cookie.setPath("/");
                        cookie.setHttpOnly(true);
                        cookie.setMaxAge(0);
                        resp.addCookie(cookie);
                    }
                    case "numOfProducts": {
                        cookie.setValue(String.valueOf(0));
                        cookie.setPath("/");
                        cookie.setHttpOnly(true);
                        cookie.setMaxAge(0);
                        resp.addCookie(cookie);
                    }
                }
            }
            req.setAttribute("orderCode", orderCode);
        }

        RequestDispatcher rd = req.getRequestDispatcher("cartCompletion.jsp");
        rd.forward(req, resp);
    }

    private Address getAddressDefault(List<Address> addresses) {
        for (Address a : addresses) {
            if (a.isDefaut()) {
                addresses.remove(a);
                return a;
            }
        }
        return null;
    }

}
