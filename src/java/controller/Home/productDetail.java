/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Home;

import dal.FeedbackDAO;
import dal.ProductDAO;
import dto.BLogDTO;
import dto.BlogCategoryDTO;
import dto.StoresDTO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import model.Product;
import service.StoreService;
import service.interfaces.IStoreService;

@WebServlet(name = "productDetail", urlPatterns = {"/productDetail"})
public class productDetail extends HttpServlet {

    private final IStoreService iStoreService = new StoreService();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet productDetail</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet productDetail at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            List<StoresDTO> storesDTOs = iStoreService.getListStores();

            request.setAttribute("listStores", storesDTOs);
            String index = request.getParameter("index") == null ? "1" : request.getParameter("index");

            String pid = request.getParameter("pid");
            ProductDAO pd = new ProductDAO();
            Product p = pd.getProductById(Integer.parseInt(pid));
            String image = String.valueOf(p.getImage());
            String name = p.getpName();
            request.setAttribute("productDetail", p);

            int total = new FeedbackDAO().countTotalFBbyPID(pid);
            int numberPage = (int) Math.ceil((double) total / 6);
            int currentPage = Integer.parseInt(index);
            request.setAttribute("numberPage", numberPage);
            request.setAttribute("avgStar", new FeedbackDAO().avgTotalFBbyPID(pid));
            request.setAttribute("listFb", new FeedbackDAO().getListFeedbackByPid(pid, 6, currentPage));
            request.getRequestDispatcher("shop/detailProduct.jsp").forward(request, response);
        } catch (Exception e) {
            processRequest(request, response);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
