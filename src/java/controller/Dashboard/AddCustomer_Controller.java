 /*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Dashboard;

import dal.CustomerDAO;
import dal.UserDAO;
import model.Role;
import model.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.util.List;
import model.Customer;
import utils.SendMail;
/**
 *
 * @author Admin
 */
public class AddCustomer_Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
request.getRequestDispatcher("/user/AddCustomer.jsp").forward(request, response);
        }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            processRequest(request, response);   
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String Name = request.getParameter("name");
        String email = request.getParameter("email");
        String phone = request.getParameter("sdt");
        String pass = SendMail.generateRandomPassword();
        
        UserDAO d = new UserDAO();
        boolean exists = d.isEmailExists(email);
        String msg = "";
        
        if(!utils.ValidateData.isValidName(Name)){
            msg = "Tên không hợp lệ, vui lòng nhập lại";
            request.setAttribute("msg", msg);
            request.setAttribute("sdt", phone);
            request.setAttribute("email", email);
            request.getRequestDispatcher("/user/AddCustomer.jsp").forward(request, response);
            return;
        }
        
        if(!utils.ValidateData.isValidEmail(email)){
            msg = "Email không hợp lệ, vui lòng nhập lại";
            request.setAttribute("msg", msg);
            request.setAttribute("name", Name);
            request.setAttribute("sdt", phone);
            request.getRequestDispatcher("/user/AddCustomer.jsp").forward(request, response);
            return;
        }
        
        if(exists){
           msg = "Email đã tồn tại vui lòng kiểm tra lại";
           request.setAttribute("msg", msg);
           request.setAttribute("name", Name);
           request.setAttribute("sdt", phone);
           request.getRequestDispatcher("/user/AddCustomer.jsp").forward(request, response);
           return;
        }
        
        if(Name != null && phone != null && email != null){
            Role role = d.getRoleById(1);
            User u = new User(-1, Name, email, phone, pass, role, null, true);
            d.addUser(u);
            SendMail.sendMail(email, 2, pass);
            msg = "Thêm mới thành công";
            request.setAttribute("msg", msg);
            request.getRequestDispatcher("/user/AddCustomer.jsp").forward(request, response);
        }
        
       
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
