/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller.Dashboard;

import dto.StatusDTO;
import dto.StoresDTO;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import service.StatusService;
import service.StoreService;
import service.interfaces.IStatusService;
import service.interfaces.IStoreService;
import utils.StringUtils;

/**
 *
 * @author Trung Kien
 */
@WebServlet(name = "ManagerStoreController", value = "/manager-stores")
public class StoreController extends HttpServlet {

    private final IStoreService iStoreService = new StoreService();

    private final IStatusService iStatusService = new StatusService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        String action = req.getParameter("action");
        String stoName = req.getParameter("stoName");
        String stoAddress = req.getParameter("stoAddress");
        String stoPhone = req.getParameter("stoPhone");
        String timeOpen = req.getParameter("stoTimeOpen");
        String timeClose = req.getParameter("stoTimeClose");
        String statusStr = req.getParameter("statusId");
        Integer statusId = StringUtils.isBlank(statusStr) ? null : Integer.parseInt(statusStr);
        switch (action) {
            case "add-store": {
                StoresDTO storeDTO = new StoresDTO(stoName, stoAddress, stoPhone, timeOpen, timeClose, statusId);
                iStoreService.insertStore(storeDTO);
                break;
            }
            case "update-store": {
                int stoId = Integer.parseInt(req.getParameter("stoId"));
                StoresDTO storeDTO = new StoresDTO(stoName, stoAddress, stoPhone, timeOpen, timeClose, statusId);
                storeDTO.setStoId(stoId);
                iStoreService.updateStore(storeDTO);
                break;
            }
            case "delete-store":
                int stoId = Integer.parseInt(req.getParameter("stoId"));
                iStoreService.deleteById(stoId);
                break;
            default:
                break;
        }
        resp.sendRedirect(req.getContextPath() + "/manager-stores");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        String action = req.getParameter("action");
        if (null == action) {
            List<StoresDTO> storesDTOs = iStoreService.getListStores();

            req.setAttribute("listStores", storesDTOs);

            RequestDispatcher rd = req.getRequestDispatcher("stores.jsp");
            rd.forward(req, resp);
        } else {
            switch (action) {
                case "add-store": {
                    List<StatusDTO> statusDTOs = iStatusService.findAll();
                    req.setAttribute("statuses", statusDTOs);
                    RequestDispatcher rd = req.getRequestDispatcher("addStore.jsp");
                    rd.forward(req, resp);
                    break;
                }
                case "update-store": {
                    int stoId = Integer.parseInt(req.getParameter("storeId"));
                    List<StatusDTO> statusDTOs = iStatusService.findAll();
                    req.setAttribute("statuses", statusDTOs);
                    StoresDTO storeDto = iStoreService.getById(stoId);
                    req.setAttribute("statuses", statusDTOs);
                    req.setAttribute("store", storeDto);
                    RequestDispatcher rd = req.getRequestDispatcher("updateStore.jsp");
                    rd.forward(req, resp);
                    break;
                }
                default:
                    break;
            }
        }
    }

}
