/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Dashboard;

import dal.OrderDAO;
import dal.UserDAO;
import dto.UserDTO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.User;
import java.time.LocalDate;

/**
 *
 * @author Admin
 */
public class SellerDetails_Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SellerDetails_Controller</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SellerDetails_Controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String uid = request.getParameter("userId");
        int userid = Integer.parseInt(uid);
        int y, m;
        String year = request.getParameter("year");
        String month = request.getParameter("month");

        LocalDate currentDate = LocalDate.now();
        int currentMonth = currentDate.getMonthValue();
        int currentYear = currentDate.getYear();

        if (year == null && month == null) {
            y = currentYear;
            m = currentMonth;
        } else {
            y = Integer.parseInt(year);
            m = Integer.parseInt(month);
        }
        UserDAO d = new UserDAO();
        OrderDAO o = new OrderDAO();

        User u = d.getUserById(userid);

        UserDTO getAll = o.getTotalOrderSeller(0, y, m);
        UserDTO getByUid = o.getTotalOrderSeller(userid, y, m);
        if (getAll != null && getByUid != null) {
            double tld = (int) (getByUid.getTotalOrder() * 100 / (double) getAll.getTotalOrder());
            double tlt = (int) (getByUid.getTotalPrice() * 100 / getAll.getTotalPrice());
            request.setAttribute("tld", tld);
            request.setAttribute("tlt", tlt);

        } else {
            request.setAttribute("tld", 0);
            request.setAttribute("tlt", 0);
            getByUid = new UserDTO(userid, 0, 0);
        }
        request.setAttribute("m", m);
        request.setAttribute("y", y);
        request.setAttribute("total", getAll);
        request.setAttribute("one", getByUid);
        request.setAttribute("u", u);
        request.getRequestDispatcher("user/SellerDetails.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
