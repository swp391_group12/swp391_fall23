/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller.Dashboard;

import dal.ProductDAO;
import dal.UserDAO;
import dto.StatusDTO;
import dto.StoresDTO;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import model.Product;
import model.User;
import service.StatusService;
import service.StoreService;
import service.interfaces.IStatusService;
import service.interfaces.IStoreService;

/**
 *
 * @author Trung Kien
 */
@WebServlet(name = "StoreDetailController", value = "/store-detail")
public class StoreDetail extends HttpServlet {

    private final IStoreService iStoreService = new StoreService();
    private final IStatusService iStatusService = new StatusService();
    private final UserDAO userDao = new UserDAO();
    private final ProductDAO productDao = new ProductDAO();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int stoId = Integer.parseInt(req.getParameter("storeId"));
        List<StatusDTO> statusDTOs = iStatusService.findAll();
        List<User> users = userDao.findAllUserByShopId(stoId);
        List<Product> products = productDao.getProductByStoreId(stoId);
        req.setAttribute("statuses", statusDTOs);
        StoresDTO storeDto = iStoreService.getById(stoId);
        req.setAttribute("statuses", statusDTOs);
        req.setAttribute("users", users);
        req.setAttribute("store", storeDto);
        req.setAttribute("products", products);
        RequestDispatcher rd = req.getRequestDispatcher("storeDetail.jsp");
        rd.forward(req, resp);
    }

}
