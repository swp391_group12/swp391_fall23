/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Dashboard;

import dal.StoreDAO;
import dal.UserDAO;
import model.Role;
import model.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Store;

/**
 *
 * @author Admin
 */
public class UpdateSeller_Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdateSeller_Controller</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdateSeller_Controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String sid = request.getParameter("userId");
        int id = Integer.parseInt(sid);
        UserDAO d = new UserDAO();
        User u = d.getUserById(id);
        List<Role> r = d.findAllRole();
        request.setAttribute("role", r);
        request.setAttribute("u", u);
        request.getRequestDispatcher("/user/UpdateSeller.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String xid = request.getParameter("id");
        int id = Integer.parseInt(xid);
        String status = request.getParameter("status");
        
        UserDAO d = new UserDAO();
        StoreDAO sto = new StoreDAO();
        
        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        
        boolean b = true;
        if(status.equals("false")){
            b = false;
        }
        
        User s = d.getUserById(id);

        s.setUserName(name);
        s.setPhone(phone);
        s.setStatus(b);
        d.UpdateUser(s, 2);
        request.getRequestDispatcher("seller").forward(request, response);
        
       
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
