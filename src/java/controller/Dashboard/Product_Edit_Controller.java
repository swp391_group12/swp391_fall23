/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Dashboard;

import dal.ProductDAO;
import dal.SizeDAO;
import jakarta.servlet.ServletContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
import java.nio.file.Paths;
import java.util.List;
import model.Image;
import model.Product;
import model.Size;
import utils.Helpers;

@MultipartConfig
public class Product_Edit_Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Product_Edit_Controller</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Product_Edit_Controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int pid = Helpers.parseInt(request.getParameter("pid"), 0);

        ProductDAO pdao = new ProductDAO();

        Product p = pdao.getProductById(pid);
        String url = request.getServletPath();

        request.setAttribute("data", p);
        request.setAttribute("url", url);
        if ("/view-product".equals(url)) {
            request.setAttribute("noEdit", "noEdit");
        }

        request.getRequestDispatcher("product/detail.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        String url = action.replace("/", "");
        int pid = Helpers.parseInt(request.getParameter("pid"), 0);
        String pName = request.getParameter("pName");
        int cid = Helpers.parseInt(request.getParameter("cid"), 0);
        int stid = Helpers.parseInt(request.getParameter("stid"), 0);

        ProductDAO pdao = new ProductDAO();

        Product p = pdao.getProductById(pid);

        switch (url) {
            case "product-listing":
                p.setpName(pName);
                p.getCategory().setCid(cid);
                p.getStatus().setStid(stid);
                break;
            case "product-edit":
                url += "?pid=" + pid;
                p.setpName(pName);
                p.getCategory().setCid(cid);
                p.getStatus().setStid(stid);
                String pDetail = request.getParameter("pDetail");
                String pBrand = request.getParameter("pBrand");
                String pSKU = request.getParameter("pSKU");
                String quantity = request.getParameter("quantity");
                String price = request.getParameter("price");
                String priceIn = request.getParameter("priceIn");
               
                if (!price.isEmpty()) {
                    p.setPrice(Helpers.parseDouble(price, p.getPrice()));
                }

                if (!priceIn.isEmpty()) {
                    p.setPriceIn(Helpers.parseDouble(priceIn, p.getPriceIn()));
                }
                if (!quantity.isEmpty()) {
                    p.setQuantity(Helpers.parseFloat(quantity, p.getQuantity()));
                }
               
                p.setpDetail(pDetail);
                p.setpBrand(pBrand);
                p.setpSKU(pSKU);

                // image
                Part path = request.getPart("ImageUpload");
                String removeImageTag = request.getParameter("removeImage");
                String imageURL = p.getImage().getiUrl();

                if (removeImageTag != null && !removeImageTag.isEmpty()) {
                    imageURL = ""; // Hoặc imageURL = null;
                }

                if (path != null && path.getSize() > 0) {
                    String filename = Paths.get(path.getSubmittedFileName()).getFileName().toString();
                    String uploadDirectory = request.getServletContext().getRealPath("/img/product");
                    String newFilePath = uploadDirectory + File.separator + filename;
                    path.write(newFilePath);
                    imageURL = filename;
                }

                Image newImage = new Image(p.getImage().getIid(), imageURL);
                p.setImage(newImage);

                break;
        }

        pdao.update(p);

        response.sendRedirect(url);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
