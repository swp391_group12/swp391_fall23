/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.Dashboard;

import dal.OrderDAO;
import dal.ProductDAO;
import dal.UserDAO;
import dto.ReportRevenueDTO;
import dto.StoresDTO;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Order;
import model.User;
import service.StoreService;
import service.interfaces.IStoreService;

/**
 *
 * @author phank
 */
@WebServlet(name="Sellerdashboard", urlPatterns={"/Sellerdashboard"})
public class Sellerdashboard extends HttpServlet {
   
    private final UserDAO userDao = new UserDAO();
    private final ProductDAO productDao = new ProductDAO();
    private final OrderDAO orderDao = new OrderDAO();
    

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
           HttpSession session = req.getSession();
        User u = (User)session.getAttribute("account");
       
        
        
         
         int totalProduct = productDao.countProductstore(u.getStore().getStoid());
         int totalUser = userDao.countAllUser();
       
        
        int totalOrder = orderDao.countorderStore(u.getStore().getStoid());
        int totalProductOutOfStock = productDao.countAllByTotal(10);
        List<User> users = userDao.findNewUsers(5);
        List<Order> orders = orderDao.getNewOrdersStore(u.getStore().getStoid(),5);
        ReportRevenueDTO revenueDTO = orderDao.getReportRevenue();
        revenueDTO.formatRevenue();
        
        req.setAttribute("totalUser", totalUser);
        req.setAttribute("totalProduct", totalProduct);
        req.setAttribute("totalOrder", totalOrder);
        req.setAttribute("totalPrice", revenueDTO.getRevenueOut());
        req.setAttribute("totalPriceIn", revenueDTO.getRevenueIn());
        req.setAttribute("totalProductOutOfStock", totalProductOutOfStock);

        req.setAttribute("users", users);
        req.setAttribute("orders", orders);

        RequestDispatcher rs = req.getRequestDispatcher("dashboardSeller.jsp");
        rs.forward(req, resp);
    }

     private User getCurrentUser(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("account");
        return user;
    }

}
