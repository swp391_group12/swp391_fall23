/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Dashboard;

import dal.BannerDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.nio.file.Paths;
import model.Banner;
import jakarta.servlet.annotation.MultipartConfig;
@MultipartConfig
/**
 *
 * @author Admin
 */
public class AddNewBanner_Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddNewBanner_Controller</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddNewBanner_Controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("banner/addnewbanner.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String des = request.getParameter("des");
        String link = request.getParameter("link");
        String status = request.getParameter("status");
        int stid = status.equals("Hiển thị") ? 1 : 2;
        String msg = "";
        Part part = request.getPart("image");
        String realpart = request.getServletContext().getRealPath("/img/banner");
        realpart = realpart.replace("\\build\\web", "\\web");
        if (part != null) {

            String filename = Paths.get(part.getSubmittedFileName()).getFileName().toString();
            part.write(realpart + "/" + filename);

            Banner b = new Banner(-1, filename, des, stid, link);
            BannerDAO bd = new BannerDAO();
            boolean bb = bd.addNewAddress(b);
            

            if (bb == true) {
                msg = "Thêm mới thành công";
                request.setAttribute("msg", msg);
                request.getRequestDispatcher("banner/addnewbanner.jsp").forward(request, response);
            } else {
                msg = "Thêm mới thất bại";
                request.setAttribute("msg", msg);
                request.getRequestDispatcher("banner/addnewbanner.jsp").forward(request, response);
            }
        }else{
                msg = "không tồn tại ảnh";
                request.setAttribute("msg", msg);
                request.getRequestDispatcher("banner/addnewbanner.jsp").forward(request, response);
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
