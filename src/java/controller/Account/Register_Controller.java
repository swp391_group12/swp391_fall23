/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Account;

import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Role;
import model.User;

/**
 *
 * @author Admin
 */
public class Register_Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("login/register.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String pass = request.getParameter("pass");
        UserDAO d = new UserDAO();
        Role role = d.getRoleById(1);
        String msg = "";
        
        if(!utils.ValidateData.isValidName(name)){
            msg = "Tên không hợp lệ, vui lòng nhập lại";
            request.setAttribute("msg", msg);
            request.setAttribute("email", email);
            request.setAttribute("phone", phone);
            request.setAttribute("pass", pass);
            request.getRequestDispatcher("login/register.jsp").forward(request, response);
            return;
        }
        
        if(!utils.ValidateData.isValidEmail(email)){
            msg = "Email không hợp lệ, vui lòng nhập lại";
            request.setAttribute("msg", msg);
            request.setAttribute("name", name);
            request.setAttribute("phone", phone);
            request.setAttribute("pass", pass);
            request.getRequestDispatcher("login/register.jsp").forward(request, response);
            return;
        }
        
        boolean b = d.isEmailExists(email);
        
        if(name != "" && email != "" && phone != "" && pass != "" ){
            if(!b){
                boolean e = d.addUser(new User(-1, name, email, phone, pass, role, null, true));
                    if(e){
                        msg = "Đăng ký thành công, vui lòng đăng nhập";
                        request.setAttribute("msg", msg);
                        request.getRequestDispatcher("login/login.jsp").forward(request, response);
                        //response.sendRedirect("login");
                    }
                    else{
                        msg = "Có lỗi xảy ra vui lòng thử lại";
                        request.setAttribute("msg", msg);
                        request.getRequestDispatcher("login/register.jsp").forward(request, response);
                    }
                }
            else{
                msg = "Email đã tồn tại, vui lòng nhập lại";    
                request.setAttribute("msg", msg);
                request.getRequestDispatcher("login/register.jsp").forward(request, response);
            }

        }
        else{
                msg = "Vui lòng nhập đủ thông tin";
                request.setAttribute("msg", msg);
                request.getRequestDispatcher("login/register.jsp").forward(request, response);  
            
        }
        
        
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
