/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.common;

import dal.BLogDAO;
import dto.BLogDTO;
import dto.BlogCategoryDTO;
import dto.StoresDTO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import service.StoreService;
import service.interfaces.IStoreService;

/**
 *
 * @author sonng
 */
@WebServlet(name = "BlogListUser", urlPatterns = {"/Blog-List"})
public class BlogListUser extends HttpServlet {

    private final IStoreService iStoreService = new StoreService();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet BlogListUser</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet BlogListUser at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BLogDAO dao = new BLogDAO();
        String cid = request.getParameter("cid") == null ? "" : request.getParameter("cid");
        String search = request.getParameter("search") == null ? "" : request.getParameter("search");
        String index = request.getParameter("index") == null ? "1" : request.getParameter("index");
        String sort = request.getParameter("sort") == null ? "" : request.getParameter("sort");

        ArrayList<BlogCategoryDTO> categoryList = dao.getBlogCategoryDTO();
        List<StoresDTO> storesDTOs = iStoreService.getListStores();

        request.setAttribute("listStores", storesDTOs);
        int total = dao.getNUmberBlog(cid, "1", search);
        int numberPage = (int) Math.ceil((double) total / 6);
        int currentPage = Integer.parseInt(index);
        ArrayList<BLogDTO> productList = dao.getBlog(cid, "1", search, currentPage, sort);
        request.setAttribute("prolist", productList);
        request.setAttribute("cate", categoryList);
        request.setAttribute("numberPage", numberPage);
        request.getRequestDispatcher("./blog.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
