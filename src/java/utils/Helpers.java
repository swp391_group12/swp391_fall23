/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utils;

import java.text.NumberFormat;
import java.util.Locale;

public class Helpers {

    public static void ErrorMessage(String reason, Object evidence) {
        System.err.println(reason + ":\n" + evidence);
    }

    /**
     *
     * @param x -> number type String want parse
     * @param defaultValue -> if parse fail -> take this value
     * @return
     */
    public static int parseInt(String x, int defaultValue) {
        int n = defaultValue;

        try {

            n = Integer.parseInt(x);

        } catch (NumberFormatException e) {
            ErrorMessage("parseInt()", e);
        }

        return n;
    }

    /**
     *
     * @param x -> number type String want parse
     * @param defaultValue -> if parse fail -> take this value
     * @return
     */
    public static double parseDouble(String x, double defaultValue) {
        double n = defaultValue;

        try {

            n = Double.parseDouble(x);

        } catch (NumberFormatException e) {
            ErrorMessage("parseDouble()", e);
        }

        return n;
    }

    public static float parseFloat(String x, float defaultValue) {
        float n = defaultValue;

        try {
            n = Float.parseFloat(x);
        } catch (NumberFormatException e) {
            ErrorMessage("parseFloat()", e);
        }

        return n;
    }

    public String formatInt(Object data) {
        NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
        if (data == null) {
            return "";
        }
        return nf.format(data);
    }

    public String formatIntVN(Object data) {
        double d = Double.parseDouble(data.toString());
        NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
        return nf.format(d);
    }

    public boolean isChecked(String[] x, int value) {
        for (String item : x) {
            if (parseInt(item, 0) == value) {
                return true;
            }
        }
        return false;
    }

}
