/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utils;

import java.util.Properties;
import java.util.Random;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Admin
 */
public class SendMail {
    
      public static String generateRandomPassword() {
        // Tạo một chuỗi ký tự cho mật khẩu
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        StringBuilder password = new StringBuilder();
        Random random = new Random();

        // Sinh ngẫu nhiên mật khẩu
        for (int i = 0; i < 8; i++) {
            password.append(characters.charAt(random.nextInt(characters.length())));
        }

        return password.toString();
    }
      
      private static void sendPasswordResetEmail(String email, String password, String subject, String text ) {
        Properties props = new Properties();
      props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        // Tạo phiên làm việc với mail server
        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("vuhuyy2@gmail.com", "vxtuqgzvgksdlhtl"); // Thay bằng email và mật khẩu của bạn
            }
        });

        try {
            // Tạo đối tượng Message
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress("vuhuyy2@gmail.com")); // Thay bằng email của bạn
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
            message.setSubject(subject, "UTF-8");
            message.setText(text,"UTF-8");

            // Gửi email
            Transport.send(message);


        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
      
      public static void sendMail(String userEmail, int type , String pass) {
          String subject;
          String text;
          switch(type){
              case 1:
                  subject = "[OGANI FRUITSHOP]-YÊU CẦU ĐẶT LẠI MẬT KHẨU";
                  text = "Xin chào!\nMật khẩu mới của tài khoản " + userEmail + " là:" + pass; 
                  sendPasswordResetEmail(userEmail, pass, subject, text);
                  break;
                  
              case 2:
                  subject = "[OGANI FRUITSHOP]-MẬT KHẨU CHO TÀI KHOẢN CỦA BẠN";
                  text = "Xin chào!\nMật khẩu cho tài khoản " + userEmail + " của bạn là: " + pass;
                  sendPasswordResetEmail(userEmail, pass, subject, text);
                  break;
          }
      }
}
