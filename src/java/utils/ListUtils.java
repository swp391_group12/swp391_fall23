/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utils;

import dto.ProductDisplayCartDTO;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Trung Kien
 */
public class ListUtils {

    public static boolean isExistedProductCart(List<ProductDisplayCartDTO> list, int pId) {
        for (ProductDisplayCartDTO data : list) {
            if (data.getPid() == pId) {
                return true;
            }
        }
        return false;
    }

    public static List<ProductDisplayCartDTO> removeByPId(List<ProductDisplayCartDTO> list, int pId) {
        List<ProductDisplayCartDTO> result = new ArrayList<>();
        for (ProductDisplayCartDTO data : list) {
            if (data.getPid() != pId) {
                result.add(data);
            }
        }
        return result;
    }
}
