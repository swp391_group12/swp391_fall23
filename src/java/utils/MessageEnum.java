/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utils;

import lombok.Getter;

/**
 *
 * @author Trung Kien
 */
@Getter
public enum MessageEnum {
    NOT_ENOUGH_QUANTITY("Warning", "NOT ENOUGH PRODUCT QUANTITY"),
    PRODUCT_NOT_FOUND("Warning", "PRODUCT NOT FOUND"),
    NO_CHOOSE_STORE_DELIVERY("Warning", "NO CHOOSE STORE DELIVERY"),
    USER_NOT_FOUND("Error", "USER NOT FOUND"),
    CART_EMPTY("Warning", "CART CAN'T EMPTY"),
    UPDATE_STATUS_ORDER_SUCCESS("Success", "Update status order success"),
    UPDATE_STATUS_ORDER_ERROR("Error", "Update status order error"),
    ADDRESS_IS_BLANK("Warning", "Address is not blank"),
    PRODUCT_MUST_THE_SAME_STORE("Warning", "Products must be from the same store"),
    ;
    private final String alert;
    private final String message;

    private MessageEnum(String alert, String message) {
        this.alert = alert;
        this.message = message;
    }

}
