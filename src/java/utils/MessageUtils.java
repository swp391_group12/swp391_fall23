/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utils;

import jakarta.servlet.http.HttpServletRequest;

/**
 *
 * @author Trung Kien
 */
public class MessageUtils {

    public static void showMessage(HttpServletRequest request, MessageEnum messageEnum) {
        request.setAttribute("message", messageEnum.getMessage());
        request.setAttribute("alert", messageEnum.getAlert());
    }
}
