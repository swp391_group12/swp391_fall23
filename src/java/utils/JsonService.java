/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Trung Kien
 */
public class JsonService {

    private static final ObjectMapper JSON_MAPPER = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .registerModule(new JavaTimeModule()); // new module, NOT JSR310Module

    public static <T> List<T> parseJsonToList(String json, Class<T> serializableClass) throws IOException, ClassNotFoundException {
        Class<T[]> arrayClass = (Class<T[]>) Class.forName("[L" + serializableClass.getName() + ";");
        if (StringUtils.isBlank(json)) {
            return new ArrayList<>();
        }
        T[] objects = JSON_MAPPER.readValue(json, arrayClass);
        return Arrays.asList(objects);
    }

    public static <T> T[] parseJsonToArray(Object jSonObject, Class<T> serializableClass) throws IOException, ClassNotFoundException {
        Class<T[]> arrayClass = (Class<T[]>) Class.forName("[L" + serializableClass.getName() + ";");
        String jsonArray = JSON_MAPPER.writeValueAsString(jSonObject);
        T[] objects = JSON_MAPPER.readValue(jsonArray, arrayClass);
        return objects;
    }

    public static <T> T parseJsonToObject(Object jSonObject, Class<T> serializableClass) {
        String jsonArray;
        T objects = null;
        try {
            jsonArray = JSON_MAPPER.writeValueAsString(jSonObject);
            objects = JSON_MAPPER.readValue(jsonArray, serializableClass);
        } catch (Exception ex) {
            Logger.getLogger(JsonService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return objects;
    }

    public static <T> T parseJsonToObject(String jSonObject, Class<T> serializableClass) {
        T objects = null;
        try {
            objects = JSON_MAPPER.readValue(jSonObject, serializableClass);
        } catch (Exception ex) {
            Logger.getLogger(JsonService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return objects;
    }

    public static String toString(Object serializableObject) {
        String ERROR_JSON = "{\"message\": \"JsonProcessingException\"}";

        String jsonResponseString;
        try {
            jsonResponseString = JSON_MAPPER.writeValueAsString(serializableObject);
        } catch (JsonProcessingException ex) {
            jsonResponseString = ERROR_JSON;
        }
        return jsonResponseString;
    }

    public static ObjectNode createObject(String key, String value) {
        ObjectNode data = JSON_MAPPER.createObjectNode();
        data.put(key, value);
        return data;
    }

    public static ObjectNode createObject(String key, ObjectNode value) {
        ObjectNode data = JSON_MAPPER.createObjectNode();
        data.put(key, value);
        return data;
    }

    public static String stringToBase64(String data) {
        return Base64.getEncoder().encodeToString(data.getBytes(StandardCharsets.UTF_8));
    }

    public static String base64ToString(String data) {
        return new String(Base64.getDecoder().decode(data), StandardCharsets.UTF_8);
    }
}
