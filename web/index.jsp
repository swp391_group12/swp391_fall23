<%-- 
    Document   : home
    Created on : Oct 2, 2023, 11:40:19 PM
    Author     : Trung Kien
--%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean class="dal.CategoryDAO" id="cdao"/>
<jsp:useBean class="utils.Helpers" id="helpers"/>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Ogani | Template</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">

        <style>
            .blog .carousel-indicators {
                left: 0;
                top: auto;
                bottom: -40px;

            }

            /* The colour of the indicators */
            .blog .carousel-indicators li {
                background: #a3a3a3;
                border: 4px;
                padding: 2px;
            }

            .blog .carousel-indicators .active {
                background: #707070;
            }
            #id_category li:hover {
                background-color: #7fad39;
            }

            /* Màu xanh khi danh mục được chọn */
            #id_category li.active,
            #id_category li:hover {
                background-color: #7fad39;
                color: #FFFFFF;
            }
            li {
                margin-bottom:0px;
                cursor: pointer;
                position: relative;

            }

            .custom-label {
                margin: 15px;
                font-size: 16px;
                font-family: "Cairo", sans-serif;
                font-weight: 400;
                letter-spacing: 1px;
                border-bottom: 1px solid #7fad39;
            }

           
            li.active label {
                font-weight: bold; /* Change the font style for the active state */
            }
        </style>
    </head>

    <body>

        <!-- Header Section Begin -->
        <jsp:include page="header.jsp" flush="true" >
            <jsp:param name="home" value="active" />
        </jsp:include>
        <!-- Header Section End -->

        <!-- Hero Section Begin -->
        <section class="hero">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="hero__categories">
                            <div class="hero__categories__all">

                                <span>Danh mục sản phẩm</span>
                            </div>
                            <form action="${pageContext.request.contextPath}/shop" method='post'>
                                <ul id = "id_category">
                                    <c:forEach items="${cdao.all}" var="item">
                                        <li class="${cid == item.cid ? 'active' : ''}" onclick="selectCategory('${item.cid}')">
                                            <input hidden type='radio' name='cid' value="${item.cid}"
                                                   ${cid != null && cid == item.cid ? 'checked' : '' } onchange="this.form.submit()" id='category_${item.cid}' />
                                            <label class="custom-label" for='category_${item.cid}'>${item.cName}</label>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </form>
                        </div>
                    </div>



                    <div class="col-lg-9">
                        <div class="hero__search">
                            <div class="hero__search__form">
                                <form action="${pageContext.request.contextPath}/shop" method='post'>

                                    <input type="text" name='search' value='${search}' placeholder="Bạn đang tìm kiếm gì ?">
                                    <button type="submit" class="site-btn">Tìm Kiếm</button>
                                </form>

                            </div>
                            <div class="hero__search__phone">
                                <div class="hero__search__phone__icon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="hero__search__phone__text">
                                    <h5>0123456789</h5>
                                    <a>Hỗ trợ từ 7:00 - 18:00</a>
                                </div>
                            </div>
                        </div>
                        <a href="${b.blink}">
                            <div class="hero__item set-bg" style="background-image: url('img/banner/${b.burl}')"  ></div>
                        </a>

                    </div>
                </div>
            </div>
        </section>
        <!-- Hero Section End -->

        <!-- Categories Section Begin -->

        <div class="container rounded-sm">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ul class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ul>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <c:if test="${fn:length(requestScope.sliders)> 0}">
                        <div class="carousel-item active">
                            <img src="${requestScope.sliders[0].slLink}" alt="Image">
                        </div>
                        <c:forEach  var="slider" items="${sliders}" varStatus="loop">
                            <c:if test="${loop.index > 0}">
                                <div class="carousel-item">
                                    <img src="${slider.slLink}" alt="Image">
                                </div>
                            </c:if>
                        </c:forEach>
                    </c:if>
                </div>

                <!-- Left and right controls -->
                <a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#myCarousel" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>
            </div>
        </div>
        <!-- Categories Section End -->

        <!-- Featured Section Begin -->
        <section class="featured spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title">
                            <h2>Sản phẩm nổi bật</h2>
                        </div>

                    </div>
                </div>
                <div class="row featured__filter">
                    <c:forEach items="${products}" var="product">
                        
                            <div class="col-lg-3 col-md-4 col-sm-6 mix oranges fresh-meat">
                                <div class="featured__item">
                                    <div class="featured__item__pic set-bg" >
                                        <c:if test="${product.image != null}">
                                            <img src="img/product/${product.image.iUrl}" height="280px" width="280px" />
                                            <ul class="featured__item__pic__hover">
                                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                               <li onclick="onClickAddToCart(${product.pid}, 1)"><a><i class="fa fa-shopping-cart"></i></a></li>
                                            </ul>
                                        </c:if>
                                    </div>
                                    <div class="featured__item__text">
                                        <a href="productDetail?pid=${product.pid}"><h6>${product.pName}</h6></a>
                                        <h5>${helpers.formatInt(product.price)} VNĐ</h5>                                    
                                    </div>
                                </div>
                            </div>
                            
                    </c:forEach>
                </div>
            </div>
        </section>
        <!-- Featured Section End -->

    
        <!-- Latest Product Section End -->

        <!-- Blog Section Begin -->
        <section class="from-blog spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title from-blog__title">
                            <h2>Bài viết</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="blog__item">
                            <div class="blog__item__pic">
                                <img src="img/blog/blog1.jpg" alt="">
                            </div>
                            <div class="blog__item__text">
                                <ul>
                                    <li><i class="fa fa-calendar-o"></i>  2023-10-17</li>
                                    
                                </ul>
                                <h5><a href="http://localhost:9999/SWP391_FA23_SE1736_TEAM1/Blog-Detail?bid=1">Ăn hoa quả đúng cách tốt cho sức khỏe</a></h5>
                                <p>Hoa quả mang lại nhiều lợi ích cho sức khỏe của con người, thế nhưng nếu bạn không nắm rõ ăn hoa quả sao cho đúng cách thì có thể từ có lợi lại biến thành có hại đó nhé.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="blog__item">
                            <div class="blog__item__pic">
                                <img src="img/blog/blog2.jpg" alt="">
                            </div>
                            <div class="blog__item__text">
                                <ul>
                                    <li><i class="fa fa-calendar-o"></i>  2023-10-17</li>
                                    
                                </ul>
                                <h5><a href="http://localhost:9999/SWP391_FA23_SE1736_TEAM1/Blog-Detail?bid=15">Cách bảo quản trái cây tươi</a></h5>
                                <p>Nên có một lớp lưới xốp bao bọc trái cây bên ngoài để tránh va chạm quá nhiều.
                                    Bảo quản ở nhiệt độ phù hợp, tránh quá nóng hoặc quá lạnh; </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="blog__item">
                            <div class="blog__item__pic">
                                <img src="img/blog/blog3.jpg" alt="">
                            <div class="blog__item__text">
                                <ul>
                                    <li><i class="fa fa-calendar-o"></i>  2023-11-02</li>
                                    
                                </ul>
                                <h5><a href="http://localhost:9999/SWP391_FA23_SE1736_TEAM1/Blog-Detail?bid=21">Lựa chọn hoa quả sạch, bảo vệ sức khỏe người tiêu dùng
                                    </a></h5>
                                <p>Hiện nay, trên thị trường, hoa quả được bày bán nhiều tại chợ, siêu thị, cửa hàng với đủ loại, xuất xứ, từ hàng nội địa đến nhập ngoại. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Blog Section End -->

        <!-- Footer Section Begin -->
        <footer class="footer spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="footer__about">
                            <div class="footer__about__logo">
                                <a href="./index.jsp"><img src="img/logo.png" alt=""></a>
                            </div>
                            <ul>
                                <li>Của hàng HCM: 66 Nguyễn Trí Sách, Phường 15 Tân Bình</li>
                                <li>Cửa hàng HN: 63 Yên Lãng - Đống Đa - Hà Nội</li>
                                <li>Phone: 0123456789</li>
                                <li>Email: shophoaqua@gmail.com</li>
                            </ul>
                        </div>
                    </div>
                    
                    
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer__copyright">
                            <div class="footer__copyright__text"><p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p></div>
                            <div class="footer__copyright__payment"><img src="img/payment-item.png" alt=""></div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer Section End -->

        <!-- Js Plugins -->
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/jquery.slicknav.js"></script>
        <script src="js/mixitup.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/main.js"></script>

        <script>
                     
                                        function selectCategory(categoryId) {
                                            document.getElementById('category_' + categoryId).checked = true;
                                            document.forms[0].submit(); // Gửi form sau khi chọn category
                                        }
        </script>
        
        
        <script>
                                        function onClickAddToCart(idItem, quantity) {
                                            console.log(idItem);
                                            if (quantity === 0) {
                                                event.preventDefault();
                                                alert("We apologize for this inconvenience." +
                                                        "\nThe item is currently out of stock, please comeback later");
                                            } else {
                                                let href = '/SWP391_FA23_SE1736_TEAM1/AddorCheckRedirectController?productId=' + idItem + '&quantity=1&path=shop';
                                                window.location.href = href;
                                            }
                                        }
        </script>

    </body>

</html>