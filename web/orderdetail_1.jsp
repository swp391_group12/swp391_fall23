<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="zxx">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Ogani | Template</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="./css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="./css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="./css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="./css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="./css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="./css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="./css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="./css/style.css" type="text/css">
        <style>
        /* Inline CSS for demonstration */
        .table-container {
            background-color: #fff;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        .table {
            border-collapse: separate;
            border-spacing: 0 15px;
        }
        .table thead tr {
            background-color: #f3f3f3;
        }
        .table th, .table td {
            padding: 15px;
            text-align: center;
        }
        .table th {
            font-weight: 600;
        }
        .table tbody tr {
            background-color: #fdfdfd;
            border-radius: 8px;
        }
        .table tbody tr:hover {
            background-color: #f9f9f9;
        }
        .table .btn {
            border-radius: 4px;
            padding: 5px 15px;
            font-size: 14px;
        }
        .btn-primary {
            background-color: #007bff;
            border-color: #007bff;
        }
        .btn-danger {
            background-color: #dc3545;
            border-color: #dc3545;
        }
        .btn-primary:hover, .btn-danger:hover {
            opacity: 0.8;
        }
    </style>
    </head>

    <body>
        <!-- Humberger Begin -->
        <div class="humberger__menu__overlay"></div>
        <jsp:include page="header.jsp" flush="true" >
            <jsp:param name="home" value="active" />
        </jsp:include>
        <!-- Header Section End -->

        <!-- Hero Section Begin -->
        <section class="hero hero-normal">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <!--                        <div class="hero__categories">
                                                    <div class="hero__categories__all">
                                                        <i class="fa fa-bars"></i>
                                                        <span>All departments</span>
                                                    </div>
                                                    <ul>
                                                        <li><a href="#">Fresh Meat</a></li>
                                                        <li><a href="#">Vegetables</a></li>
                                                        <li><a href="#">Fruit & Nut Gifts</a></li>
                                                        <li><a href="#">Fresh Berries</a></li>
                                                        <li><a href="#">Ocean Foods</a></li>
                                                        <li><a href="#">Butter & Eggs</a></li>
                                                        <li><a href="#">Fastfood</a></li>
                                                        <li><a href="#">Fresh Onion</a></li>
                                                        <li><a href="#">Papayaya & Crisps</a></li>
                                                        <li><a href="#">Oatmeal</a></li>
                                                        <li><a href="#">Fresh Bananas</a></li>
                                                    </ul>
                                                </div>-->
                    </div>
                    <div class="col-lg-9">
                        <div class="hero__search">
                            
                            <div class="hero__search__phone">
                                <div class="hero__search__phone__icon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="hero__search__phone__text">
                                    <h5>0123456789</h5>
                                    <span>Hỗ trợ từ 7:00 - 18:00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Hero Section End -->

        <!-- Blog Details Section Begin -->
        <section class="blog-details spad">
            <div class="container">
                <h1 style="text-align: center; margin-bottom: 25px; padding: 20px; background-color: #e8f5e9; color: #2e7d32; font-family: 'Arial', sans-serif; font-size: 2.5rem; letter-spacing: 2px; text-transform: uppercase; text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.1); border-radius: 10px; box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);">Chi tiết đơn hàng</h1>

                <table  class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Tên sản phẩm</th>
                            <th>Xuất xứ</th>
                            <th>Ảnh</th>
                            <th>Giá</th>
                            <th>Số lượng</th>
                            <th>Tổng</th>
                            <th>Đánh giá sản phẩm</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="o" items="${de}">
                            <tr>
                                <td>${o.orderDetailId}</td>
                                <td>${o.product.pName}</td>
                                <td>${o.product.pBrand}</td>
                                <td><img src="img/product/${o.product.image.iUrl}" width="200px"></td>
                                <td>${o.product.price}</td>
                                <td>${o.quantity}</td>
                                <td>${o.quantity * o.product.price}đ</td>
                                <td>
                                    <c:if test="${param.ss==7}">
                                        <a href="SendFeedback?proID=${o.product.pid}" class="btn btn-primary">Đánh giá sản phẩm</a>
                                    </c:if>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </section>
        <!-- Blog Details Section End -->

        <!-- Related Blog Section Begin -->
        <section class="related-blog spad">

        </section>
        <!-- Related Blog Section End -->

        <!-- Footer Section Begin -->
        <footer class="footer spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="footer__about">
                            <div class="footer__about__logo">
                                <a href="home"><img src="img/logo.png" alt=""></a>
                            </div>
                            <ul>
                                <li>Của hàng HCM: 66 Nguyễn Trí Sách, Phường 15 Tân Bình</li>
                                <li>Cửa hàng HN: 63 Yên Lãng - Đống Đa - Hà Nội</li>
                                <li>Phone: 0123456789</li>
                                <li>Email: shophoaqua@gmail.com</li>
                            </ul>
                        </div>
                    </div>
                    
                    
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer__copyright">
                            <div class="footer__copyright__text"><p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p></div>
                            <div class="footer__copyright__payment"><img src="img/payment-item.png" alt=""></div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer Section End -->
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
        <script>
                                        $(document).ready(function () {
                                            $('#myDataTable').DataTable();
                                        });
        </script>

    </body>

</html>