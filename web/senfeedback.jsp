<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="zxx">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Ogani | Template</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="./css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="./css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="./css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="./css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="./css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="./css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="./css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="./css/style.css" type="text/css">
        <style>
            .feedback-section {
                background-color: #f7f7f7;
                padding: 50px 0;
            }
            .feedback-container {
                background: #fff;
                border-radius: 8px;
                padding: 40px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            }
            .feedback-title {
                text-align: center;
                margin-bottom: 25px;
                font-size: 24px;
                color: #333;
            }
            .feedback-form {
                margin-top: 20px;
            }
            .feedback-form label {
                font-weight: bold;
                display: block;
                margin-bottom: 10px;
            }
            .feedback-form textarea {
                width: 100%;
                padding: 10px;
                margin-bottom: 20px;
                border: 1px solid #ddd;
                border-radius: 4px;
            }
            .feedback-form .btn-submit {
                padding: 10px 20px;
                font-size: 16px;
                background-color: #5cb85c;
                color: white;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            .feedback-form .btn-submit:hover {
                background-color: #4cae4c;
            }
            *{
                margin: 0;
                padding: 0;
            }
            .rate {
                float: left;
                height: 46px;
                padding: 0 10px;
            }
            .rate:not(:checked) > input {
                position:absolute;
                top:-9999px;
            }
            .rate:not(:checked) > label {
                float:right;
                width:1em;
                overflow:hidden;
                white-space:nowrap;
                cursor:pointer;
                font-size:30px;
                color:#ccc;
            }
            .rate:not(:checked) > label:before {
                content: '★ ';
            }
            .rate > input:checked ~ label {
                color: #ffc700;
            }
            .rate:not(:checked) > label:hover,
            .rate:not(:checked) > label:hover ~ label {
                color: #deb217;
            }
            .rate > input:checked + label:hover,
            .rate > input:checked + label:hover ~ label,
            .rate > input:checked ~ label:hover,
            .rate > input:checked ~ label:hover ~ label,
            .rate > label:hover ~ input:checked ~ label {
                color: #c59b08;
            }

            .feedback-title {
                font-family: 'Arial', sans-serif; /* Sử dụng font Arial */
                font-size: 40px; /* Kích thước chữ */
                font-weight: bold; /* In đậm chữ */
                color: #0077B3; /* Màu sắc chữ xanh biển */

                text-align: center; /* Căn giữa tiêu đề */
                padding: 20px; /* Đệm xung quanh tiêu đề */
                background-color: #f3f3f3; /* Màu nền cho tiêu đề */
                border-radius: 8px; /* Bo góc cho tiêu đề */
                box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1); /* Bóng đổ cho khối tiêu đề */
            }



        </style>
    </head>

    <body>
        <!-- Humberger Begin -->
        <div class="humberger__menu__overlay"></div>
        <jsp:include page="header.jsp" flush="true" >
            <jsp:param name="home" value="active" />
        </jsp:include>
        <!-- Header Section End -->

        <!-- Hero Section Begin -->
        <section class="hero hero-normal">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <!--                        <div class="hero__categories">
                                                    <div class="hero__categories__all">
                                                        <i class="fa fa-bars"></i>
                                                        <span>All departments</span>
                                                    </div>
                                                    <ul>
                                                        <li><a href="#">Fresh Meat</a></li>
                                                        <li><a href="#">Vegetables</a></li>
                                                        <li><a href="#">Fruit & Nut Gifts</a></li>
                                                        <li><a href="#">Fresh Berries</a></li>
                                                        <li><a href="#">Ocean Foods</a></li>
                                                        <li><a href="#">Butter & Eggs</a></li>
                                                        <li><a href="#">Fastfood</a></li>
                                                        <li><a href="#">Fresh Onion</a></li>
                                                        <li><a href="#">Papayaya & Crisps</a></li>
                                                        <li><a href="#">Oatmeal</a></li>
                                                        <li><a href="#">Fresh Bananas</a></li>
                                                    </ul>
                                                </div>-->
                    </div>
                    <div class="col-lg-9">
                        <div class="hero__search">

                            <div class="hero__search__phone">
                                <div class="hero__search__phone__icon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="hero__search__phone__text">
                                    <h5>0123456789</h5>
                                    <span>Hỗ trợ từ 7:00 - 18:00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Hero Section End -->

        <!-- Blog Details Section Begin -->
        <section class="feedback-section">
            <div class="container">
                <div class="feedback-container">
                    <h1 class="feedback-title">Gửi đánh giá</h1>
                    <h3 style="text-align: center; margin-bottom: 25px; margin-top: 25px; color: #008000; font-family: 'Arial', sans-serif; font-size: 24px; font-weight: bold; display: block;">
                        Tên sản phẩm: ${product.pName}
                    </h3>

                    <form class="feedback-form" action="SendFeedback" method="post" onsubmit="return validateForm();">
                        <label for="feedbackContent">Nội dung đánh giá:</label>
                        <textarea class="form-control" id="feedbackContent" name="feedbackContent" rows="4" placeholder="Viết cảm nhận đánh giá của bạn..."></textarea>

                        <label>Đánh giá số sao:</label>
                        <div style="margin-right: 20px">
                            <div class="rate">
                                <input type="radio" id="star5" name="rate" value="5" />
                                <label for="star5" title="text">5 stars</label>
                                <input type="radio" id="star4" name="rate" value="4" />
                                <label for="star4" title="text">4 stars</label>
                                <input type="radio" id="star3" name="rate" value="3" />
                                <label for="star3" title="text">3 stars</label>
                                <input type="radio" id="star2" name="rate" value="2" />
                                <label for="star2" title="text">2 stars</label>
                                <input type="radio" id="star1" name="rate" value="1"  />
                                <label for="star1" title="text">1 star</label>
                            </div>
                        </div>
                        <br>
                        <br><label for="imgadd">Ảnh:</label>
                        <input class="form-control" id="imgadd"  onchange="changeimgadd()" name="image" type="file" >

                        <input name="proimage" id="imageadd" value="" type="hidden">
                        <br><!-- comment -->
                        <image src="" id="demoimgadd" style="margin-top: 5px;" width="30%">

                        <input name="pid" value="${product.pid}" type="hidden">
                        <br><!-- comment -->
                        <button type="submit" class="btn-submit">Gửi</button>
                    </form>
                </div>
        </section>
        <!-- Blog Details Section End -->

        <!-- Related Blog Section Begin -->
        <section class="related-blog spad">

        </section>
        <!-- Related Blog Section End -->

        <!-- Footer Section Begin -->
        <footer class="footer spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="footer__about">
                            <div class="footer__about__logo">
                                <a href="home"><img src="img/logo.png" alt=""></a>
                            </div>
                            <ul>
                                <li>Của hàng HCM: 66 Nguyễn Trí Sách, Phường 15 Tân Bình</li>
                                <li>Cửa hàng HN: 63 Yên Lãng - Đống Đa - Hà Nội</li>
                                <li>Phone: 0123456789</li>
                                <li>Email: shophoaqua@gmail.com</li>
                            </ul>
                        </div>
                    </div>


                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer__copyright">
                            <div class="footer__copyright__text"><p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p></div>
                            <div class="footer__copyright__payment"><img src="img/payment-item.png" alt=""></div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer Section End -->
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
        <script>
                                        function changeimgadd() {
                                            var file = document.getElementById("imgadd").files[0];
                                            if (file.name.match(/.+\.(jpg|png|jpeg)/i)) {
                                                if (file.size / (1024 * 1024) < 5) {
                                                    var fileReader = new FileReader();
                                                    fileReader.readAsDataURL(file);
                                                    fileReader.onload = function () {
                                                        document.getElementById("imageadd").value = (fileReader.result);
                                                        document.getElementById("demoimgadd").src = (fileReader.result);
                                                    }
                                                } else {
                                                    uploadError();
                                                }
                                            } else {
                                                uploadError();
                                            }
                                        }
                                        function uploadError() {
                                            alert('Please upload photo file < 5MB')
                                            document.getElementById("imgadd").files[0].value = ''
                                            document.getElementById("imgadd").type = '';
                                            document.getElementById("imgadd").type = 'file';
                                        }

        </script>
        <script>
            function validateForm() {
                // Validate star rating
                var stars = document.getElementsByName('rate');
                var starChecked = false;
                for (var i = 0; i < stars.length; i++) {
                    if (stars[i].checked) {
                        starChecked = true;
                        break;
                    }
                }
                if (!starChecked) {
                    alert("Vui lòng chọn số sao.");
                    return false;
                }

                // Validate feedback content
                var feedbackContent = document.getElementById('feedbackContent').value.trim();
                if (feedbackContent === '') {
                    alert("Vui lòng nhập nội dung phản hồi.");
                    return false;
                }

                return true;
            }
        </script>
    </body>

</html>