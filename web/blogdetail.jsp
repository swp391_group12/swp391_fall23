<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="zxx">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Ogani | Template</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="./css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="./css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="./css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="./css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="./css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="./css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="./css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="./css/style.css" type="text/css">
    </head>


    <!-- Header Section End -->
    <jsp:include page="./header.jsp" flush="true" >
        <jsp:param name="blog" value="active" />
    </jsp:include>

    <!-- Hero Section Begin -->
    <section class="hero hero-normal">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="hero__categories">
                        
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="hero__search">
                        <div class="hero__search__form">
                            <form method="get" action="Blog-List">
                                    <input type="text" name="search" value="${param['search']}" placeholder="Tìm kiếm bài viết...">
                                    <input type="hidden" name="cid" value="${param['cid']}">
                                    <input type="hidden" name="sort" value="${param['sort']}">
                                    <button type="submit" class="site-btn">Tìm kiếm</button>
                                </form>
                            </div>
                        <div class="hero__search__phone">
                            <div class="hero__search__phone__icon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="hero__search__phone__text">
                                <h5>0123456789</h5>
                                <span>Hỗ trợ từ 7:00 - 18:00</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Hero Section End -->

    <!-- Blog Details Section Begin -->
    <section class="blog-details spad">
        <div class="container">
             <div class="section-title">
                <h2>${b.blogTitle}</h2>
            </div>
            <div class="row">
<!--                <div class="col-lg-4 col-md-5 order-md-1 order-2">
                    <div class="blog__sidebar">
                        <div class="blog__sidebar__search">
                            <form action="Blog-List">
                                <input type="search" placeholder="Search Blog...">
                                <button type="submit"><span class="icon_search"></span></button>
                            </form>
                        </div>
                        <div class="blog__sidebar__item">
                            <h4>Sort by</h4>
                            <ul>
                                <li><a href="./Blog-List?cid=${param['cid']}&search=${param['search']}&index=${index}&sort=" ${param['sort']==""?"style='font-weight: bold'":""}>Default</a>     </li>
                                <li><a href="./Blog-List?cid=${param['cid']}&search=${param['search']}&index=${index}&sort=order by b.blogDate desc" ${param['sort']=="order by b.blogDate desc"?"style='font-weight: bold'":""}>Newest</a>     </li>
                                <li><a href="./Blog-List?cid=${param['cid']}&search=${param['search']}&index=${index}&sort=order by b.blogDate asc" ${param['sort']=="order by b.blogDate asc"?"style='font-weight: bold'":""}>Oldest</a></li>
                                <li><a href="./Blog-List?cid=${param['cid']}&search=${param['search']}&index=${index}&sort=order by b.blogTitle desc" ${param['sort']=="order by b.blogTitle desc"?"style='font-weight: bold'":""}>Title Z-A</a>      </li>
                                <li><a href="./Blog-List?cid=${param['cid']}&search=${param['search']}&index=${index}&sort=order by b.blogTitle asc" ${param['sort']=="order by b.blogTitle asc"?"style='font-weight: bold'":""}>Title A-Z</a>     </li> 
                            </ul>
                        </div>
                        <div class="blog__sidebar__item">
                            <h4>Categories </h4>
                            <ul>
                                <li><a  ${param['cid']==""?"style='font-weight: bold'":""} href="./Blog-List?cid=&search=${param['search']}&index=${index}&sort=${param['sort']}">All</a></li>
                                    <c:forEach var="c" items="${cate}">
                                    <li><a href="./Blog-List?cid=${c.categoryBlogId}&search=${param['search']}&index=${index}&sort=${param['sort']}"
                                           ${param['cid']==c.categoryBlogId?"style='font-weight: bold'":""}>${c.categoryBlogName}</a></li>
                                    </c:forEach>
                            </ul>
                        </div>
                    </div>
                </div>-->
                <div class="col-lg-12 col-md-12 order-md-1 order-1">

                    <div class="blog__details__text" style="display: flex; flex-direction: column; align-items: center">
                        <img width="30%" src="${b.thumbnail}" alt="">
                        <div>
                            ${b.blogDetail}
                        </div>
                    </div>
                    <div class="blog__details__content">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="blog__details__author">
                                    <div class="blog__details__author__pic">
                                    </div>
                                    <div class="blog__details__author__text">
                                        <h6>Ngày đăng:  </h6> <span>${b.blogDate}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="blog__details__widget">
                                    <ul>
                                        <li><span>Thể loại bài viết:</span> ${b.category}</li>
                                        <li><span>Tags:</span> All, Trending, Life Style</li>
                                    </ul>
<!--                                    <div class="blog__details__social">
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                        <a href="#"><i class="fa fa-google-plus"></i></a>
                                        <a href="#"><i class="fa fa-linkedin"></i></a>
                                        <a href="#"><i class="fa fa-envelope"></i></a>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Blog Details Section End -->

   

    <!-- Footer Section Begin -->
      <footer class="footer spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="footer__about">
                            <div class="footer__about__logo">
                                <a href="home"><img src="img/logo.png" alt=""></a>
                            </div>
                            <ul>
                                <li>Của hàng HCM: 66 Nguyễn Trí Sách, Phường 15 Tân Bình</li>
                                <li>Cửa hàng HN: 63 Yên Lãng - Đống Đa - Hà Nội</li>
                                <li>Phone: 0123456789</li>
                                <li>Email: shophoaqua@gmail.com</li>
                            </ul>
                        </div>
                    </div>
                    
                    
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer__copyright">
                            <div class="footer__copyright__text"><p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p></div>
                            <div class="footer__copyright__payment"><img src="img/payment-item.png" alt=""></div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="./js/jquery-3.3.1.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/jquery.nice-select.min.js"></script>
    <script src="./js/jquery-ui.min.js"></script>
    <script src="./js/jquery.slicknav.js"></script>
    <script src="./js/mixitup.min.js"></script>
    <script src="./js/owl.carousel.min.js"></script>
    <script src="./js/main.js"></script>



</body>

</html>