<%@page import="model.Product"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean class="utils.Helpers" id="helpers"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Ogani | Template</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <!--        <link rel="stylesheet" href="css/detailProduct.css" type="text/css">-->
        <style>
            .product__pagination label {
                display: inline-block;
                width: 30px;
                height: 30px;
                border: 1px solid #b2b2b2;
                font-size: 14px;
                color: #b2b2b2;
                font-weight: 700;
                line-height: 28px;
                text-align: center;
                margin-right: 16px;
                -webkit-transition: all, 0.3s;
                -moz-transition: all, 0.3s;
                -ms-transition: all, 0.3s;
                -o-transition: all, 0.3s;
                transition: all, 0.3s;
            }

            .product__pagination label:hover{
                background: #7fad39;
                border-color: #7fad39;
                color: #ffffff;
                cursor: pointer;
            }

            .product__pagination label.active {
                background-color: #7fad39;
            }

            .sidebar__item ul li.active {
                font-weight: bold
            }

            .sidebar__item ul li label{
                font-size: 16px;
                color: #1c1c1c;
                line-height: 39px;
                display: block;
            }

            .sidebar__item ul li label:hover{
                cursor: pointer;
                color: #7fad39;
            }

            .sidebar__item__size label.active {
                background: #7fad39;
                color: white;
                border: 1px solid red;
            }

            .sidebar__item__box.scroll {
                max-height: 27vh;
                overflow: auto
            }
            .product__details__pic{
                width: 90%;
            }
            .v3_select{
                pointer-events: none;
            }

        </style>
        <style>
            *{
                margin: 0;
                padding: 0;
            }
            .rate {
                float: left;
                height: 46px;
                padding: 0 10px;
            }
            .rate:not(:checked) > input {
                position:absolute;
                top:-9999px;
            }
            .rate:not(:checked) > label {
                float:right;
                width:1em;
                overflow:hidden;
                white-space:nowrap;
                cursor:pointer;
                font-size:30px;
                color:#ccc;
            }
            .rate:not(:checked) > label:before {
                content: '★ ';
            }
            .rate > input:checked ~ label {
                color: #ffc700;
            }
            .rate:not(:checked) > label:hover,
            .rate:not(:checked) > label:hover ~ label {
                color: #deb217;
            }
            .rate > input:checked + label:hover,
            .rate > input:checked + label:hover ~ label,
            .rate > input:checked ~ label:hover,
            .rate > input:checked ~ label:hover ~ label,
            .rate > label:hover ~ input:checked ~ label {
                color: #c59b08;
            }
        </style>
    </head>
    <body>
        <jsp:include page="../header.jsp" flush="true" >
            <jsp:param name="shop" value="active" />
        </jsp:include>
        <c:if test="${not empty param.message}">

            <c:if test="${param.alert.equals('danger')}">
                <div id="myAlert" style="right: 50%; z-index: 999" class="alert alert-danger alert-dismissible fade show text-center position-absolute " role="alert">
                    ${param.message}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </c:if>
            <c:if test="${param.alert.equals('Warning')}">
                <div id="myAlert" style="right: 50%; z-index: 999" class="alert alert-warning alert-dismissible fade show text-center position-absolute " role="alert">
                    ${param.message}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </c:if>
        </c:if>

        <!-- Hero Section Begin -->
        <section class="hero hero-normal">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="hero__categories">

                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="hero__search">
                            <div class="hero__search__form">
                                <form action="${pageContext.request.contextPath}/shop" method='post'>

                                    <input type="text" name='search' value='${search}' placeholder="Bạn đang tìm kiếm gì ?">
                                    <button type="submit" class="site-btn">Tìm kiếm</button>
                                </form>
                            </div>
                            <div class="hero__search__phone">
                                <div class="hero__search__phone__icon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="hero__search__phone__text">
                                    <h5>0123456789</h5>
                                    <span>Hỗ trợ từ 7:00 - 18:00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <% Product p = (Product)request.getAttribute("productDetail"); %>                        
            <section class="breadcrumb-section set-bg" data-setbg="img/breadcrumb.jpg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center">

                        </div> <section class="product-details spad">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="product__details__pic">
                                            <div class="product__details__pic__item">
                                                <img class="product__details__pic__item--large" src="img/product/<%= p.getImage().getiUrl() %>" alt="">
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="product__details__text">
                                            <h3><%=p.getpName()%></h3>
                                            <p>Trung bình đánh giá: ${avgStar}<span style="color: yellow">&#9733;</span></p>

                                            <div class="product__details__price">Giá : <%=helpers.formatInt(p.getPrice())%> VNĐ</div>
                                            <p>
                                                Mô tả sản phẩm: <%=p.getpDetail()%>
                                            </p>
                                            <div class="product__details__quantity">
                                                <div class="quantity">
                                                    <label for="quantity">Số lượng</label>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input class="form-control" type="text" value="1" name="quantity" id="numberInput-quantity" oninput="validateInput(this)">
                                                            <span class="input-group-text">kg</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <span class="text-success" id="quantityAvailable"><%=p.getQuantity()%> kg có sẳn</span>
                                            </div>

                                            <br> <!-- Add a line break here to move the button to the next line -->

                                            <button onclick="buyProduct(<%=p.getPid()%>)" id="checkout" name="id" value="" class="btn btn-round btn-danger" style="padding: 16px 28px 14px; margin-right: 6px; margin-bottom: 5px; margin-top: 20px"> Mua ngay</button>

                                            <button onclick="onClickAddToCart(<%=p.getPid()%>, 1)" class="primary-btn btn btn-success" style="color: white; margin-top: 20px">ADD TO CARD</button>  
                                            <ul>
                                                <li><b>Xuất xứ: </b> <span><%=p.getpBrand()%></span></li>
                                                <li><b>Mã sản phẩm: </b> <span><%=p.getpSKU()%></span></li>
                                                <li>
                                                    <b>Share on</b>
                                                    <div class="share">
                                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                                        <a href="#"><i class="fa fa-instagram"></i></a>
                                                        <a href="#"><i class="fa fa-pinterest"></i></a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <c:if test="${param['index']==null }">   
                                <c:set var = "index" scope = "page" value = "1"/>
                            </c:if>
                            <c:if test="${param['index']!=null}">
                                <c:set var = "index" scope = "page" value = "${param['index']}"/>
                            </c:if>
                            <div class="product-image-gallery">
                                <h2>Đánh giá sản phẩm:</h2>
                                <br></br>
                                <h5>Trung bình đánh giá sản phẩm: ${avgStar}<span style="color: yellow">&#9733;</span></h5>
                                <div class="image-gallery">
                                    <c:forEach items="${listFb}" var="fb">
                                        <div style="display: block !important;height: 220px;position: relative; width: 100% !important;padding: 20px;box-shadow: 0 0 5px 5px lightgray; border-radius: 10px; margin: 15px;">
                                            <h4 style="text-align: left;">${fb.customer.cusName}</h4>
                                            <p style="text-align: left;">${fb.fbDate}</p>
                                            
                                            <p style="text-align: left;"></p>
                                            <div class="rate">
                                                <input type="radio" disabled  id="star5" name="rate${fb.fbId}" value="5" ${fb.fbStar==5?"checked":""}/>
                                                <label for="star5" title="text">5 stars</label>
                                                <input type="radio" disabled id="star4" name="rate${fb.fbId}" value="4" ${fb.fbStar==4?"checked":""}/>
                                                <label for="star4" title="text">4 stars</label>
                                                <input type="radio" disabled id="star3" name="rate${fb.fbId}" value="3" ${fb.fbStar==3?"checked":""}/>
                                                <label for="star3" title="text">3 stars</label>
                                                <input type="radio" disabled id="star2" name="rate${fb.fbId}" value="2" ${fb.fbStar==2?"checked":""} />
                                                <label for="star2" title="text">2 stars</label>
                                                <input type="radio" disabled id="star1" name="rate${fb.fbId}" value="1" ${fb.fbStar==1?"checked":""} />
                                                <label for="star1" title="text">1 star</label>
                                                <p style="text-align: left;">Nội dung: ${fb.fbContent}</p>
                                            </div>
                                                
                                            <img style="position: absolute; right: 40px; top:20px;" src="${fb.fbImage}" onclick="changeSize${fb.fbId}()" id="feedbackImg${fb.fbId}" width="120px"  >
                                        </div>
                                        <script>
                                            function changeSize${fb.fbId}() {
                                                var curSize = document.getElementById("feedbackImg${fb.fbId}").width;
                                                console.log(curSize)
                                                if (curSize == 120) {
                                                    document.getElementById("feedbackImg${fb.fbId}").style.width = "190px";
                                                } else {
                                                    document.getElementById("feedbackImg${fb.fbId}").style.width = "120px";

                                                }
                                            }
                                        </script>
                                    </c:forEach>
                                    <div class="col-lg-12">
                                        <div class="pagination-arena " style="margin-left: 40%">
                                            <ul class="pagination">
                                                <li class="page-item"><a href="./productDetail?pid=${param['pid']}&status=${param['status']}&index=1" class="page-link"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                                                <li  style="${index-2<1?"display:none;":""}" class="page-item">
                                                    <a href="./productDetail?pid=${param['pid']}&status=${param['status']}&index=${index-2}" class="page-link ">${index-2}</a></li>
                                                <li  style="${index-1<1?"display:none;":""}" class="page-item">
                                                    <a href="./productDetail?pid=${param['pid']}&status=${param['status']}&index=${index-1}" class="page-link ">${index-1}</a></li>
                                                <li class="page-item ">
                                                    <a href="./productDetail?pid=${param['pid']}&status=${param['status']}&index=${index}" class="page-link">${index}</a>
                                                </li>
                                                <li  style="${index+1>numberPage?"display:none;":""}" class="page-item">
                                                    <a href="./productDetail?pid=${param['pid']}&status=${param['status']}&index=${index+1}" class="page-link " >${index+1}</a></li>
                                                <li  style="${index+2>numberPage?"display:none;":""}" class="page-item">
                                                    <a href="./productDetail?pid=${param['pid']}&status=${param['status']}&index=${index+2}" class="page-link ">${index+2}</a></li>
                                                <li><a href="./productDetail?pid=${param['pid']}&status=${param['status']}&index=${numberPage}" class="page-link"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </section>
            <!-- Breadcrumb Section End -->

            <!-- Product Details Section Begin -->

            <!-- Product Details Section End -->

            <!-- Related Product Section Begin -->

            <!-- Related Product Section End -->

            <!-- Footer Section Begin -->
            <footer class="footer spad">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="footer__about">
                                <div class="footer__about__logo">
                                    <a href="home"><img src="img/logo.png" alt=""></a>
                                </div>
                                <ul>
                                    <li>Của hàng HCM: 66 Nguyễn Trí Sách, Phường 15 Tân Bình</li>
                                    <li>Cửa hàng HN: 63 Yên Lãng - Đống Đa - Hà Nội</li>
                                    <li>Phone: 0123456789</li>
                                    <li>Email: shophoaqua@gmail.com</li>
                                </ul>
                            </div>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer__copyright">
                                <div class="footer__copyright__text"><p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                        Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p></div>
                                <div class="footer__copyright__payment"><img src="img/payment-item.png" alt=""></div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- Footer Section End -->

            <!-- Js Plugins -->
            <script src="js/jquery-3.3.1.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/jquery.nice-select.min.js"></script>
            <script src="js/jquery-ui.min.js"></script>
            <script src="js/jquery.slicknav.js"></script>
            <script src="js/mixitup.min.js"></script>
            <script src="js/owl.carousel.min.js"></script>
            <script src="js/main.js"></script>  


            <script>
                function onClickAddToCart(idItem, quantity) {
                    console.log(idItem);
                    if (quantity === 0) {
                        event.preventDefault();
                        alert("We apologize for this inconvenience." +
                                "\nThe item is currently out of stock, please comeback later");
                    } else {
                        console.log(quantity);
                        let href = '/SWP391_FA23_SE1736_TEAM1/AddorCheckRedirectController?productId=' + idItem + '&quantity=' + quantity + '&path=productDetail';
                        window.location.href = href;
                    }
                }

                function buyProduct(idItem) {
                    const inputNum = document.getElementById("numberInput-quantity");
                    console.log(idItem, inputNum.value);
                    onClickAddToCart(idItem, inputNum.value);
                }

            </script>
            <script>
                // Sử dụng JavaScript hoặc jQuery để tự động đóng alert sau một khoảng thời gian
                setTimeout(function () {
                    $("#myAlert").alert('close'); // Sử dụng jQuery
                    // Hoặc sử dụng JavaScript thuần
                    // document.getElementById('myAlert').style.display = 'none';
                }, 3000); // 3000 milliseconds tương ứng với 3 giây
                
                
                 function validateInput(input) {
                    // Xóa mọi ký tự không phải số hoặc dấu chấm thập phân
                    input.value = input.value.replace(/[^1-9.]/g, '');

                    // Kiểm tra xem có nhiều hơn một dấu chấm thập phân không
                    var dotCount = (input.value.match(/\./g) || []).length;
                    if (dotCount > 1) {
                        // Nếu có nhiều hơn một dấu chấm, loại bỏ ký tự cuối cùng
                        input.value = input.value.slice(0, -1);
                    }
                }

            </script>
</html>
