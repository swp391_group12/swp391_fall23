<%-- 
    Document   : UserManage
    Created on : Sep 18, 2023, 8:44:34 PM
    Author     : Admin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<jsp:useBean class="utils.Helpers" id="helpers"/>
<!DOCTYPE html>
<!DOCTYPE html>

<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>Danh sách Khách Hàng | Quản trị Admin</title>

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <!-- or -->
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">

        <!-- Font-icon css-->
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    </head>

    <body onload="time()" class="app sidebar-mini rtl">
        <!-- Navbar-->
        <header class="app-header">
            <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar"
                                            aria-label="Hide Sidebar"></a>
            <!-- Navbar Right Menu-->
            <ul class="app-nav">


                <!-- User Menu-->
                <li><a class="app-nav__item" href="home"><i class='bx bx-log-out bx-rotate-180'></i> </a>

                </li>
            </ul>
        </header>
        <!-- Sidebar menu-->
        <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
        <jsp:include page="../navbar.jsp" flush="true" >
            <jsp:param name="seller" value="active" />
        </jsp:include>
        <main class="app-content">
            <div class="app-title">
                <ul class="app-breadcrumb breadcrumb side">
                    <li class="breadcrumb-item active"><a href="seller"><b>Danh sách khách hàng</b></a></li>
                </ul>
                <div id="clock"></div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body" style="height:322px">
                            <div class="d-flex flex-column align-items-center text-center">
                                <img src="img/icon.png" alt="Admin" class="rounded-circle p-1 bg-primary" width="110">
                                <div class="mt-3">
                                    <h4>${u.userName}</h4>
                                    <p class="text-secondary mb-1">${u.phone}</p>
                                    <p class="text-secondary mb-1">${u.email}</p>
                                    <p class="text-secondary mb-1">${u.store.stoName}</p>
                                    <span
                                                    class="badge ${u.status == 'true' ? 'bg-success' : 'bg-danger' }">${u.status == 'true' ? 'Hoạt động' : 'Khóa'}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body" style="height:322px">
                                    <form action="sellerdetails" method="get">
                                        <input type="hidden" value="${u.userId}" name="userId">
                                        <h5 class="d-flex align-items-center mb-3">Thống kê</h5>
                                        <div class="d-flex align-items-center mb-3">
                                            <select name="month" id="month">
                                                <option value="1" ${m == 1 ? 'selected' : ''}>Tháng 1</option>
                                                <option value="2" ${m == 2 ? 'selected' : ''}>Tháng 2</option>
                                                <option value="3" ${m == 3 ? 'selected' : ''}>Tháng 3</option>
                                                <option value="4" ${m == 4 ? 'selected' : ''}>Tháng 4</option>
                                                <option value="5" ${m == 5 ? 'selected' : ''}>Tháng 5</option>
                                                <option value="6" ${m == 6 ? 'selected' : ''}>Tháng 6</option>
                                                <option value="7" ${m == 7 ? 'selected' : ''}>Tháng 7</option>
                                                <option value="8" ${m == 8 ? 'selected' : ''}>Tháng 8</option>
                                                <option value="9" ${m == 9 ? 'selected' : ''}>Tháng 9</option>
                                                <option value="10" ${m == 10 ? 'selected' : ''}>Tháng 10</option>
                                                <option value="11" ${m == 11 ? 'selected' : ''}>Tháng 11</option>
                                                <option value="12" ${m == 12 ? 'selected' : ''}>Tháng 12</option>
                                            </select>
                                            <select name="year" id="year" size="1">
                                                <% 
                                                int currentYear = java.util.Calendar.getInstance().get(java.util.Calendar.YEAR);
                                                for (int year = currentYear; year >= 2020; year--) {
                                                %>
                                                <%
                                                int y = (int) request.getAttribute("y");
                                                %>
                                                <option value="<%= year %>" <%=year == y ? "selected" : ""%>><%= year %></option>
                                                <%
                                                }
                                                %>
                                            </select>
                                            <input class ="button-date" type="submit" value="Tìm kiếm">
                                        </div>
                                        <p style="font-size:16px">Số đơn hàng : ${one.totalOrder} đơn</p>
                                        <div class="progress mb-3" style="height: 15px">
                                            <div class="progress-bar bg-primary" role="progressbar" style="width: ${tld}%"  aria-valuemin="0" aria-valuemax="100">${tld}%</div>
                                        </div>
                                        <p style="font-size:16px">Doanh số: ${helpers.formatIntVN(one.totalPrice)} VNĐ</p>
                                        <div class="progress mb-3" style="height: 15px">
                                            <div class="progress-bar bg-primary" role="progressbar" style="width: ${tlt}%"  aria-valuemin="0" aria-valuemax="100">${tlt}%</div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <style>
            select{
                margin-right:10px;
                border: 2px solid black; /* Viền đen 2px */
                border-radius: 10px;
                size: 20px;
            }
            .button-date{
                border: none; /* Bỏ viền */
                border-radius: 10px; /* Làm tròn nút với bán kính 10px */
                background-color: #FBE2C5; /* Màu nền của nút */
                color: #F59D39; /* Màu văn bản */
                margin: auto 0;
                cursor: pointer;
                width:100px;
                font-weight: bold;
            }
        </style>

        <!-- Essential javascripts for application to work-->
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="src/jquery.table2excel.js"></script>
        <script src="js/main.js"></script>
        <!-- The javascript plugin to display page loading on top-->
        <script src="js/plugins/pace.min.js"></script>
        <!-- Page specific javascripts-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
        <!-- Data table plugin-->
        <script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript">$('#sampleTable').DataTable();</script>
        <script>

            function confirmDelete(button) {
                // Lấy form cha của button được nhấn
                var form = button.closest('.deleteForm');

                // Lấy giá trị userId từ form
                var cusId = form.querySelector('.cusId').value;

                if (confirm('Bạn có chắc chắn muốn xóa người dùng có id: ' + cusId + ' không?')) {
                    // Gán giá trị userId vào form và submit
                    form.submit();
                }
            }


            //Thời Gian
            function time() {
                var today = new Date();
                var weekday = new Array(7);
                weekday[0] = "Chủ Nhật";
                weekday[1] = "Thứ Hai";
                weekday[2] = "Thứ Ba";
                weekday[3] = "Thứ Tư";
                weekday[4] = "Thứ Năm";
                weekday[5] = "Thứ Sáu";
                weekday[6] = "Thứ Bảy";
                var day = weekday[today.getDay()];
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yyyy = today.getFullYear();
                var h = today.getHours();
                var m = today.getMinutes();
                var s = today.getSeconds();
                m = checkTime(m);
                s = checkTime(s);
                nowTime = h + " giờ " + m + " phút " + s + " giây";
                if (dd < 10) {
                    dd = '0' + dd
                }
                if (mm < 10) {
                    mm = '0' + mm
                }
                today = day + ', ' + dd + '/' + mm + '/' + yyyy;
                tmp = '<span class="date"> ' + today + ' - ' + nowTime +
                        '</span>';
                document.getElementById("clock").innerHTML = tmp;
                clocktime = setTimeout("time()", "1000", "Javascript");

                function checkTime(i) {
                    if (i < 10) {
                        i = "0" + i;
                    }
                    return i;
                }
            }
            //In dữ liệu
            var myApp = new function () {
                this.printTable = function () {
                    var tab = document.getElementById('sampleTable');
                    var win = window.open('', '', 'height=700,width=700');
                    win.document.write(tab.outerHTML);
                    win.document.close();
                    win.print();
                }
            }

        </script>
    </body>

</html>
