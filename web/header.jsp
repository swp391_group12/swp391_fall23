<%-- 
    Document   : header
    Created on : Oct 5, 2023, 12:00:00 AM
    Author     : Trung Kien
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean class="utils.Helpers" id="helpers"/>
<!DOCTYPE html>
<style>
    .store{
        margin-top: 5px;
        width: 100%;
        height: 75%;
        max-width: 140px;
        background: #FFFFFF;
        padding: 0px 8px;
        border-radius: 5px;
    }
    .v3_location_title{
        font-size: 12px;
        font-weight: bold;
        margin: 0;
        padding: 0;
        display: inline-block;
        height: 12px;
    }

    .v3_select{
        border: none;
        font-size: 13px;
        font-weight: bold;
    }
</style>

<header class="header">
    <div class="header__top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="header__top__left">
                        <ul>
                            <li><i class="fa fa-envelope"></i> shophoaqua@gmail.com</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div class="store">
                        <p class="v3_location_title">Xem và mua hàng tại:</p>
                        <div class="v3_location-dropdown-title">
                            <select id="citySelect" class="v3_select" onchange="sendSelectedValue()">
                                <c:forEach items="${listStores}" var="item">

                                    <option value="${item.stoId}">${item.stoName}</option> 

                                </c:forEach>
                            </select>

                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="header__top__right">
                        <div class="header__top__right__social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-pinterest-p"></i></a>
                        </div>
                        <div class="header__top__right__language">
                            <div>Tiếng Việt</div>
                        </div>
                        <div class="header__top__right__auth ">
                            <div class="header__menu" style="padding: 12px 0">
                                <c:if test="${sessionScope.account == null}">
                                    <a href="login"><i class="fa fa-user"></i> Đăng nhập</a>
                                </c:if>
                                <c:if test="${sessionScope.account != null}">
                                    <ul>
                                        <li><a><i class="fa fa-user"></i> ${sessionScope.account.userName}</a>
                                            <ul class="header__menu__dropdown">
                                                <li class="float-left"><a href="MyOrder"><i class="fa fa-user"></i>Đơn hàng</a></li>
                                                <li class="float-left"><a href="profile"><i class="fa fa-user"></i>Tài khoản</a></li>
                                                <li class="float-left"><a href="logout"><i class="fa fa-outdent"></i>Đăng xuất</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header__bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="header__logo">
                        <a href="home"><img src="img/logo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <nav class="header__menu">
                        <ul>
                            <li class="${param.home}"><a href="<c:url value="/home"/>">Trang Chủ</a></li>
                            <li class="${param.shop}"><a href="<c:url value="/shop"/>">Sản Phẩm</a></li>
                            <li class="${param.blog}"><a href="./Blog-List">Bài viết</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-3">
                    <div class="header__cart">
                        <ul>
                            <li><a href="#"><i class="fa fa-heart"></i> <span>1</span></a></li>
                            <li>
                                <a href="<c:url value="/shopping-cart"/>">
                                    <i class="fa fa-shopping-bag"></i>
                                    <c:if test="${cookie.numOfProducts.value != null && cookie.numOfProducts.value != 0}">
                                        <c:set var="numProduct" value="${cookie.numOfProducts.value}" />
                                        <span>${numProduct}</span>
                                    </c:if>
                                </a>
                            </li>
                        </ul>
                        <div class="header__cart__price">Tổng: 
                            <c:if test="${cookie.totalPrice != null}">
                                <c:set var="totalPrice" value="${cookie.totalPrice.value}" />
                                <span>${helpers.formatIntVN(totalPrice)} VNĐ</span>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>                  
    <div class="humberger__open">
        <i class="fa fa-bars"></i>
    </div>
</div>
</header>
<style>
    .header.fixed{
        position: fixed;
        width: 100%;
        top: 0;
        z-index: 1000;
        background: #f5f5f5;

    }
    .header__bottom{
        background: #F5FFFA;
    }


</style>

<script>
    window.addEventListener('scroll', function () {
        var navbar = document.querySelector('.header');
        var scrollPosition = window.scrollY;

        if (scrollPosition > 100) {
            navbar.classList.add('fixed');
        } else {
            navbar.classList.remove('fixed');
        }
    });

// Thêm đoạn mã sau để xử lý việc cuộn lên đầu trang
    window.addEventListener('scroll', function () {
        var navbar = document.querySelector('.header');

        var scrollPosition = window.scrollY;

        if (scrollPosition === 0) {
            navbar.classList.remove('fixed');
        }
    });
</script>
<script>
    // Lưu giá trị được chọn vào Local Storage khi thay đổi
    function setCookie(name, value, days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    }

    // Hàm lấy giá trị từ cookie
    function getCookie(name) {
        var nameEQ = name + "=";
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i];
            while (cookie.charAt(0) === ' ') {
                cookie = cookie.substring(1, cookie.length);
            }
            if (cookie.indexOf(nameEQ) === 0) {
                return cookie.substring(nameEQ.length, cookie.length);
            }
        }
        return null;
    }

    if (getCookie('selectedCity') === null) {
        setCookie('selectedCity', 1, 365);
    }

    document.getElementById('citySelect').addEventListener('change', function () {
        localStorage.setItem('selectedCity', this.value);
        var selectedValue = this.value;

        setCookie('selectedCity', selectedValue, 365);
        location.reload();
    });

    // Lấy giá trị từ Local Storage và đặt làm giá trị mặc định khi tải lại trang
    document.addEventListener('DOMContentLoaded', function () {
        var selectedCity = localStorage.getItem('selectedCity');
        if (selectedCity) {
            document.getElementById('citySelect').value = selectedCity;
        }
    });
</script>

<script>
    function sendSelectedValue() {
        var selectedValue = document.getElementById("citySelect").value;

        // Sử dụng Ajax để gửi giá trị đến servlet
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "/home?selectedValue=" + selectedValue, true);
        xhr.send();
    }
</script>

