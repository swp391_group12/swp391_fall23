
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:useBean class="dal.CategoryDAO" id="cdao"/>
<jsp:useBean class="dal.StatusDAO" id="stdao"/>
<jsp:useBean class="dal.StoreDAO" id="stodao"/>
<jsp:useBean class="utils.Helpers" id="helpers"/>


<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Chi Tiết sản phẩm | Quản trị Admin</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <!-- Font-icon css-->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <!-- or -->
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
        <script src="http://code.jquery.com/jquery.min.js" type="text/javascript"></script>
        <script>

            function readURL(input, thumbimage) {
                if (input.files && input.files[0]) { //Sử dụng  cho Firefox - chrome
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#thumbimage").attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                } else { // Sử dụng cho IE
                    $("#thumbimage").attr('src', input.value);

                }
                $("#thumbimage").show();
                $('.filename').text($("#uploadfile").val());
                $('.Choicefile').css('background', '#14142B');
                $('.Choicefile').css('cursor', 'default');
                $(".removeimg").show();
                $(".Choicefile").unbind('click');

            }
            $(document).ready(function () {
                $(".Choicefile").bind('click', function () {
                    $("#uploadfile").click();

                });
                $(".removeimg").click(function () {
                    $("#thumbimage").attr('src', '').hide();
                    $("#myfileupload").html('<input type="file" id="uploadfile"  onchange="readURL(this);" /> <input id="removeTag" type="hidden" name="removeImage" value="true" >');
                    $(".removeimg").hide();
                    $(".Choicefile").bind('click', function () {
                        $("#uploadfile").click();
                    });
                    $('.Choicefile').css('background', '#14142B');
                    $('.Choicefile').css('cursor', 'pointer');
                    $(".filename").text("");
                });
            })
        </script>
    </head>

    <body class="app sidebar-mini rtl">
        <style>
            .Choicefile {
                display: block;
                background: #14142B;
                border: 1px solid #fff;
                color: #fff;
                width: 150px;
                text-align: center;
                text-decoration: none;
                cursor: pointer;
                padding: 5px 0px;
                border-radius: 5px;
                font-weight: 500;
                align-items: center;
                justify-content: center;
            }

            .Choicefile:hover {
                text-decoration: none;
                color: white;
            }

            #uploadfile,
            .removeimg {
                display: none;
            }

            #thumbbox {
                position: relative;
                width: 100%;
                margin-bottom: 20px;
            }

            .removeimg {
                height: 25px;
                position: absolute;
                background-repeat: no-repeat;
                top: 5px;
                left: 5px;
                background-size: 25px;
                width: 25px;
                /* border: 3px solid red; */
                border-radius: 50%;

            }

            .removeimg::before {
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
                content: '';
                border: 1px solid red;
                background: red;
                text-align: center;
                display: block;
                margin-top: 11px;
                transform: rotate(45deg);
            }

            .removeimg::after {
                /* color: #FFF; */
                /* background-color: #DC403B; */
                content: '';
                background: red;
                border: 1px solid red;
                text-align: center;
                display: block;
                transform: rotate(-45deg);
                margin-top: -2px;
            }
        </style>
        <!-- Navbar-->
        <header class="app-header">
            <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar"
                                            aria-label="Hide Sidebar"></a>
            <!-- Navbar Right Menu-->
            <ul class="app-nav">


                <!-- User Menu-->
                <li><a class="app-nav__item" href="home"><i class='bx bx-log-out bx-rotate-180'></i> </a>

                </li>
            </ul>
        </header>
        <!-- Sidebar menu-->
        <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
        <jsp:include page="../navbar.jsp" flush="true" >
            <jsp:param name="product" value="active" />
        </jsp:include>
        <main class="app-content">
            <div class="app-title">
                <ul class="app-breadcrumb breadcrumb">
                    <li class="breadcrumb-item"><a href="product-listing">Danh sách sản phẩm</a></li>
                    <li class="breadcrumb-item"><a href="#">Sản phẩm chi tiết</a></li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tile">
                        <h3 class="tile-title">Thông tin sản phẩm</h3>
                        <div class="tile-body">
                            <form action="product-edit" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="pid" value="${data.pid}">
                                <input type="hidden" name="action" value="${url}">
                                <div class="row element-button">
                                </div>
                                <div class="row">

                                    <div class="form-group col-md-3">
                                        <label class="control-label">ID </label>
                                        <input class="form-control" type="text" readonly="" value="${data.pid}" placeholder="">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label class="control-label">Tên sản phẩm</label>
                                        <input class="form-control" type="text" name="pName" value="${data.pName}" required="">
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label class="control-label">Số lượng</label>
                                        <input class="form-control" type="text" name="quantity" value="${data.quantity}" required="">
                                    </div><!-- comment -->

                                    <div class="form-group col-md-3">
                                        <label class="control-label">Giá nhập</label>
                                        <input class="form-control" type="text" name="priceIn" value="${helpers.formatInt(data.priceIn)}" required="">
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label class="control-label">Giá bán</label>
                                        <input class="form-control" type="text" name="price" value="${helpers.formatInt(data.price)}" required="">
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label class="control-label">Xuất xứ</label>
                                        <input class="form-control" type="text" name="pBrand" value="${data.pBrand}" required="">
                                    </div>

                                    <div class="form-group col-md-3 ">
                                        <label for="exampleSelect1" class="control-label">Trạng thái sản phẩm</label>
                                        <select class="form-control" id="exampleSelect1" name="stid">
                                            <option>-- Chọn tình trạng --</option>
                                            <c:forEach items="${stdao.all}" var="st">
                                                <option value="${st.stid}" ${st.stid == data.status.stid ? 'selected' : ''}>${st.stName}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="exampleSelect1" class="control-label">Danh mục sản phẩm</label>
                                        <select class="form-control" id="exampleSelect1" name="cid">
                                            <option>-- Chọn danh mục --</option>
                                            <c:forEach items="${cdao.all}" var="c">
                                                <option value="${c.cid}" ${c.cid == data.category.cid ? 'selected' : ''}>${c.cName}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label class="control-label">SKU</label>
                                        <input class="form-control" type="text" name="pSKU" value="${data.pSKU}"  required="">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label class="control-label">Mô tả sản phẩm</label>
                                        <textarea class="form-control" name="pDetail" id="mota">${data.pDetail}</textarea>
                                        <script>CKEDITOR.replace('mota');</script>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">Ảnh sản phẩm</label>
                                        <div id="myfileupload">
                                            <input type="file" id="uploadfile" name="ImageUpload" onchange="readURL(this);" />
                                        </div>
                                        <div id="thumbbox">
                                            <img height="450" width="400" alt="Thumb image" id="thumbimage" src="img/product/${data.image.iUrl}" />

                                        </div>
                                        <c:if test="${noEdit == null}">
                                            <div id="boxchoice">
                                                <a href="javascript:" class="Choicefile"><i class="fas fa-cloud-upload-alt"></i> Chọn ảnh</a>
                                                <p style="clear:both"></p>
                                            </div>
                                        </c:if>
                                    </div>

                                </div>
                                <c:if test="${noEdit == null}">
                                    <button class="btn btn-save" type="submit">Lưu lại</button>
                                    <a class="btn btn-cancel" href="product-listing">Hủy bỏ</a>
                                </c:if>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>


        <!--
        MODAL CHỨC VỤ 
        -->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
             data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group  col-md-12">
                                <span class="thong-tin-thanh-toan">
                                    <h5>Thêm mới nhà cung cấp</h5>
                                </span>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="control-label">Nhập tên chức vụ mới</label>
                                <input class="form-control" type="text" required>
                            </div>
                        </div>
                        <BR>
                        <button class="btn btn-save" type="button">Lưu lại</button>
                        <a class="btn btn-cancel" data-dismiss="modal" href="#">Hủy bỏ</a>
                        <BR>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
        <!--
      MODAL
        -->



        <!--
        MODAL DANH MỤC
        -->
        <div class="modal fade" id="adddanhmuc" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
             data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group  col-md-12">
                                <span class="thong-tin-thanh-toan">
                                    <h5>Thêm mới danh mục </h5>
                                </span>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="control-label">Nhập tên danh mục mới</label>
                                <input class="form-control" type="text" required>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="control-label">Danh mục sản phẩm hiện đang có</label>
                                <ul style="padding-left: 20px;">
                                    <li>Bàn ăn</li>
                                    <li>Bàn thông minh</li>
                                    <li>Tủ</li>
                                    <li>Ghế gỗ</li>
                                    <li>Ghế sắt</li>
                                    <li>Giường người lớn</li>
                                    <li>Giường trẻ em</li>
                                    <li>Bàn trang điểm</li>
                                    <li>Giá đỡ</li>
                                </ul>
                            </div>
                        </div>
                        <BR>
                        <button class="btn btn-save" type="button">Lưu lại</button>
                        <a class="btn btn-cancel" data-dismiss="modal" href="#">Hủy bỏ</a>
                        <BR>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
        <!--
      MODAL
        -->




        <!--
        MODAL TÌNH TRẠNG
        -->
        <div class="modal fade" id="addtinhtrang" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
             data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group  col-md-12">
                                <span class="thong-tin-thanh-toan">
                                    <h5>Thêm mới tình trạng</h5>
                                </span>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="control-label">Nhập tình trạng mới</label>
                                <input class="form-control" type="text" required>
                            </div>
                        </div>
                        <BR>
                        <button class="btn btn-save" type="button">Lưu lại</button>
                        <a class="btn btn-cancel" data-dismiss="modal" href="#">Hủy bỏ</a>
                        <BR>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
        <!--
      MODAL
        -->

        <div class="modal fade" id="addsize" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
             data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <form action="product-size-add" method="POST">
                        <input type="hidden" name="pid" value="${data.pid}">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group  col-md-12">
                                    <span class="thong-tin-thanh-toan">
                                        <h5>Add New Size</h5>
                                    </span>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="exampleSelect1" class="control-label">Size</label>
                                    <select class="form-control" id="exampleSelect1" name="sid">
                                        <c:forEach items="${sdao.getNotExist(data.size)}" var="s">
                                            <option value="${s.sid}" >${s.sName}</option>
                                        </c:forEach>
                                    </select>
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="exampleSelect1" class="control-label">Quantity</label>
                                    <input class="form-control" type="text" name="quantity">
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="exampleSelect1" class="control-label">Price</label>
                                    <input class="form-control" type="text" name="price">
                                </div>
                            </div>
                            <BR>
                            <button class="btn btn-save" type="submit">Lưu lại</button>
                            <a class="btn btn-cancel" data-dismiss="modal" href="#">Hủy bỏ</a>
                            <BR>
                        </div>
                    </form>


                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>


        <script>
            function view_size(id) {
                const boxes = document.querySelectorAll('.form_size');

                for (var i = 0; i < boxes.length; i++) {
                    boxes[i].classList.add('d-none');
                }

                const box = document.querySelector('#' + id);
                box.classList.remove('d-none');

            }
        </script>

        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/main.js"></script>
        <script src="js/plugins/pace.min.js"></script>
        <script>
            const inpFile = document.getElementById("inpFile");
            const loadFile = document.getElementById("loadFile");
            const previewContainer = document.getElementById("imagePreview");
            const previewContainer = document.getElementById("imagePreview");
            const previewImage = previewContainer.querySelector(".image-preview__image");
            const previewDefaultText = previewContainer.querySelector(".image-preview__default-text");
            inpFile.addEventListener("change", function () {
                const file = this.files[0];
                if (file) {
                    const reader = new FileReader();
                    previewDefaultText.style.display = "none";
                    previewImage.style.display = "block";
                    reader.addEventListener("load", function () {
                        previewImage.setAttribute("src", this.result);
                    });
                    reader.readAsDataURL(file);
                }
            });

        </script>
    </body>

</html>
