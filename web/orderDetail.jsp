<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean class="utils.Helpers" id="helpers"/>
<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Danh sách Orders | Quản trị Admin</title>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <!-- or -->
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">

        <!-- Font-icon css-->
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">


    </head>


    <body onload="time()" class="app sidebar-mini rtl">
        <!-- Navbar-->
        <header class="app-header">
            <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar"
                                            aria-label="Hide Sidebar"></a>
            <!-- Navbar Right Menu-->
            <ul class="app-nav">


                <!-- User Menu-->
                <li><a class="app-nav__item" href="home"><i class='bx bx-log-out bx-rotate-180'></i> </a>

                </li>
            </ul>
        </header>
        <c:if test="${not empty message}">
            <c:if test="${alert.equals('Success')}">
                <div id="myAlert" style="right: 50%; z-index: 999" class="alert alert-success alert-dismissible fade show text-center position-absolute " role="alert">
                    ${message}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </c:if>
            <c:if test="${alert.equals('Error')}">
                <div id="myAlert" style="right: 50%; z-index: 999" class="alert alert-danger alert-dismissible fade show text-center position-absolute " role="alert">
                    ${message}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </c:if>
            <c:if test="${alert.equals('Warning')}">
                <div id="myAlert" style="right: 50%; z-index: 999" class="alert alert-warning alert-dismissible fade show text-center position-absolute " role="alert">
                    ${message}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </c:if>
        </c:if>
        <!-- Sidebar menu-->
        <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
        <jsp:include page="navbar.jsp" flush="true" >
            <jsp:param name="orders" value="active" />
        </jsp:include>
        <main class="app-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="app-title">
                        <ul class="app-breadcrumb breadcrumb">
                            <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/manager-orders"><b>Danh sách Orders</b></a></li>
                            <li class="breadcrumb-item"><a href="#"><b>Chi tiết đơn  </b></a></li>
                        </ul>
                        <div id="clock"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="container mt-5">
                    <h1 class="mb-4">Cập nhật thông tin trạng thái đơn hàng</h1>

                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Đơn Hàng #${order.orderCode}</h5>
                            <p class="card-text">Ngày Đặt Hàng: ${order.createDate}</p>
                            <p class="card-text">Ngày giao hàng dự kiến: ${order.deliveryDate}</p>
                            <p class="card-text">Trạng thái đơn hàng: 
                                <c:if test="${order.statusModel != null}">
                                    <c:if test="${order.statusModel.stName == 'Delivery_Success'}">
                                        <span class="badge bg-success">${order.statusModel.stNameVi}</span>
                                    </c:if>
                                    <c:if test="${order.statusModel.stName == 'Order_Cancel'}">
                                        <span class="badge bg-danger">${order.statusModel.stNameVi}</span>
                                    </c:if>
                                    <c:if test="${order.statusModel.stName != 'Order_Cancel' && order.statusModel.stName != 'Delivery_Success'}">
                                        <span class="badge bg-primary">${order.statusModel.stNameVi}</span>
                                    </c:if>
                                </c:if>
                                <c:if test="${order.statusModel == null}">
                                <td><span >--</span></td>
                            </c:if>
                            </p>
                            <p class="card-text">Hình thức thanh toán: Thanh toán khi nhận hàng</p>
                            <hr>
                            <h6>Thông Tin Sản Phẩm:</h6>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Sản Phẩm</th>
                                        <th scope="col">Đơn giá</th>
                                        <th scope="col">Số Lượng(kg)</th>
                                        <th scope="col">Thành tiền</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${orderDetails}" var="item">
                                        <tr>
                                            <td>${item.product.pName}</td>
                                            <td>${helpers.formatInt(item.product.price)} VNĐ</td>
                                            <td>${item.quantity}</td>
                                            <td>${helpers.formatInt(item.quantity * item.product.price)} VNĐ</td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                            <hr>
                            <h6>Thông Tin Khách Hàng:</h6>
                            <p class="card-text"><strong>Tên người nhận:</strong> ${order.name}</p>
                            <p class="card-text"><strong>Số điện thoại:</strong> ${order.phone}</p>
                            <p class="card-text"><strong>Địa Chỉ nhận hàng:</strong> ${order.address.address}, ${order.address.commune}, ${order.address.district}, ${order.address.city}</p>
                            <hr>
                            <h6>Tổng Tiền: ${helpers.formatInt(order.totalPrice)} VNĐ</h6>
                            <c:if test="${action != null}">
                                <form action="${pageContext.request.contextPath}/manager-orders/update-status" method="POST">
                                    <div class="row align-items-end">
                                        <input hidden="true" value="${order.orderCode}" name="orderCode"/>
                                        <div class="form-group  col-md-5">
                                            <label for="exampleSelect1" class="control-label">Trạng thái</label>
                                            <select class="form-control" id="exampleSelect1" name="statusId">
                                                <c:if test="${item.status != null}}">
                                                    <option value="${item.status}">${item.statusModel.stNameVi}</option>
                                                </c:if>
                                                <c:if test="${item.status != null}}">
                                                    <option value="">Chọn trạng thái</option>
                                                </c:if>
                                                <c:forEach items="${statuses}" var="status">
                                                    <option value = ${status.stid}>${status.stNameVi}</option>
                                                </c:forEach>

                                            </select>
                                        </div>

                                        <div class="form-group  col-md-5">
                                            <label for="deliveryDate" class="control-label">Ngày giao hàng dự kiến</label>
                                            <input id="deliveryDate" name="deliveryDate" class="form-control" type="date" required="true"/>
                                        </div>

                                        <div class="form-group  col-md-2">
                                            <button class="btn " style="background-color: #b1ffb5" type="submit">Lưu lại</button>
                                             <a class="btn btn-cancel" data-dismiss="modal" href="manager-orders">Hủy bỏ</a>
                                        </div>
                                    </div>
                                </form>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>
    <!--
    MODAL
    -->

    <!-- Essential javascripts for application to work-->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="src/jquery.table2excel.js"></script>
    <script src="js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="js/plugins/pace.min.js"></script>
    <!-- Page specific javascripts-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <!-- Data table plugin-->
    <script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>
    <!--     Js Plugins 
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/jquery.slicknav.js"></script>
        <script src="js/mixitup.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>-->

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"></script>
    <script src="js/app.js"></script>
    <script type="text/javascript">
        $('#sampleTable').DataTable();
        //Thời Gian
        function time() {
            var today = new Date();
            var weekday = new Array(7);
            weekday[0] = "Chủ Nhật";
            weekday[1] = "Thứ Hai";
            weekday[2] = "Thứ Ba";
            weekday[3] = "Thứ Tư";
            weekday[4] = "Thứ Năm";
            weekday[5] = "Thứ Sáu";
            weekday[6] = "Thứ Bảy";
            var day = weekday[today.getDay()];
            var dd = today.getDate();
            var mm = today.getMonth() + 1;
            var yyyy = today.getFullYear();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            m = checkTime(m);
            s = checkTime(s);
            nowTime = h + " giờ " + m + " phút " + s + " giây";
            if (dd < 10) {
                dd = '0' + dd
            }
            if (mm < 10) {
                mm = '0' + mm
            }
            today = day + ', ' + dd + '/' + mm + '/' + yyyy;
            tmp = '<span class="date"> ' + today + ' - ' + nowTime +
                    '</span>';
            document.getElementById("clock").innerHTML = tmp;
            clocktime = setTimeout("time()", "1000", "Javascript");

            function checkTime(i) {
                if (i < 10) {
                    i = "0" + i;
                }
                return i;
            }
        }
    </script>
    <script>
        function deleteRow(r) {
            var i = r.parentNode.parentNode.rowIndex;
            console.log(i);
            document.getElementById("myTable").deleteRow(i);
        }
        jQuery(function () {
            jQuery(".trash").click(function (e) {
                swal({
                    title: "Cảnh báo",
                    text: "Bạn có chắc chắn là muốn xóa sản phẩm này?",
                    buttons: ["Hủy bỏ", "Đồng ý"],
                })
                        .then((willDelete) => {
                            console.log(willDelete);
                            if (willDelete) {
                                swal("Đã xóa thành công.!", {

                                });
                            }
                        });
            });
        });
        oTable = $('#sampleTable').dataTable();
        $('#all').click(function (e) {
            $('#sampleTable tbody :checkbox').prop('checked', $(this).is(':checked'));
            e.stopImmediatePropagation();
        });
        function showModalUpdateStatus(orderCode) {
            console.log(orderCode)
            $("#ModalUP-" + orderCode).modal({backdrop: false, keyboard: false});
        }
    </script>
    <script>
        // Sử dụng JavaScript hoặc jQuery để tự động đóng alert sau một khoảng thời gian
        setTimeout(function () {
            $("#myAlert").alert('close'); // Sử dụng jQuery
            // Hoặc sử dụng JavaScript thuần
            // document.getElementById('myAlert').style.display = 'none';
        }, 3000); // 3000 milliseconds tương ứng với 3 giây
    </script>

</html>s