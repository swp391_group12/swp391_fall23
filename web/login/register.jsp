<%-- 
    Document   : Register
    Created on : Oct 2, 2023, 8:16:11 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<link rel="stylesheet" type="text/css" href="css/login.css">
<div class="container right-panel-active" id="container">
	<div class="form-container sign-in-container">
            <form name="registerForm" action="register" method="post" >
			<h1>Tạo tài khoản</h1>
                        <input type="text" name="name" placeholder="Họ Tên*" required="" value="${name}" />
			<input type="email" name ="email" placeholder="Email*" required="" value="${email}" />
			<input type="password" name="pass" placeholder="Mật khẩu*" minlength="6" maxlength="24" pattern=".{6,24}" title="Mật khẩu phải từ 6 đến 24 kí tự" required="" value="${pass}"/>
                        <input type="text" name="phone" placeholder="Số Điện Thoại*" pattern="0[0-9]{9}" title="Số điện thoại bắt đầu bằng 0 và có 10 số" required="" value="${phone}"/>
                        <button type="submit">Đăng ký</button> 
                        </form>
            <div class="message" id="message">${msg}</div>
	</div>
                
	<div class="overlay-container">
		<div class="overlay">
			<div class="overlay-panel overlay-left">
				<h1>Xin chào</h1>
				<p>Bạn đã có tài khoản? Đăng nhập tại đây!</p>
				<button class="ghost" id="signIn">Đăng nhập</button>
			</div>
		</div>
	</div>
</div>
        <div class="back-button">
        <a href="home" onclick="goToHomePage()">
        <span>&larr;</span> Trở về trang chủ
        </a>
</div>
</html>

<style>
  .back-button {
  margin: 20px;
}

.back-button a {
  text-decoration: none;
  font-size: 16px;
}

.back-button a span {
  margin-right: 7px;
}
</style>

<script>
    const signInButton = document.getElementById('signIn');

    signInButton.addEventListener('click', () => {
        // Chuyển hướng đến servlet khi nhấn nút "Sign In"
        window.location.href = 'login';
    });
    
    
</script>


