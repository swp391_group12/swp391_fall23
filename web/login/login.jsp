<%-- 
    Document   : Register
    Created on : Oct 2, 2023, 8:16:11 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">

<link rel="stylesheet" type="text/css" href="css/login.css">
<div class="container" id="container">
	<div class="form-container sign-in-container">
            <form action="login" method="post">
			<h1>Đăng nhập</h1>
                        <input type="hidden" name="redirectPage" value="${page}" />
                        <input type="email" name="email" placeholder="Email*" value="${email}" required/>
			<input type="password" name="pass" placeholder="Mật khẩu*" required/>
			<a href="forgot">Quên mật khẩu?</a>
                        <button type="submit">Đăng nhập</button>
		</form>
            <div class="message" id="message">${msg}</div>
	</div>
	<div class="overlay-container">
		<div class="overlay">
			<div class="overlay-panel overlay-right">
				<h1>Chào bạn!</h1>
				<p>Bạn chưa có tài khoản? Đăng ký ngay tại đây!</p>
				<button class="ghost" id="signUp">Đăng ký</button>
			</div>
		</div>
	</div>
</div>
        </div>
        <div class="back-button">
        <a href="home" onclick="goToHomePage()">
        <span>&larr;</span> Trở về trang chủ
        </a>
</div>
</html>
<style>
  .back-button {
  margin: 20px;
}

.back-button a {
  text-decoration: none;
  font-size: 16px;
}

.back-button a span {
  margin-right: 7px;
}
</style>

<script>
    const signUpButton = document.getElementById('signUp');

    signUpButton.addEventListener('click', () => {
        // Chuyển hướng đến servlet khi nhấn nút "Sign Up"
        window.location.href = 'register';
    });
</script>


