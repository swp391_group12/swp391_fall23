<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Ogani | Template</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300&display=swap" rel="stylesheet">
        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">

        <style>
            .blog .carousel-indicators {
                left: 0;
                top: auto;
                bottom: -40px;

            }

            /* The colour of the indicators */
            .blog .carousel-indicators li {
                background: #a3a3a3;
                border: 4px;
                padding: 2px;
            }

            .blog .carousel-indicators .active {
                background: #707070;
            }
            
  .thongbao{
    margin-bottom: 40px;
    margin-top: 10px;
    font-family: "Quicksand", sans-serif;
    font-weight: 900;
    margin-left: 40px;
    font-size: 14px;
}      
  
  .user-info {
    max-width: 400px;
    margin: 0 0 0 50px;
  }
  
  .info-item {
    display: flex;
    flex-direction: row;
    align-items: center;
    margin-bottom: 10px;
    margin-top: 20px;
  }
  
  label {
    flex: 1;
    font-weight: bold;
    margin-right: 10px;
    font-size: 13px;
    font-family: "Quicksand", sans-serif;
  }
  
 .input-info {
    flex: 2;
    padding: 8px;
    box-sizing: border-box;
    border: 1px solid #ededed;
    border-radius: 5px;
    font-size: 14px;
    font-weight: 800;
    width: 100%; /* Ensure the input and textarea take full width */
    
    background: #ededed
  }
  
  textarea {
    height: 100px; /* Set a specific height for the textarea */
  }
  
  input:focus, textarea:focus {
    border-color: #00a205;
    outline: none;
  }
  h1{
    position: relative;
    text-align: center;
    font-size: 25px;
    margin-top: 30px;
    font-weight: 800;
    line-height: initial;
    font-family: "Quicksand", sans-serif;

  }

  h1::after {
    content: "";
    background: #000;
    display: block;
    width: 60px;
    height: 4px;
    margin: 20px auto;
    
  }

  h2{
    font-size: 15px;
    margin-top: 50px;
    margin-left: 50px;
    border-bottom: 1px solid #ccc;
    margin-bottom: 20px;
    font-weight: 900;
    padding-bottom: 10px;
    letter-spacing: 1px;
    font-family: "Quicksand", sans-serif;
    
  }

  h3{
    font-size: 16px;
    font-weight: bold;
    margin-top: 52px;
    letter-spacing: 1px;  
    font-family: "Quicksand", sans-serif;
  }

  h4{
    font-size: 14px;
    font-weight: bold;
    margin: 0px;
    font-family: "Quicksand", sans-serif;
  }

  h5{
    text-align: center;
    font-size: 14px;
    font-weight: 900;
    background: #bbffc8;
    padding: 10px;
    margin:0px;
    text-transform: uppercase;
    font-family: "Quicksand", sans-serif;
  }

  .links a {
    color: black; /* Màu chữ ban đầu */
    text-decoration: none; /* Loại bỏ gạch chân mặc định cho liên kết */
    font-size: 13px;
    font-weight: bold;
    font-family: "Quicksand", sans-serif;
  }
  
  .links a:hover {
    color: green; /* Màu chữ khi di chuột qua */
  }
  .custom-button {
      font-family: "Quicksand", sans-serif;
    margin-top: 20px;
    font-weight: 900;
    padding: 5px 14px; /* Điều chỉnh padding để nút nhỏ hơn */
    font-size: 14px;
    background-color: white; /* Màu nền trắng */
    color: #00b916; /* Màu chữ xanh */
    border: 2px solid #00b916; /* Viền bo màu xanh */
    border-radius: 5px;
    cursor: pointer;
  }
  
  .custom-button:hover {
    background-color: #00b916; 
    color: white; 
  }
  .custom-button-pass{
      font-family: "Quicksand", sans-serif;
      margin-top: 20px;
    font-weight: 900;
    padding: 5px 14px; 
    font-size: 14px;
    background-color: white; 
    color: #FF0000; 
    border: 2px solid #FF0000; 
    border-radius: 5px;
    cursor: pointer;
  }
  
.custom-button-pass:hover {
    background-color: #FF0000;
    color: white; 

  .left{
    width: 35%;
  }
  .right{
    width: 65%;
  }

  .large_view{
    display: flex;
  }
  b{
    font-size: 14px;
  }

  p{
    font-size: 13px;
    font-weight: bold;
  }

  .title-name{
    background-color: #bbffc8;
    border-color: #bbffc8;
    padding: 10px 65px 10px 10px;
    position: relative;

  }
  
  

  .address-user{
    background-color: #fbfbfb;
    border-color: #bbffc8;
    padding: 10px 65px 10px 10px;
    position: relative;
  }
  

  .table{
    margin-top: 50px;
  }

  .clearfix{
    margin-bottom: 15px;
  }

  .input-table{
    background-color: #fbfbfb;
    padding:15px;
  }
 
  .input-address{
    margin-bottom: 15px;
    border: 1px solid #bbffc8;
  }

  #address-heading {
    cursor: pointer;
}
#inputTable {
  display: none;
}

.info-item {
  display: flex;
  align-items: center;
  margin-bottom: 10px;
}

.info-item {
  display: flex;
  align-items: center;
  margin-bottom: 10px;
}



        </style>
    </head>

    <body>

       <!-- Header Section Begin -->
        <jsp:include page="../header.jsp" flush="true" >
            <jsp:param name="home" value="active" />
        </jsp:include>
        <!-- Header Section End -->

        <!-- Hero Section Begin -->
        <section class="hero hero-normal">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="hero__categories">
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- Hero Section End -->

        <!-- Breadcrumb Section Begin -->
        
        <div class="container">
            <h1 style='font-family: "Quicksand", sans-serif'>Tài khoản của bạn</h1>
    <div class="row">
        <div class="col-lg-3 col-md-12 col-12">
            <h3 class="AccountTitle titleSidebar">Tài khoản</h3>
            <div class="links">
                <li><a href="profile">Thông tin tài khoản</a></li>
                <li><a href="useraddress">Danh sách địa chỉ</a></li>
                <li><a href="logout">Đăng xuất</a></li>
            </div>
          </div>
      <div class="col-lg-9 col-md-12 col-12">
        <h2>Đổi Mật Khẩu</h2>
        <form action="changepass" method="post">
        <div class="user-info">
          
          <div class="info-item">
            <label for="email">Mật khẩu cũ*</label>
            <input class="input-info" type="password" name="oldpass"  minlength="6" maxlength="24" pattern=".{6,24}" title="Mật khẩu phải từ 6 đến 24 kí tự" required >
          </div>
          <div class="info-item">
            <label for="name">Mật khẩu mới*</label>
            <input class="input-info" type="password" name="newpass"  minlength="6" maxlength="24" pattern=".{6,24}" title="Mật khẩu phải từ 6 đến 24 kí tự" required>
          </div>
          <div class="info-item">
            <label for="phone">Nhập lại mật khẩu*</label>
            <input class="input-info" type="password" name="newpass1" minlength="6" maxlength="24" pattern=".{6,24}" title="Mật khẩu phải từ 6 đến 24 kí tự" required>
          </div>
          <div class="button-change">
              <button type="submit" class="custom-button">Cập nhật</button>
            <button class="custom-button-pass" onclick="callServlet()">Quay về</button>
          </div>
        </div>
        </form>
        <div class="thongbao">${msg}</div>
      </div>
    </div>
  </div>
        <!-- Footer Section Begin -->
        <footer class="footer spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="footer__about">
                            <div class="footer__about__logo">
                                <a href="./index.html"><img src="assets/img/logo.png" alt=""></a>
                            </div>
                            <ul>
                                <li>Address: 60-49 Road 11378 New York</li>
                                <li>Phone: +65 11.188.888</li>
                                <li>Email: hello@colorlib.com</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 offset-lg-1">
                        <div class="footer__widget">
                            <h6>Useful Links</h6>
                            <ul>
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">About Our Shop</a></li>
                                <li><a href="#">Secure Shopping</a></li>
                                <li><a href="#">Delivery infomation</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Our Sitemap</a></li>
                            </ul>
                            <ul>
                                <li><a href="#">Who We Are</a></li>
                                <li><a href="#">Our Services</a></li>
                                <li><a href="#">Projects</a></li>
                                <li><a href="#">Contact</a></li>
                                <li><a href="#">Innovation</a></li>
                                <li><a href="#">Testimonials</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="footer__widget">
                            <h6>Join Our Newsletter Now</h6>
                            <p>Get E-mail updates about our latest shop and special offers.</p>
                            <form action="#">
                                <input type="text" placeholder="Enter your mail">
                                <button type="submit" class="site-btn">Subscribe</button>
                            </form>
                            <div class="footer__widget__social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-pinterest"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer__copyright">
                            <div class="footer__copyright__text"><p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p></div>
                            <div class="footer__copyright__payment"><img src="assets/img/payment-item.png" alt=""></div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer Section End -->

      

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>

<script>
        function callServlet() {
            // Gọi đến servlet với đường dẫn changepass
            window.location.href = 'profile';
        }
    </script>

