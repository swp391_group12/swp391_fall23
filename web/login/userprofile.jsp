<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Ogani | Template</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300&display=swap" rel="stylesheet">
        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">

        <style>
            .blog .carousel-indicators {
                left: 0;
                top: auto;
                bottom: -40px;

            }

            /* The colour of the indicators */
            .blog .carousel-indicators li {
                background: #a3a3a3;
                border: 4px;
                padding: 2px;
            }

            .blog .carousel-indicators .active {
                background: #707070;
            }
            
        
  
  .user-info {
    max-width: 400px;
    margin: 0 0 0 50px;
  }
  
  .info-item {
    display: flex;
    flex-direction: row;
    align-items: center;
    margin-bottom: 10px;
    margin-top: 20px;
  }
  
  label {
    flex: 1;
    font-weight: bold;
    margin-right: 10px;
    font-size: 13px;
    font-family: "Quicksand", sans-serif;
  }
  
 .input-info {
    flex: 2;
    padding: 8px;
    box-sizing: border-box;
    border: 1px solid #ededed;
    border-radius: 5px;
    font-size: 14px;
    font-weight: 800;
    width: 100%; /* Ensure the input and textarea take full width */
    
    background: #ededed
  }
  
  textarea {
    height: 100px; /* Set a specific height for the textarea */
  }
  
  input:focus, textarea:focus {
    border-color: #00a205;
    outline: none;
  }
  h1{
    position: relative;
    text-align: center;
    font-size: 25px;
    margin-top: 30px;
    font-weight: 800;
    line-height: initial;
    font-family: "Quicksand", sans-serif;

  }

  h1::after {
    content: "";
    background: #000;
    display: block;
    width: 60px;
    height: 4px;
    margin: 20px auto;
    
  }

  h2{
    font-size: 15px;
    margin-top: 50px;
    margin-left: 50px;
    border-bottom: 1px solid #ccc;
    margin-bottom: 20px;
    font-weight: 900;
    padding-bottom: 10px;
    letter-spacing: 1px;
    font-family: "Quicksand", sans-serif;
    
  }

  h3{
    font-size: 16px;
    font-weight: bold;
    margin-top: 52px;
    letter-spacing: 1px;  
    font-family: "Quicksand", sans-serif;
  }

  h4{
    font-size: 14px;
    font-weight: bold;
    margin: 0px;
    font-family: "Quicksand", sans-serif;
  }

  h5{
    text-align: center;
    font-size: 14px;
    font-weight: 900;
    background: #bbffc8;
    padding: 10px;
    margin:0px;
    text-transform: uppercase;
    font-family: "Quicksand", sans-serif;
  }

  .links a {
    color: black; /* Màu chữ ban đầu */
    text-decoration: none; /* Loại bỏ gạch chân mặc định cho liên kết */
    font-size: 13px;
    font-weight: bold;
    font-family: "Quicksand", sans-serif;
  }
  
  .links a:hover {
    color: green; /* Màu chữ khi di chuột qua */
  }
  .custom-button {
      font-family: "Quicksand", sans-serif;
    margin-top: 20px;
    font-weight: 900;
    padding: 5px 14px; /* Điều chỉnh padding để nút nhỏ hơn */
    font-size: 14px;
    background-color: white; /* Màu nền trắng */
    color: #00b916; /* Màu chữ xanh */
    border: 2px solid #00b916; /* Viền bo màu xanh */
    border-radius: 5px;
    cursor: pointer;
    margin-bottom: 50px;
  }
  
  .custom-button:hover {
    background-color: #00b916; 
    color: white; 
  }
  .custom-button-pass{
      font-family: "Quicksand", sans-serif;
      margin-top: 20px;
    font-weight: 900;
    padding: 5px 14px; 
    font-size: 14px;
    background-color: white; 
    color: #FF0000; 
    border: 2px solid #FF0000; 
    border-radius: 5px;
    cursor: pointer;
    margin-bottom: 50px;
  }
  
.custom-button-pass:hover {
    background-color: #FF0000;
    color: white; 

  .left{
    width: 35%;
  }
  .right{
    width: 65%;
  }

  .large_view{
    display: flex;
  }
  b{
    font-size: 14px;
  }

  p{
    font-size: 13px;
    font-weight: bold;
  }

  .title-name{
    background-color: #bbffc8;
    border-color: #bbffc8;
    padding: 10px 65px 10px 10px;
    position: relative;

  }

  .address-user{
    background-color: #fbfbfb;
    border-color: #bbffc8;
    padding: 10px 65px 10px 10px;
    position: relative;
  }

  .table{
    margin-top: 50px;
  }

  .clearfix{
    margin-bottom: 15px;
  }

  .input-table{
    background-color: #fbfbfb;
    padding:15px;
  }
 
  .input-address{
    margin-bottom: 15px;
    border: 1px solid #bbffc8;
  }

  #address-heading {
    cursor: pointer;
}
#inputTable {
  display: none;
}

.info-item {
  display: flex;
  align-items: center;
  margin-bottom: 10px;
}

.info-item {
  display: flex;
  align-items: center;
  margin-bottom: 10px;
}




        </style>
    </head>

    <body>

        <!-- Header Section Begin -->
        <jsp:include page="../header.jsp" flush="true" >
            <jsp:param name="home" value="active" />
        </jsp:include>
        <!-- Header Section End -->

        <!-- Hero Section Begin -->
        <section class="hero hero-normal">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="hero__categories">
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- Hero Section End -->

        <!-- Breadcrumb Section Begin -->
        
        <div class="container">
            <h1 style='font-family: "Quicksand", sans-serif'>Tài khoản của bạn</h1>
    <div class="row">
        <div class="col-lg-3 col-md-12 col-12">
            <h3 class="AccountTitle titleSidebar">Tài khoản</h3>
            <div class="links">
                <li><a href="profile">Thông tin tài khoản</a></li>
                <li><a href="useraddress">Danh sách địa chỉ</a></li>
                <li><a href="logout">Đăng xuất</a></li>
            </div>
          </div>
      <div class="col-lg-9 col-md-12 col-12">
        <h2>THÔNG TIN TÀI KHOẢN</h2>
        <form action="profile" method="post">
        <div class="user-info">
            <div class="info-item">
            <input class="input-info" type="hidden" name="id" value="${sessionScope.account.userId}">
            </div>
          <div class="info-item">
            <label for="email">Email</label>
            <input class="input-info" type="email" id="email" value="${sessionScope.account.email}" readonly="">
          </div>
          <div class="info-item">
            <label for="name">Tên</label>
            <input class="input-info" type="text" id="name" name="name" value="${sessionScope.account.userName}" required="">
          </div>
          <div class="info-item">
            <label for="phone">Số Điện thoại</label>
            <input class="input-info" type="text" id="phone" name="phone" value="${sessionScope.account.phone}" pattern="0[0-9]{9}" title="Số điện thoại bắt đầu bằng 0 và có 10 số" required>
          </div>
          <div>
            <button type="submit" class="custom-button">Cập nhật</button>
            <button type ="button" class="custom-button-pass" onclick="callServlet()" >Đổi Mật Khẩu</button>
          </div>
          </div>
        </form>
      </div>
      
    </div>
  </div>
        <!-- Footer Section Begin -->
        <footer class="footer spad">
            <div class="container">
                 <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="footer__about">
                            <div class="footer__about__logo">
                                <a href="home"><img src="img/logo.png" alt=""></a>
                            </div>
                            <ul>
                                <li>Của hàng HCM: 66 Nguyễn Trí Sách, Phường 15 Tân Bình</li>
                                <li>Cửa hàng HN: 63 Yên Lãng - Đống Đa - Hà Nội</li>
                                <li>Phone: 0123456789</li>
                                <li>Email: shophoaqua@gmail.com</li>
                            </ul>
                        </div>
                    </div>
                    
                    
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer__copyright">
                            <div class="footer__copyright__text"><p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p></div>
                            <div class="footer__copyright__payment"><img src="assets/img/payment-item.png" alt=""></div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer Section End -->

        <!-- Js Plugins -->
<!--        <script src="assets/js/jquery-3.3.1.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.nice-select.min.js"></script>
        <script src="assets/js/jquery-ui.min.js"></script>
        <script src="assets/js/jquery.slicknav.js"></script>
        <script src="assets/js/mixitup.min.js"></script>
        <script src="assets/js/owl.carousel.min.js"></script>
        <script src="assets/js/main.js"></script>   -->

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>

<script>
        function callServlet() {
            // Gọi đến servlet với đường dẫn changepass
            window.location.href = 'changepass';
        }
    </script>

