<%-- 
    Document   : cart
    Created on : Oct 2, 2023, 11:16:46 PM
    Author     : Trung Kien
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean class="utils.Helpers" id="helpers"/>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Ogani | Template</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <style>
            /* Hide the default number input buttons */
            input[type=number]::-webkit-inner-spin-button,
            input[type=number]::-webkit-outer-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }
            .v3_select{
                pointer-events: none;
            }

            input[type=number] {
                /* Firefox */
                -moz-appearance: textfield;
            }

            /* Hide the increment arrows in IE */
            input[type=number] {
                -ms-inner-appearance: none;
            }
        </style>
    </head>

    <body>
        <!-- Humberger End -->
        <c:if test="${not empty param.message}">
            <c:if test="${param.alert.equals('success')}">
                <div id="myAlert" style="right: 50%; z-index: 999" class="alert alert-success alert-dismissible fade show text-center position-absolute " role="alert">
                    ${param.message}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </c:if>
            <c:if test="${param.alert.equals('danger')}">
                <div id="myAlert" style="right: 50%; z-index: 999" class="alert alert-danger alert-dismissible fade show text-center position-absolute " role="alert">
                    ${param.message}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </c:if>
            <c:if test="${param.alert.equals('Warning')}">
                <div id="myAlert" style="right: 50%; z-index: 999" class="alert alert-warning alert-dismissible fade show text-center position-absolute " role="alert">
                    ${param.message}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </c:if>
        </c:if>

        <jsp:include page="header.jsp" flush="true" >
            <jsp:param name="shop" value="active" />
        </jsp:include>
        <!-- Header Section End -->

        <!-- Hero Section Begin -->

        <!-- Hero Section End -->

        <!-- Breadcrumb Section Begin -->
        <section class="breadcrumb-section set-bg" style="background-image: url('img/breadcrumb.jpg')">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="breadcrumb__text">
                            <h2>Giỏ hàng</h2>
                            <div class="breadcrumb__option">
                                <a href="./home">Trang chủ</a>
                                <span>Giỏ hàng</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Breadcrumb Section End -->

        <!-- Shoping Cart Section Begin -->
        <section class="shoping-cart spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="shoping__cart__table">
                            <table>
                                <thead>
                                    <tr>
                                        <th class="shoping__product">Sản phẩm</th>
                                        <th>Giá tiền/Kg</th>
                                        <th>Số lượng</th>
                                        <th>Tổng tiền</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test="${!productDisplayCartDTOList.isEmpty()}">
                                        <c:forEach var="product" items="${productDisplayCartDTOList}">
                                        <form id="formEditCart" action="<c:url value="/AddorCheckRedirectController?action=remove" />" method="POST">
                                            <input type="hidden" value="${product.getPid()}" name="productId"/>
                                            <input type="hidden" value="${product.getSizeId()}" name="sizeId"/>

                                            <tr>
                                                <td class="shoping__cart__item">
                                                    <img src="img/product/${product.getImageUrl()}" style="height: 60px; width: 60px" alt="">
                                                    <h5>${product.getPName()}</h5>
                                                </td>
                                                <td class="shoping__cart__price">
                                                    ${helpers.formatInt(product.getPrice())} VNÐ
                                                </td>
                                                <td class="shoping__cart__quantity">
                                                    <div class="quantity">
                                                        <div class="pro-qty">
                                                            <span class="dec qtybtn" onclick="decreaseQuantity(${product.getPid()}, ${product.getSizeId()}, '${pageContext.request.contextPath}')" >-</span>
                                                            <input type="number" value="${product.getQuantity()}" class="quantity-input" name="quantity" onkeyup="onEnterInput(event, ${product.getPid()}, '${pageContext.request.contextPath}')"> Kg
                                                            <span class="inc qtybtn" onclick="increaseQuantity(${product.getPid()}, ${product.getSizeId()}, '${pageContext.request.contextPath}')">+</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="shoping__cart__total">
                                                    ${helpers.formatInt(product.getTotal())} VNÐ
                                                </td>
                                                <td class="shoping__cart__item__close">
                                                    <button type="submit" style="border: none">
                                                        <span class="icon_close"></span>
                                                    </button>
                                                </td>
                                            </tr>
                                        </form>
                                    </c:forEach>
                                </c:if>
                                <c:if test="${productDisplayCartDTOList == null || productDisplayCartDTOList.isEmpty()}">
                                    <tr>
                                        <td colspan="4"><h3>Không còn mặt hàng nào trong giỏ hàng của bạn</h3></td>
                                    </tr>
                                </c:if>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-lg-6">
                        <div class="shoping__cart__btns">
                            <a href="${pageContext.request.contextPath}/shop" class="primary-btn cart-btn">Tiếp tục mua hàng</a>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="shoping__checkout" style="margin-top: 0">
                            <h5>Tổng số sản phẩm trong giỏ: <c:if test="${cookie.numOfProducts.value != null && cookie.numOfProducts.value != 0}">
                                    <c:set var="numProduct" value="${cookie.numOfProducts.value}" />
                                    <span>${numProduct}</span>
                                </c:if> sản phẩm</h5>
                            <ul>
                                <!--<li>Subtotal <span>$454.98</span></li>-->
                                <li>Tổng thanh toán  <span>${helpers.formatInt(requestScope.total)} VNÐ</span></li>
                            </ul>
                            <c:if test="${productDisplayCartDTOList != null && !productDisplayCartDTOList.isEmpty()}">
                                <a href="<c:url value="/checkout-cart"/>" style="cursor: pointer"  class="primary-btn w-100">MUA HÀNG</a>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Shoping Cart Section End -->

        <!-- Footer Section Begin -->
        <footer class="footer spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="footer__about">
                            <div class="footer__about__logo">
                                <a href="home"><img src="img/logo.png" alt=""></a>
                            </div>
                            <ul>
                                <li>Của hàng HCM: 66 Nguyễn Trí Sách, Phường 15 Tân Bình</li>
                                <li>Cửa hàng HN: 63 Yên Lãng - Đống Đa - Hà Nội</li>
                                <li>Phone: 0123456789</li>
                                <li>Email: shophoaqua@gmail.com</li>
                            </ul>
                        </div>
                    </div>


                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer__copyright">
                            <div class="footer__copyright__text"><p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p></div>
                            <div class="footer__copyright__payment"><img src="img/payment-item.png" alt=""></div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer Section End -->

        <!-- Js Plugins -->
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/jquery.slicknav.js"></script>
        <script src="js/mixitup.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/main.js"></script>
        <script>
//                                        var quantityButtons = document.querySelectorAll('.shoping__cart__quantity .qtybtn');
//                                        quantityButtons.forEach(function (button) {
//                                            button.addEventListener('click', function () {
//                                                // Lấy input số lượng tương ứng với nút được nhấn
//                                                var inputElement = button.parentElement.querySelector('.quantity-input');
//                                                var currentValue = parseInt(inputElement.value);
//                                                console.log(currentValue);
//                                                // Nếu nút '-' được nhấn, giảm giá trị
//                                                if (button.classList.contains('dec')) {
//                                                    if (currentValue > 1) {
//                                                        inputElement.value = currentValue - 1;
//                                                    }
//                                                } else {
//                                                    // Nếu nút '+' được nhấn, tăng giá trị
//                                                    inputElement.value = currentValue + 1;
//                                                }
//                                            });
//                                        });

                                        function decreaseQuantity(productId, sizeId, context) {
                                            var inputElement = document.querySelector('.quantity-input');
                                            var currentValue = parseFloat(inputElement.value);
                                            if (currentValue > 0.1) {
                                                let href = context + '/AddorCheckRedirectController/descrease?productId=' + productId + '&sizeId=' + sizeId + '&path=cart';
                                                window.location.href = href;
                                            } else {
                                                alert("We apologize for this inconvenience." +
                                                        "\nThe item is currently out of stock, please comeback later");
                                            }
                                        }

                                        function increaseQuantity(productId, sizeId, context) {
                                            let href = context + '/AddorCheckRedirectController/increase?productId=' + productId + '&sizeId=' + sizeId + '&path=cart';
                                            window.location.href = href;
                                        }

                                        function onEnterInput(e, productId, context) {
                                            console.log(e.target.value);
                                            if (e.key === 'Enter' || e.keyCode === 13) {
                                                // Do something
                                                e.preventDefault();
                                                let val = e.target.value;
                                                console.log(val);
                                                let href = context + '/AddorCheckRedirectController?quantity=' + val + '&productId=' + productId + '&path=cart&action=replace';
                                                window.location.href = href;
                                            }
                                        }


//                                        var quantityInputs = document.querySelectorAll('.shoping__cart__quantity .quantity-input');
//                                        quantityInputs.forEach(function (input) {
//                                            input.addEventListener('keyup', function (e) {
//                                                if (e.key === 'Enter' || e.keyCode === 13) {
//                                                    // Do something
//                                                    let val = e.target.value;
//                                                    console.log(val);
//                                                    let href = '/SWP391_FA23_SE1736_TEAM1/AddorCheckRedirectController?quantity=' + val + '&productId=' + productId + '&sizeId=' + sizeId + '&path=cart';
////                                                    window.location.href = href;
//                                                }
//                                            });
//                                        });
                                        function validCart(products) {
                                            console.log(products);
                                        }

        </script>
        <script>
            // Sử dụng JavaScript hoặc jQuery để tự động đóng alert sau một khoảng thời gian
            setTimeout(function () {
                $("#myAlert").alert('close'); // Sử dụng jQuery
                // Hoặc sử dụng JavaScript thuần
                // document.getElementById('myAlert').style.display = 'none';
            }, 3000); // 3000 milliseconds tương ứng với 3 giây
        </script>
        <script>
            const formCart = document.getElementById("formEditCart");
            console.log(formCart);
            formCart.addEventListener('keypress', function (e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                }
            });
            $('#formEditCart').on('keyup keypress', function (e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            });
        </script>


    </body>

</html> 